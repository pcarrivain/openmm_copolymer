# Copolymer using OpenMM

I am writing "import python file" using
the python
[OpenMM API](http://openmm.org)
to simulate block-copolymer.

## Background

The block-copolymer simulation is used to model
the epigenetic interactions in organism like
fruit fly, mouse, human...
This module takes advantage of the
[OpenMM API](http://openmm.org)
and GPU acceleration.
It builds a
[Kremer-Grest](https://aip.scitation.org/doi/10.1063/1.458541)
polymer model with uni-dimensional epigenetic
information and construct
the epigenetic interactions based on the model you design.
You simply need to feed the module with an *epigenome* state file,
the interaction model and the mechanical properties of the polymer.
You can imagine to model small part of an *epigenome* or
the whole genome confined inside the cell nucleus.
In the case you would like to study organism like Drosophila
you can use the module to introduce chromosome pairing.

## Building, test and run

To install the module you simply need to clone the repository.
I use a conda environment and python 3.7.
The module *openmm_copolymer* uses:

1. [OpenMM](http://openmm.org/)
2. [Numba](http://numba.pydata.org)
3. [Dask](https://dask.org)
4. [Numpy](numpy.org)
5. [Scipy](https://www.scipy.org)
6. [matplotlib](https://matplotlib.org/)
7. [pydoc3.7](https://docs.python.org/3/library/pydoc.html) to generate the documentation (html format)

The bash script
[run_test.sh](https://gitlab.com/pcarrivain/openmm_copolymer/-/blob/master/run_test.sh)
creates the documentation, runs the tests.
```bash
./run_test.sh -h
./run_test.sh -t <path to openmm_copolymer_functions.py> -p <path to python> -d <path to pydoc>
```

The
[documentation](https://gitlab.com/pcarrivain/openmm_copolymer/-/blob/master/openmm_copolymer_functions.html)
uses
[pydoc3.7](https://docs.python.org/3/library/pydoc.html).

The following command:
```bash
./run_examples.sh <path to python>
```
runs a block-copolymer (ABCD pattern) with self-attraction
of about $`0.2~k_BT`$ (Lennard-Jones potential),
computes contacts map and writes it.
If the example succeed you should see something like:
[ABCD contacts map](https://gitlab.com/pcarrivain/openmm_copolymer/-/blob/master/contact_maps/contacts.A.B.C.D_0.2kBT.png).
The data can be find in the folder:
```bash
linear/lj/gjf/CPU2/run_M_A.B.C.D_I_A.B.C.D_0.2kBT0.2kBT0.2kBT0.2kBT
```
The
[example script](https://gitlab.com/pcarrivain/openmm_copolymer/-/blob/master/run_examples.sh)
starts from a polymer that fits
in the simulation box and make it grow until the
bonds reach the size you ask for.
You can find
[OpenMM checkpoint](http://docs.openmm.org/latest/api-python/index.html)
and png files in the folder:
```bash
linear/lj/gjf/CPU2/run_M_A.B.C.D_I_A.B.C.D_0.2kBT0.2kBT0.2kBT0.2kBT/initial/seed1
```
Then, it runs the model you define over a number
of time-step and uses
[data analysis module](https://gitlab.com/pcarrivain/bacteria_analysis)
to compute contacts map.
The example might take some time to complete.

You can find another
[example](https://gitlab.com/pcarrivain/openmm_copolymer/-/blob/master/examples/make_yeast_telomere.sh)
about
[Yeast](https://en.wikipedia.org/wiki/Yeast)
[telomere](https://en.wikipedia.org/wiki/Telomere)
simulation.

## List of files

The example file
[openmm_copolymer.py](https://gitlab.com/pcarrivain/openmm_copolymer/-/blob/master/openmm_copolymer.py)
shows how to use the functions defined in
[openmm_copolymer_functions.py](https://gitlab.com/pcarrivain/openmm_copolymer/-/blob/master/openmm_copolymer_functions.py).

I also provide a
[jupyter-notebook](https://gitlab.com/pcarrivain/openmm_copolymer/-/blob/master/demonstration/openmm_copolymer_demonstration.ipynb)
(you can find in the [demonstration folder](https://gitlab.com/pcarrivain/openmm_copolymer/-/blob/master/demonstration)
that is more user-friendly.

The file
[models.py](https://gitlab.com/pcarrivain/openmm_copolymer/-/blob/master/models.py)
shows how to create specific block-copolymer models.

I provide a bash script
[run_model.sh](https://gitlab.com/pcarrivain/openmm_copolymer/-/blob/master/run_model.sh)
that creates folder according to the parameters you choose to use,
runs the simulation and writes data.

## Inputs

The state of each bead of the copolymer is described
by a file (*name.in*) where the first
column is the index of the chain, the second one
is the index of the bead inside
the chain and the third one is the name of the state.
The interaction between the states is also described
by a file (*name.int*) where the first
column is the first state A, the second one is the
second state B and the third one is interaction strength.
In the same way, it is possible to introduce
binders (*name.binders*) as small bead that bind to
specific state and can make bridges with other type of binder.

## Examples: simple block-copolymer models

You can run the
[openmm_copolymer.py](https://gitlab.com/pcarrivain/openmm_copolymer/-/blob/master/openmm_copolymer.py)
script example for a block-copolymer model (linear and circular)
with 1000 beads and ABBA, ABBB, ABBC and ABCD patterns with self-interaction
of $`0.2~k_BT`$ (Lennard-Jones potential) and get the
[cumulative contact maps](https://gitlab.com/pcarrivain/openmm_copolymer/-/blob/master/contact_maps)
over the trajectory.

The command line I used is like:
```bash
/scratch/anaconda3/bin/python3.7 /scratch/openmm_copolymer/openmm_copolymer.py -r 150 -k 300 -K 30 -b 0.02 -s 1 -R 0 -g GeForce_GTX_1050_Ti_OpenCL -m run -t gjf -c lj -l yes --precision=single --model=A.B.C.D --interactions=A.B.C.D_0.2kBT0.2kBT0.2kBT0.2kBT --binders=A.B.C.D --tmpdir=/scratch/openmm_copolymer
```
to create the initial conformation and then to run the system.

To build a conformation that fits into the simulation box,
to run or to do both:
```bash
--mode=initial
--mode=run
--mode=all
```

If you pass:
```bash
--model="A.B.C.D"
```
the code will know that you want to run a simple block-copolymer
model with four states.
Note that, the dot is important between two states.
It will looks for a file (in the epigenetics folder) like:
```bash
chain monomer state
0 0 A
0 1 A
0 2 B
0 3 B
0 4 C
0 5 C
0 6 D
0 7 D
```

If you pass:
```bash
--binders="A.B.C.D"
```
the code will know that you want to add four type of binders
that bind to the corresponding epigenetic state.

If you pass:
```bash
--interactions=A.B.C.D_0.2kBT0.2kBT0.2kBT0.2kBT
```
or:
```bash
--interactions=A.B.C.D_binders_0.2kBT0.2kBT0.2kBT0.2kBT
```
the code will know that you want to run a Potts model without/with binders.

To use Lennard-Jones/gaussian overlap potential:
```bash
-c lj
-c go
```

The functions that make epigenetic models are just examples on how to create your
own states files as well as how to create the interactions files.
The function **make_simple_epigenetic_models** creates chains according to the
pattern (example "A.B.C.D") you give as an argument.
The pattern is projected onto the chains you want to create.

It creates a file with binders like:
```bash
binder number mass_amu sigma_nm
binder_A 100 100.0 10.0
binder_B 100 100.0 10.0
binder_C 100 100.0 10.0
binder_D 100 100.0 10.0
```

The interaction file (Potts model with $`0.2~k_BT`$) is like:
```bash
A binder_A 0.2
B binder_B 0.2
C binder_C 0.2
D binder_D 0.2
binder_A binder_A 0.2
binder_B binder_B 0.2
binder_C binder_C 0.2
binder_D binder_D 0.2
```

It also creates a file with no binders (Potts model for the epigenetic states
with $`0.2~k_BT`$):
```bash
A A 0.2
B B 0.2
C C 0.2
D D 0.2
```

The following command line asks to run OpenMM
on a GeForce GTX 1050 Ti with OpenCL
implementation (CUDA is also possible) and single precision.
```bash
--precision=single -g GeForce_GTX_1050_Ti_OpenCL
```

The number of chains and the number of bonds are
given by the *.in* input file.

The resolution of the beads (in base-pair)
as-well-as the Kuhn length (in base-pair
and in nanometer) are given by:
```bash
-r 150 -k 300 -K 30
```

The number of base-pair per $`\text{nanometer}^3`$
controls the size of the box:
```bash
-b 0.02
```

Linear or circular polymer is given by:
```bash
-l yes
-l no
```

Note that genome-wide simulation of the
*Yeast* or the simulation of the *Drosophila*
uses spherical confinement; the previous option is then not used.

The seed and the thermostat are given by:
```bash
-s 1 -t gjf
```

You can also add pairing:
```bash
--pairing=pairing.in
```
where *pairing.in* is a file with pairing
description (in *epigenetics* folder).
```bash
chainA chainB U sigma
0 1 2.0 100.0
1 2 4.0 100.0
```
The first two columns are the indices of the chains.
The pairing uses a Lennard-Jones potential.
The parameters are given by the third and fourth column.
The path to where to read and write the data is given like:
```bash
--tmpdir=/scratch/openmm_copolymer
```

**Thermostat**

In the previous example I use the global thermostat
I implemented as an OpenMM CustomIntegrator.
You can choose the local version of the Langevin
(```bash -t langevin```) thermostat already implemented in OpenMM.
You can choose the
[Gronbech-Jensen-Farago](https://www.tandfonline.com/doi/abs/10.1080/00268976.2012.760055).
I implemented using the
[CustomIntegrator](https://simtk.org/api_docs/openmm/api4_1/python/classsimtk_1_1openmm_1_1openmm_1_1CustomIntegrator.html)
class from OpenMM.
In particular, I added a limit displacement and rescaling of the velocities options.
It is also possible to use the
[global thermostat](https://www.sciencedirect.com/science/article/pii/S0010465508000106?via%3D\ihub)
with limit on the displacement and rescaling.
This thermostat is known to lead to the
[Flying-Ice-Cube](https://onlinelibrary.wiley.com/doi/abs/10.1002/%28SICI%291096-987X\%28199805%2919%3A7%3C726%3A%3AAID-JCC4%3E3.0.CO%3B2-S)
phenomena.
Therefore, we need to periodically remove the linear and angular
motion with respect to the center-of-mass of the system.
I added a function *correction_flying_ice_cube* you can call outside of the
[OpenMM context](https://simtk.org/api_docs/openm\m/api10/classOpenMM_1_1Context.html).
To avoid calculation outside the context I added the correction directly in the
[CustomIntegrator](http://docs.openmm.org/lat\est/api-python/generated/simtk.openmm.openmm.CustomIntegrator.html)
I used to implement the
[global thermostat](https://www.s\ciencedirect.com/science/article/pii/S0010465508000106?via%3Dihub).

**Data analysis**

[up-to-date version](https://gitlab.com/pcarrivain/bacteria_analysis)
of data analysis module.

**Visualisation with [Blender](https://www.blender.org) (WIP)**

[up-to-date version](https://gitlab.com/pcarrivain/visualisation_with_blender)
of visualisation with Blender module.
Typical usage is like:
```bash
/scratch/blender-2.79b-linux-glibc219-x86_64/blender -b -P visualisation.py -- -v n1_r0_400x150bp.bin
									       -s n1_r0_400x150bp
									       -p path to the data
									       -w where to write rendering
									       -x 0.0 -y 0.0 -z 10.0
									       --e1=0.0 --e2=0.0 --e3=0.0
									       --nvertices=16
									       --rainbow
									       --resX=3840 --resY=2160
									       --save
```
where xyz is the location of the camera, e1,e2,e3 are Euler angles for the camera orientation.
The color palette is rainbow, the number of vertices per mesh is 16 and the resolution is 3840x2160.
The command line asks to save a blender file (all the outputs are written in the folder given by *-w*).
The example file I provide has many frames: you can navigate from one to another through the Blender interface.