#!/usr/bin/python
# -*- coding: utf-8 -*-
"""@package docstring
This module 'openmm_copolymer.py' is an example on how to create and run your model from
the functions described in the module 'openmm_copolymer_functions.py'.
A simple 'python3.7 openmm_copolymer.py -h' gives you more information about the parameters.
The following code (has been developped by Pascal Carrivain) is distributed under MIT licence.
"""
import sys
import json
import getopt
import simtk.openmm as mm
import numpy as np
import openmm_copolymer_functions as ocf


def main(argv):
    # default parameters
    seed = 1
    resolution = 150
    k_bp = 300
    k_nm = 30
    bp_per_nm3 = 0.01
    platform_name = "CPU1"
    PBC = True
    bconfinement = False
    mode = "initial"
    pair_style = "lj"
    rerun = 0
    thermostat = "langevin"
    linear = True
    start = 0
    every = 1000000
    MDS = 160000000
    precision = "double"
    model = "A.B.C.D"
    interactions = "Potts_0.2kBT"
    binders = ""
    pairing = "nopairing.in"
    try:
        opts, args = getopt.getopt(
            argv,
            "h:r:k:K:b:s:R:g:m:t:c:l:M:I:J:",
            [
                "resolution=",
                "kuhn_bp=",
                "kuhn_nm=",
                "bp_per_nm3=",
                "seed=",
                "rerun=",
                "platform_name=",
                "mode=",
                "thermostat=",
                "pair_style=",
                "linear=",
                "get_platforms",
                "precision=",
                "start=",
                "every=",
                "steps=",
                "model=",
                "interactions=",
                "binders=",
                "pairing=",
                "nopbc",
                "confinement",
                "tmpdir=",
            ],
        )
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for opt, arg in opts:
        if opt == "-h":
            print("--- openmm_copolymer.py usage :")
            print("python3.7 openmm_copolymer.py -r <resolution in bp>")
            print("                              -k <kuhn in bp>")
            print("                              -K <kuhn in nm>")
            print("                              -b <bp per nm3>")
            print("                              -s <seed>")
            print("                              -R <rerun>")
            print("                              -g <platform name>")
            print(
                "                              -m <mode ('initial' create the initial conformation while 'run' start from the initial conformation)>"
            )
            print(
                "                              -t <thermostat(gjf,global,langevin,nve)>"
            )
            print(
                "                              -c <pairwise style for block-copolymer interactions(go,lj)>"
            )
            print("                              -l <linear(no,yes)")
            print("                              --start=<start from iterations>")
            print("                              --every=<write conformation every>")
            print("                              --steps=<number of iterations>")
            print("                              --precision=<single,double>")
            print(
                "                              --model=<A.B.A.B,A.B.C.D,wt_Dernburg_Cell_1998,mutant_Dernburg_Cell_1998,yeast ...>"
            )
            print("                              --interactions=<Potts_-0.4kBT>")
            print("                              --binders=<add binders to the system>")
            print("                              --pairing=<add pairing from file>")
            print(
                "                              --nopbc <do not use periodic-boundary-conditions>"
            )
            print(
                "                              --confinement <use spherical confinement>"
            )
            print("                              --tmpdir=<where to save the data>")
            print(
                "The parameter given through option -n is ignored for Dernburg, Cell 1998 model."
            )
            print("\n--- openmm_copolymer.py examples :")
            print(
                "python3.7 openmm_copolymer.py -r 150 -k 300 -K 30 -b 0.02 -s 1 -g GeForce_GTX_1060_3GB_OpenCL -m run -t gjf -c lj -l yes --model=A.B.C.D --interactions=A.B.C.D_0.2kBT0.2kBT0.2kBT0.2kBT.int --pairing=pairing.in --tmpdir=/scratch/pcarriva/simulations/openmm_copolymer"
            )
            print(
                "python3.7 openmm_copolymer.py -r 150 -k 300 -K 30 -b 0.02 -s 1 -g GeForce_GTX_2080_Ti_OpenCL -m run -t langevin -c lj -l yes --model=wt_Dernburg_Cell_1998 --interactions=Potts_0.2kBT"
            )
            print(
                "python3.7 openmm_copolymer.py -k 300 -K 30 -b 0.02 -s 1 -g Tesla_K80_OpenCL -m run -t gjf -c lj -l yes --model=promoter_twist_enhancer_wt_23049440_s21000000_e24000000 --interactions=enhancer_promoter_0.2kBT --tmpdir=/scratch/pcarriva/simulations/openmm_copolymer"
            )
            print(
                "python3.7 openmm_copolymer.py --get_platforms prints the available platforms and devices and exit."
            )
            print(
                "generate the documentation : pydoc3.7 -w openmm_copolymer_functions.py"
            )
            sys.exit()
        elif opt == "--get_platforms":
            ocf.get_platforms()
            sys.exit()
        elif opt == "-r" or opt == "--resolution":
            resolution = int(arg)
        elif opt == "-k" or opt == "--kuhn_bp":
            k_bp = float(arg)
        elif opt == "-K" or opt == "--kuhn_nm":
            k_nm = float(arg)
        elif opt == "-b" or opt == "--bp_per_nm3":
            bp_per_nm3 = float(arg)
        elif opt == "-s" or opt == "--seed":
            seed = int(arg)
        elif opt == "-R" or opt == "--rerun":
            rerun = int(arg)
        elif opt == "-g" or opt == "--platform_name":
            platform_name = str(arg)
        elif opt == "-m" or opt == "--mode":
            mode = str(arg)
        elif opt == "-t" or opt == "--thermostat":
            thermostat = str(arg)
        elif opt == "-c" or opt == "--pair_style":
            pair_style = str(arg)
        elif opt == "-l" or opt == "--linear":
            linear = bool(arg == "yes")
        elif opt == "--start":
            start = int(arg)
        elif opt == "--every":
            every = int(arg)
        elif opt == "--steps":
            MDS = int(arg)
        elif opt == "--precision":
            precision = arg
        elif opt == "-M" or opt == "--model":
            model = arg
        elif opt == "-I" or opt == "--interactions":
            interactions = arg
        elif opt == "-J" or opt == "--binders":
            binders = arg
        elif opt == "--pairing":
            pairing = arg
        elif opt == "--nopbc":
            PBC = False
        elif opt == "--confinement":
            bconfinement = True
            PBC = False
        elif opt == "--tmpdir":
            tmpdir = arg
        else:
            pass

    if seed == 1 or seed == 2:
        every = 1000
    # bond size
    sigma = (k_nm * resolution / k_bp) / 0.965
    # mass in amu
    mass = resolution * 700.0
    # temperature in Kelvin
    Temp = 300.0
    # bending strength
    Kb = ocf.bending_rigidity(k_bp, resolution, Temp)
    # get the epigenetic states first
    chains, states, colors, N_per_C, C, N = ocf.read_states(
        tmpdir + "/epigenetics/" + model + ".in"
    )
    # get the binders
    mbinders = []
    sbinders = []
    cbinders = []
    if binders != "":
        states, mbinders, sbinders, cbinders = ocf.read_binders(
            tmpdir + "/epigenetics/" + binders + ".binders", states
        )
    # number of binders
    B = len(mbinders)
    # drosophila/yeast bp per nm^3 and box size
    if "Dernburg" in model:
        bp_per_nm3 = 320 * 1000000 / (4.0 * np.pi * np.power(2000.0, 3.0) / 3.0)
        L = np.cbrt(N * resolution / bp_per_nm3)
    elif "promoter" in model:
        bp_per_nm3 = 320 * 1000000 / (4.0 * np.pi * np.power(2500.0, 3.0) / 3.0)
        L = np.cbrt(N * resolution / bp_per_nm3)
    elif "yeast_genome" in model or "yeast_single_chromosome" in model:
        L = 2000.0
        PBC = False
    else:
        L = np.cbrt(N * resolution / bp_per_nm3)
    # check the volume fraction
    ocf.check_volume_fraction([sigma] * N + sbinders, L)
    # get the epigenetic interactions
    model_of_interactions = ocf.read_interactions(
        tmpdir + "/epigenetics/" + interactions + ".int", Temp
    )
    # time scale size*sqrt(mass/kBT)
    tau = ocf.minimal_time_scale(
        [mass] * N + mbinders, [sigma] * N + sbinders, [ocf.get_kBT(Temp), Kb]
    )

    # path
    polymer = ["ring", "linear"][int(linear)]
    root_to = (
        tmpdir
        + "/"
        + polymer
        + "/"
        + pair_style
        + "/"
        + thermostat
        + "/"
        + platform_name
        + "/run_M_"
        + model
        + "_I_"
        + interactions
    )
    path_to = root_to + "/seed" + str(seed) + "/rerun" + str(rerun)
    initial_name = root_to + "/initial/seed" + str(seed) + "/initial_C" + str(C)

    # write entanglement info
    ocf.write_entanglement_info(
        bp_per_nm3,
        N,
        resolution,
        k_bp,
        k_nm,
        tmpdir + "/dimensions/dimensions_C" + str(C) + "_" + model + ".out",
    )
    # system creation (remove com motion every step)
    system = ocf.create_system(L, L, L, [mass] * N + mbinders, 2 * MDS)
    # add fene bond to the system (you have to before to run the 'create_initial_conformation' function)
    system.addForce(ocf.fene_wca(N_per_C, Temp, sigma, linear, PBC))
    # add WCA to the system (you have to before to run the 'create_initial_conformation' function)
    system.addForce(ocf.wca(N_per_C, Temp, sigma, sbinders, linear, PBC, True))
    # add nucleus confinement
    if bconfinement:
        system.addForce(
            ocf.confinement(
                N + B,
                np.cbrt(3.0 * N * resolution / (4.0 * np.pi * bp_per_nm3)),
                0.5 * L,
                0.5 * L,
                0.5 * L,
                2.0 * ocf.get_kBT(Temp),
            )
        )
    # add pairing
    system.addForce(ocf.pairing(N_per_C, tmpdir + "/epigenetics/" + pairing, PBC))
    # add bending force to the system
    if resolution < k_bp:
        system.addForce(
            ocf.tt_bending(N_per_C, [Kb] * (N - int(linear) * 2 * C), linear, PBC)
        )
    # add copolymer interactions to the system
    if pair_style == "go":
        system.addForce(
            ocf.go_copolymer(
                resolution,
                k_bp,
                k_nm,
                states,
                model_of_interactions,
                N_per_C,
                linear,
                PBC,
            )
        )
    elif pair_style == "lj":
        system.addForce(
            ocf.lj_copolymer(
                resolution,
                k_bp,
                k_nm,
                [sigma] * N + sbinders,
                states,
                model_of_interactions,
                N_per_C,
                linear,
                PBC,
            )
        )
    else:
        print("No valid pair-style. Please consider using 'go' or 'lj'")
    # create the integrator
    integrator = ocf.create_integrator(
        thermostat,
        Temp,
        0.01 * tau,
        3 * system.getNumParticles(),
        tau,
        seed,
        False,
        False,
        False,
    )
    # create the platform
    platform, properties = ocf.create_platform(platform_name, precision)
    # get the centromeres ?
    z_offset = [0] * C
    if "yeast_genome" in model or "yeast_single_chromosome" in model:
        chr0, arm0, z_offset = ocf.get_yeast_centromeres(resolution)
    # create the context with positions and velocities
    positions, sigma0, Lx, Ly, Lz = ocf.positions_linear_or_ring(
        N_per_C, z_offset, sigma, sbinders, L, linear
    )
    velocities = ocf.v_gaussian(Temp, [mass] * N + mbinders, seed, False)
    context = ocf.create_context(
        system,
        integrator,
        platform,
        properties,
        [mm.Vec3(p[0], p[1], p[2]) for p in positions],
        velocities,
    )
    # write details
    with open(path_to + "/details.json", "w") as in_file:
        details = {}
        details["chains"] = C
        details["bonds"] = int(np.sum(N_per_C))
        details["bond_length"] = sigma
        details["dt"] = 0.01 * tau
        details["inverse_coupling_frequency"] = tau
        for i, n in enumerate(N_per_C):
            details["chain" + str(i)] = n
        json.dump(details, in_file, indent=1)

    if mode == "initial":
        # no block-copolymer interactions during the creation of the initial conformation
        ocf.on_off_block_copolymer(context, "off")
        # create and write the initial conformation
        ocf.create_initial_conformation(
            context,
            N_per_C,
            thermostat,
            sigma,
            sigma0,
            Temp,
            1000,
            2000,
            16,
            initial_name,
        )
        # write the checkpoint
        ocf.read_write_checkpoint(context, initial_name + "_s0.chk", "write")
        # get info about the system you just created
        ocf.get_info_system(context)
    elif mode == "run":
        # run the model with the previously generated initial conformation
        success = ocf.read_write_checkpoint(
            context, initial_name + "_s" + str(start) + ".chk", "read"
        )
        if success == 0:
            sys.exit()
        # turn on the block-copolymer interactions
        ocf.on_off_block_copolymer(context, "on")
        ocf.get_info_system(context)
        iterations = start
        while iterations < (start + MDS):
            print("from " + str(iterations) + " to " + str(iterations + every))
            context.getIntegrator().step(every)
            iterations += every
            # get the positions (in nanometer) for further analysis
            positions = ocf.get_positions_from_context(context, 0, N + B, PBC=False)
            ocf.write_draw_conformation(
                positions,
                norm_p=L,
                xyz_lim=[0.0, 1.0, 0.0, 1.0, 0.0, 1.0],
                name_write=path_to + "/xyz." + str(iterations) + ".out",
                mode="write",
            )
            if (iterations % 100000) == 0:
                ocf.write_draw_conformation(
                    positions,
                    norm_p=L,
                    xyz_lim=[0.0, 1.0, 0.0, 1.0, 0.0, 1.0],
                    name_write=path_to + "/xyz." + str(iterations) + ".out",
                    name_draw=path_to + "/xyz." + str(iterations) + ".png",
                    mode="write_draw",
                )
        # from context to blender
        ocf.from_context_to_blender(
            context,
            colors + cbinders,
            False,
            np.array([sigma] * N + sbinders),
            path_to
            + "/n"
            + str(seed)
            + "_r"
            + str(rerun)
            + "_"
            + str(N)
            + "x"
            + str(resolution)
            + "bp",
            "yes",
        )
        # write checkpoint
        ocf.read_write_checkpoint(
            context, initial_name + "_s" + str(start + MDS) + ".chk", "write"
        )


if __name__ == "__main__":
    main(sys.argv[1:])
