#!/usr/bin/python
# -*- coding: utf-8 -*-
"""@package docstring
The module 'openmm_copolymer_functions.py' is a collection of functions used to define
a Kremer-Grest polymer with bending rigidity and uni-dimensionnal epigenetic informations.
This module goes with a python script 'openmm_copolymer.py' that is an example on how to
create and run your model.
The following code (has been developped by Pascal Carrivain) is distributed under MIT licence.
"""
import numba
from numba import prange
import cProfile, io, pstats
import json
import time
import struct
import sys
from sys import stdout
import simtk.openmm as mm
import simtk.unit as unit
from mpl_toolkits import mplot3d
import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
from numpy import linalg as LA


def openmm_units():
    print("position is in nanometer.")
    print("mass is in amu.")
    print("kBT is in kJ/mol.")
    print("time is in picosecond.")


def get_kBT(Temp: float) -> float:
    """return kB*Temp in kJ/mol.
    Parameters
    ----------
    Temp : temperature in Kelvin (float)
    """
    # print(unit.BOLTZMANN_CONSTANT_kB*Temp/(unit.BOLTZMANN_CONSTANT_kB*unit.AVOGADRO_CONSTANT_NA*Temp))
    # sys.exit()
    return 1.0
    # return unit.BOLTZMANN_CONSTANT_kB*unit.AVOGADRO_CONSTANT_NA*Temp
    # return unit.BOLTZMANN_CONSTANT_kB*Temp


def old_minimal_time_scale(mass: list, sigmas: list, Temp: float = 300.0) -> float:
    """return the minimal time-scale based on the mass and size of the particle.
    Parameters
    ----------
    mass   : mass (in amu) of each particle (list of float)
    sigmas : size (in nm) of each particle (list of float)
    Temp   : temperature (in kelvin) of the system (float)
    Returns
    -------
    the minimal time-scale in picosecond (float)
    Raises
    ------
    """
    kBT = unit.BOLTZMANN_CONSTANT_kB * unit.AVOGADRO_CONSTANT_NA * Temp * unit.kelvin
    return np.min(
        np.array(
            [
                (s * unit.nanometer * unit.sqrt(mass[i] * unit.amu / kBT))
                / unit.picosecond
                for i, s in enumerate(sigmas)
            ]
        )
    )


def minimal_time_scale(mass: list, sigmas: list, U: list) -> float:
    """return the minimal time-scale based on the mass and size of the particle.
    Parameters
    ----------
    mass   : mass (in amu) of each particle (list of float)
    sigmas : size (in nm) of each particle (list of float)
    U      : energy strength (in kBT) (list float)
    Returns
    -------
    minimal time-scale (float)
    Raises
    ------
    """
    time_scale = np.zeros(len(U))
    for i, u in enumerate(U):
        time_scale[i] = np.min(
            np.array([s * np.sqrt(mass[j] / u) for j, s in enumerate(sigmas)])
        )
    return np.min(time_scale)


@numba.jit(nopython=True)
def square_distance_with_PBC(
    p1: np.ndarray, p2: np.ndarray, L: np.ndarray, PBC: bool = True
) -> float:
    """return the distance between two points with PBC.
    Parameters
    ----------
    p1  : point 1 (np.ndarray)
    p2  : point 2 (np.ndarray)
    L   : box size (np.ndarray)
    PBC : periodic boundary conditions (bool)
    Returns
    -------
    square distance with PBC (float)
    """
    d12 = np.subtract(p1, p2)
    if PBC:
        d12 = np.subtract(
            d12,
            np.array(
                [
                    L[0] * np.rint(d12[0] / L[0]),
                    L[1] * np.rint(d12[1] / L[1]),
                    L[2] * np.rint(d12[2] / L[2]),
                ]
            ),
        )
    return np.dot(d12, d12)


def check_volume_fraction(sizes: list, L: float):
    """check the volume fraction, exit if it is greater than one.
    Parameters
    ----------
    sizes : size of each particle (list of float)
    L     : size of the box (float)
    Returns
    -------
    Raises
    ------
    """
    # volume
    V = np.sum(
        np.array([4.0 * np.pi * (0.5 * s) * (0.5 * s) * (0.5 * s) / 3.0 for s in sizes])
    )
    # volume fraction
    if (V / (L * L * L)) >= 1.0:
        print(
            "fraction of occupied volume "
            + str(V / (L * L * L))
            + " is greater than one, exit."
        )
        sys.exit()
    else:
        print("fraction of occupied volume=" + str(V / (L * L * L)))


@numba.jit
def from_i_to_bead(i: int, N: int) -> int:
    """return the index 'i' projected to the ring chain.
    Parameters
    ----------
    i : index (int) from the start of the chain
    N : the number of beads in the ring chain (int)
    Returns
    -------
    return the index 'i' (int) projected onto the ring chain.
    Raises
    ------
    """
    if i < 0:
        return N - (-i) % N
    elif i >= N:
        return i % N
    else:
        return i


@numba.jit(nopython=True, parallel=True)
def where_the_beads_are(positions: np.ndarray, size: float) -> tuple:
    """return a partition of the space.
    Parameters
    ----------
    positions : list of positions (in nm) from 'get_positions_from_context' function
    size      : size of each partition block (in nanometer)
    Returns
    -------
    the partition as a python dictionnary (dict), the bead to block (list) correspondance.
    Raises
    ------
    """
    size_positions = np.multiply(1.0 / size, positions)
    # space partitioning
    xyz = np.zeros(3)
    XYZ = np.zeros(3)
    for c in prange(3):
        xyz[c] = np.min(size_positions[:, c]) - 1
        XYZ[c] = np.max(size_positions[:, c]) + 1
    # number of beads
    N, D = positions.shape  # int(np.sum(np.greater_equal(size_positions[:,0],xyz[0])))
    # blocks per dimension
    Px, Py, Pz = (
        int(XYZ[0] - xyz[0]) + 1,
        int(XYZ[1] - xyz[1]) + 1,
        int(XYZ[2] - xyz[2]) + 1,
    )
    P = Px * Py * Pz
    # step 1
    bead_to_block = np.full(N, -1)
    beads_in_block = np.full(P, 0)
    for i in range(N):
        pi = size_positions[i, :3]
        index = (
            int(pi[0] - xyz[0]) * Py * Pz
            + int(pi[1] - xyz[1]) * Pz
            + int(pi[2] - xyz[2])
        )
        beads_in_block[index] += 1
        bead_to_block[i] = index
    # step 2
    max_beads_in_block = int(np.max(beads_in_block))
    # step 3
    block_to_beads = np.full((P, max_beads_in_block), -1)
    beads_in_block = np.full(P, 0)
    for i, b in enumerate(bead_to_block):
        block_to_beads[b][beads_in_block[b]] = i
        beads_in_block[b] += 1
    return block_to_beads, beads_in_block, bead_to_block, Px, Py, Pz


@numba.jit(nopython=True)
def get_close_ij(
    positions: np.ndarray, cut: float = 1.0, cut_ij: int = 1, linear: bool = True
) -> tuple:
    """simple algorithm to return the pairwize with distance <= cut.
    Parameters
    ----------
    positions : list of positions (in nm) returns by 'get_positions_from_context' function
    cut       : cutoff (in nanometer) to define close (i,j) (float)
    cut_ij    : keep only the pairwize with |j-i|>=cut_ij (integer)
    linear    : linear polymer (True) or ring polymer (False)
    Returns
    -------
    a list made of the close (i,j) with |j-i|>=cut_ij.
    Raises
    ------
    """
    int_linear = int(linear)
    N, D = positions.shape
    # space partitioning
    block_to_beads, beads_in_block, bead_to_block, Px, Py, Pz = where_the_beads_are(
        positions, cut
    )
    # loop over cells
    close_ij = []
    C = 0
    for i in range(N):
        # block bead 'i'
        block_i = bead_to_block[i]
        iz = block_i % Pz
        iy = ((block_i - iz) // Pz) % Py
        ix = (block_i - iz - iy * Pz) // (Py * Pz)
        sex = np.arange(max(0, ix - 1), min(ix + 1, Px - 1) + 1)
        sey = np.arange(max(0, iy - 1), min(iy + 1, Py - 1) + 1)
        sez = np.arange(max(0, iz - 1), min(iz + 1, Pz - 1) + 1)
        # position bead 'i'
        pi = positions[i, :]
        # loop over neighboor cells
        for x in sex:
            for y in sey:
                for z in sez:
                    block = x * Py * Pz + y * Pz + z
                    partition_block = block_to_beads[block][: beads_in_block[block]]
                    # loop over bead "j"
                    for j in partition_block:
                        if j >= i:
                            break
                        pj = positions[j, :]
                        ij = (i - j) * int_linear + min(N - (i - j), i - j) * (
                            1 - int_linear
                        )
                        if (
                            np.sum(np.square(np.subtract(pi, pj))) <= (cut * cut)
                            and ij >= cut_ij
                        ):
                            close_ij.append([j, i])
                            C += 1
    return close_ij, C


def flangevin(x: float) -> float:
    """return the value of the Langevin function at x : 1/tanh(x)-1/x.
    Parameters
    ----------
    x : the x where to evaluate the Langevin function (float)
    Returns
    -------
    the value of the Langevin function at x (float)
    Raises
    ------
    """
    if x == 0.0:
        return 0.0
    else:
        return 1.0 / np.tanh(x) - 1.0 / x


def bending_rigidity(k_bp: int, resolution: int, Temp: float = 300.0) -> float:
    """return the bending rigidity for a specific resolution (in bp) and Kuhn length (in bp).
    Parameters
    ----------
    k_bp       : Kuhn length in bp (int)
    resolution : resolution of a segment in bp (int)
    Temp       : temperature (float)
    Returns
    -------
    bending rigidity in units of kB*Temperature (float)
    Raises
    ------
    """
    if resolution >= k_bp:
        return 0.0
    mcos = (k_bp / resolution - 1.0) / (k_bp / resolution + 1.0)
    a = 1e-10
    b = 1e2
    precision = 1e-10
    fa = flangevin(a) - mcos
    fb = flangevin(b) - mcos
    m = 0.5 * (a + b)
    fm = flangevin(m) - mcos
    while abs(a - b) > precision:
        m = 0.5 * (a + b)
        fm = flangevin(m) - mcos
        fa = flangevin(a) - mcos
        if fm == 0.0:
            break
        if (fa * fm) > 0.0:
            a = m
        else:
            b = m
    return m * get_kBT(Temp)


def fene_wca(
    N_per_C: list, Temp: float, sigma: float, linear: bool, PBC: bool = True
) -> mm.CustomBondForce:
    """return a FENE force plus WCA repulsive force between consecutive beads.
    you can change the size of the bond with the global parameter 'f_fene'.
    Parameters
    ----------
    N_per_C : number of beads for each chain (list of int)
    Temp    : temperature (float)
    sigma   : size of the bond between two consecutive beads (float)
    linear  : True (linear chain) or False (ring chain) (bool)
    PBC     : does it use the periodic boundary conditions, False or True ? (bool)
    Returns
    -------
    OpenMM CustomBondForce (FENE potential between two consecutive bonds).
    """
    a_cutoff = np.power(2.0, 1.0 / 6.0)
    fene_wca = mm.CustomBondForce(
        "-0.5*K_fene*R0*R0*log(1-(r*r/(f_fene*R0*f_fene*R0)))*step(0.999*f_fene*R0-r)+4*U_fene*((f_fene*sigma/r)^12-(f_fene*sigma/r)^6+0.25)*step(a_cutoff_fene*f_fene*sigma-r)"
    )
    fene_wca.addGlobalParameter("K_fene", 30.0 * get_kBT(Temp) / (sigma * sigma))
    fene_wca.addGlobalParameter("U_fene", get_kBT(Temp))
    fene_wca.addGlobalParameter("sigma", sigma)
    fene_wca.addGlobalParameter("R0", 1.5 * sigma)
    fene_wca.addGlobalParameter("f_fene", 1.0)
    fene_wca.addGlobalParameter("a_cutoff_fene", a_cutoff)
    index = 0
    if linear:
        for n in N_per_C:
            for i in range(n - 1):
                fene_wca.addBond(index + i, index + i + 1)
            index += n
    else:
        for n in N_per_C:
            for i in range(n):
                fene_wca.addBond(index + i, index + from_i_to_bead(i + 1, n))
            index += n
    fene_wca.setUsesPeriodicBoundaryConditions(PBC)
    fene_wca.setForceGroup(0)
    return fene_wca


def pairing(N_per_C: list, fpairing: str = "", PBC: bool = True) -> mm.CustomBondForce:
    """return a LJ pairing potential between homologous beads.
    if the two chains do not have the same length, do nothing.
    Parameters
    ----------
    N_per_C  : number of beads for each chain (list of int)
    fpairing : name of file that described pairing (str)
              do nothing if it is "" or if number of chains is 1
              example (column 1 is chain 1, column 2 is chain 2, column 3 is pairing strength in kBT and column 4 is sigma in nm)
              chainA chainB U sigma
              0 1 2.0 100.0
              1 2 4.0 150.0
    PBC      : does it use the periodic boundary conditions, False or True ? (bool)
    Returns
    -------
    OpenMM CustomBondForce (short-range attractive potential for pairing between two homologous chains).
    if the two chains do not have the same length, return an empty bond custom force.
    Raises
    ------
    """
    C = len(N_per_C)
    pairing = mm.CustomBondForce(
        "4*epsilon_pairing*((sigma_pairing/r)^12-(sigma_pairing/r)^6+0.25)"
    )
    pairing.addPerBondParameter("epsilon_pairing")
    pairing.addPerBondParameter("sigma_pairing")
    if C > 1 or fpairing != "":
        try:
            with open(fpairing, "r") as in_file:
                lines = in_file.readlines()
                L = len(lines)
                for i, l in enumerate(lines):
                    sl = l.split()
                    if i == 0:
                        if (
                            sl[0] != "chainA"
                            and sl[1] != "chainB"
                            and sl[2] != "U"
                            and sl[3] != "sigma"
                        ):
                            print("no header in " + str(pairing) + ", do nothing.")
                            break
                    else:
                        C1 = int(sl[0])
                        C2 = int(sl[1])
                        U = float(sl[2])
                        sigma = float(sl[3])
                        # C1 and C2 have to be lesser than the number of chains
                        if C1 < C and C2 < C:
                            if N_per_C[C1] == N_per_C[C2]:
                                N = sum(N_per_C)
                                N1 = N - sum(N_per_C[C1:])
                                N2 = N - sum(N_per_C[C2:])
                                L1 = N_per_C[C1]
                                for i in range(L1):
                                    pairing.addBond(N1 + i, N2 + i, [U, sigma])
        except IOError:
            print("did not find pairing file: " + fpairing + ", do nothing.")
    else:
        pass
    pairing.setUsesPeriodicBoundaryConditions(PBC)
    pairing.setForceGroup(1)
    return pairing


def tt_bending(N_per_C: list, Kb: list, linear: bool, PBC: bool) -> mm.CustomAngleForce:
    """return a bending force between three consecutive beads.
    Parameters
    ----------
    N_per_C : number of beads for each chain (list of int)
    Kb      : bending rigidity along the chain (list of float)
    linear  : linear (True) or ring chain (False) (bool)
    PBC     : does it use the periodic boundary conditions, False or True ? (bool)
    Returns
    -------
    OpenMM CustomAngleForce bending force between two consecutive tangent vector t.
    Raises
    ------
    if the number of angles does not correspond to the length of bending rigidity input argument 'Kb'.
    """
    int_linear = int(linear)
    # number of bonds
    N = sum(N_per_C) - len(N_per_C) * int_linear
    # number of angles
    A = sum(N_per_C) - 2 * len(N_per_C) * int_linear
    # bending as mm.CustomAngleForce
    bending = mm.CustomAngleForce("f_tt*Kb_tt*(1.0-cos(theta-A_tt))")
    bending.addPerAngleParameter("Kb_tt")
    bending.addGlobalParameter("f_tt", 1.0)
    bending.addGlobalParameter("A_tt", np.pi)
    bending.addEnergyParameterDerivative("f_tt")
    if len(Kb) == A:
        # add angles
        index = 0
        angles = 0
        for n in N_per_C:
            for i in range(n - 2 * int_linear):
                bending.addAngle(
                    index + i,
                    index + from_i_to_bead(i + 1, n),
                    index + from_i_to_bead(i + 2, n),
                    [Kb[angles]],
                )
                angles += 1
            index += n
    else:
        print(
            "The number of angles does not correspond to the length of bending rigidity, return empty 'CustomAngleForce'."
        )
    bending.setUsesPeriodicBoundaryConditions(PBC)
    bending.setForceGroup(2)
    return bending


def wca(
    N_per_C: list,
    Temp: float,
    sigma: float,
    binders_size: list,
    linear: bool = True,
    PBC: bool = True,
    bond_exclusion: bool = True,
) -> mm.CustomNonbondedForce:
    """return a WCA potential to resolve excluded volume constraints.
    Parameters
    ----------
    N_per_C        : number of beads for each chain (list of int)
    Temp           : temperature (float)
    sigma          : size of the bead (float)
    binders_size   : size of each binder (list of float)
    linear         : linear (True) or ring chain (False) (bool)
                     argument 'linear' is unused for now
    PBC            : does it use periodic boundary conditions ? (bool)
    bond_exclusion : does wca apply between two beads of the same bond ? (bool)
    Returns
    -------
    OpenMM CustomNonbondedForce (WCA potential)
    """
    a_cutoff = np.power(2.0, 1.0 / 6.0)
    # wca=mm.CustomNonbondedForce('4*epsilon_wca*((sigma_wca/r)^12-(sigma_wca/r)^6+0.25)*step(a_cutoff_wca*sigma_wca-r);sigma_wca=f_wca*sqrt(sigma_wca1*sigma_wca2)')
    wca = mm.CustomNonbondedForce(
        "4*epsilon_wca*((sigma_wca/r)^12-(sigma_wca/r)^6+0.25)*step(a_cutoff_wca*sigma_wca-r);sigma_wca=f_wca*0.5*(sigma_wca1+sigma_wca2)"
    )
    wca.addGlobalParameter("a_cutoff_wca", a_cutoff)
    wca.addGlobalParameter("epsilon_wca", get_kBT(Temp))
    wca.addGlobalParameter("f_wca", 1.0)
    wca.addPerParticleParameter("sigma_wca")
    wca.addEnergyParameterDerivative("epsilon_wca")
    wca.addEnergyParameterDerivative("f_wca")
    if PBC:
        wca.setNonbondedMethod(mm.NonbondedForce.CutoffPeriodic)
    else:
        wca.setNonbondedMethod(mm.NonbondedForce.CutoffNonPeriodic)
    # add N particles
    N = sum(N_per_C)
    for i in range(N):
        wca.addParticle([sigma])
    # exclusion(s) ?
    if bond_exclusion:
        index = 0
        for n in N_per_C:
            for i in range(n - 1):
                wca.addExclusion(index + i, index + i + 1)
            if not linear:
                wca.addExclusion(index + n - 1, index + 0)
            index += n
    # add the binders
    max_sigma = sigma
    for s in binders_size:
        wca.addParticle([s])
        max_sigma = max(max_sigma, s)
    # cutoff
    wca.setCutoffDistance(a_cutoff * max_sigma)
    print("wca cutoff=", wca.getCutoffDistance())
    wca.setForceGroup(3)
    return wca


def go_copolymer(
    resolution: int,
    k_bp: int,
    k_nm: float,
    states: list,
    interactions,
    N_per_C: list,
    linear: bool,
    PBC: bool,
) -> mm.CustomNonbondedForce:
    """return a gaussian overlap for copolymer interactions.
    Parameters
    ----------
    resolution   : the resolution of the bead in bp (integer)
    k_bp         : Kuhn length in bp (integer)
    k_nm         : Kuhn length in nm (float)
    states       : a list of states, example [1,0,3,4,...,3,4,4,4] (list of int)
    interactions : a list of tuple, example [[0,0,-3.0],[0,1,0.0],...,[1,4,1.0]] (N-tuple of [integer,integer,float])
    N_per_C      : number of beads for each chain (list of int)
                   it is needed for the bond exclusion.
    linear       : linear (True) or ring chain (False)
    PBC          : does it use periodic boundary conditions ? (bool)
    Returns
    -------
    OpenMM CustomNonbondedForce (gaussian overlap for copolymer interactions)
    """
    # counting the number of states (based on their names)
    S = len(states)
    unique_states = [states[0]]
    for r in range(1, S):
        unique = True
        for s in range(0, r):
            unique = unique and not states[r] == states[s]
        if unique:
            unique_states.append(states[r])
    n_states = len(unique_states)
    print("The function 'go_copolymer' found " + str(n_states) + " states.")
    # creating the custom force expression
    go = mm.CustomNonbondedForce("")
    for s in range(n_states):
        go.addPerParticleParameter("p_" + unique_states[s])  # 0 or 1 for the state "u"
    # no need to add a zero interaction to the custom force expression
    expression = ""
    for i in interactions:
        r0 = i[0]
        s0 = i[1]
        print("interaction between " + r0 + " and " + s0 + " is " + str(i[2]) + ".")
        expression += "p_" + r0 + "1" + "*" + "p_" + s0 + "2*D_" + r0 + "_" + s0 + "+"
        if r0 != s0:
            expression += (
                "p_" + s0 + "1" + "*" + "p_" + r0 + "2*D_" + r0 + "_" + s0 + "+"
            )
        # interaction strength between state "r" and state "s" with D(r,s)=D(s,r)
        go.addGlobalParameter("D_" + r0 + "_" + s0, i[2])
    expression = expression[:-1]
    print(
        "The epigenetic interaction expression built from the interaction file is : "
        + expression
        + "."
    )
    Rg2 = np.power(k_nm * resolution / k_bp, 2.0) / 6.0
    go.setEnergyFunction("D_r_s*go_cst*exp(exp_cst*r^2);D_r_s=" + expression)
    go.addGlobalParameter(
        "go_cst",
        np.power(0.75 / np.pi, 1.5) * resolution * resolution / (Rg2 * np.sqrt(Rg2)),
    )
    go.addGlobalParameter("exp_cst", -0.75 / Rg2)
    if PBC:
        go.setNonbondedMethod(mm.NonbondedForce.CutoffPeriodic)
    else:
        go.setNonbondedMethod(mm.NonbondedForce.CutoffNonPeriodic)
    go.setCutoffDistance(5.0 * np.sqrt(Rg2))
    # per particle parameter p(s)
    for i in range(S):
        state = states[i]
        go.addParticle([1 * int(state == unique_states[s]) for s in range(n_states)])
    # no go interactions between particles of the same bond
    index = 0
    for n in N_per_C:
        for i in range(n - int(linear)):
            go.addExclusion(index + i, index + from_i_to_bead(i + 1, n))
        index += n
    go.setForceGroup(4)
    return go


def lj_copolymer(
    resolution: int,
    k_bp: int,
    k_nm: float,
    sigma: list,
    states: list,
    interactions: list,
    N_per_C: list,
    linear: bool,
    PBC: bool,
) -> mm.CustomNonbondedForce:
    """return a lj potential for copolymer interactions.
    Parameters
    ----------
    resolution   : the resolution of the bead in bp (integer)
    k_bp         : Kuhn length in bp (integer)
    k_nm         : Kuhn length in nm (float)
    sigma        : size of each bead (list of float)
    states       : a list of states, example [1,0,3,4,...,3,4,4,4] (list of int)
    interactions : a list of tuple, example [[0,0,-3.0],[0,1,0.0],...,[1,4,1.0]] (N-tuple of [integer,integer,float])
    N_per_C      : number of beads for each chain (list of int)
                   it is needed for the bond exclusion.
    linear       : linear (True) or ring chain (False)
    PBC          : does it use periodic boundary conditions ? (bool)
    Returns
    -------
    OpenMM CustomNonbondedForce (gaussian overlap for copolymer interactions)
    Raises
    ------
    """
    # counting the number of states (based on their names)
    S = len(states)
    unique_states = [states[0]]
    for r in range(1, S):
        unique = True
        for s in range(0, r):
            unique = unique and not states[r] == states[s]
        if unique:
            unique_states.append(states[r])
    n_states = len(unique_states)
    print("The function 'lj_copolymer' found " + str(n_states) + " states.")
    # max(sigma)
    max_sigma = max(sigma)
    # creating the custom force expression
    lj = mm.CustomNonbondedForce("")
    lj.addPerParticleParameter("sigma_lj")
    # 0 or 1 for the state "u"
    for u in unique_states:
        lj.addPerParticleParameter("p_" + u)
    # no need to add a zero interaction to the custom force expression
    expression = ""
    for i in interactions:
        r0 = i[0]
        s0 = i[1]
        print("interaction between " + r0 + " and " + s0 + " is " + str(i[2]) + ".")
        expression += "p_" + r0 + "1" + "*" + "p_" + s0 + "2*D_" + r0 + "_" + s0 + "+"
        if r0 != s0:
            expression += (
                "p_" + s0 + "1" + "*" + "p_" + r0 + "2*D_" + r0 + "_" + s0 + "+"
            )
        # interaction strength between state "r" and state "s" with D(r,s)=D(s,r)
        lj.addGlobalParameter("D_" + r0 + "_" + s0, i[2])
    expression = expression[:-1]
    print(
        "The epigenetic interaction expression built from the interaction file is : "
        + expression
        + "."
    )
    # on/off the LJ copolymer potential
    lj.addGlobalParameter("lj_on_off", 1)
    lj.setEnergyFunction(
        "lj_on_off*4*D_r_s*((sigma_lj/r)^12-(sigma_lj/r)^6);D_r_s="
        + expression
        + ";sigma_lj=0.5*(sigma_lj1+sigma_lj2)"
    )  # ;sigma_lj=sqrt(sigma_lj1*sigma_lj2)")
    if PBC:
        lj.setNonbondedMethod(mm.NonbondedForce.CutoffPeriodic)
    else:
        lj.setNonbondedMethod(mm.NonbondedForce.CutoffNonPeriodic)
    # cutoff at U~0.01*D_r_s
    lj.setCutoffDistance(2.15 * max_sigma)
    # per particle parameter p(s)
    for i in range(S):
        state = states[i]
        lj.addParticle([sigma[i]] + [1 * int(state == u) for u in unique_states])
    # no lj interactions between particles of the same bond
    index = 0
    for n in N_per_C:
        for i in range(n - int(linear)):
            lj.addExclusion(index + i, index + from_i_to_bead(i + 1, n))
        index += n
    lj.setForceGroup(4)
    return lj


def confinement(
    N: int, xc: float, yc: float, zc: float, R: float, g: float = 1.0
) -> mm.CustomCompoundBondForce:
    """return a spherical confinement force.
    Parameters
    ----------
    N  : number of beads (int)
    xc : confinement center along x (float)
    yc : confinement center along y (float)
    zc : confinement center along z (float)
    R  : radius of the spherical confinement in nm (float)
    g  : strength of the spherical confinement in kBT (float)
    Returns
    -------
    OpenMM CustomCompoundBondForce to deal with spherical confinement.
    Raises
    ------
    """
    expression = "step(r-R_confinement)*g_confinement*(exp(r/R_confinement-1)-1);r=sqrt((x1-xc)*(x1-xc)+(y1-yc)*(y1-yc)+(z1-zc)*(z1-zc))"
    confinement = mm.CustomCompoundBondForce(1, expression)
    confinement.addGlobalParameter("R_confinement", R)
    confinement.addGlobalParameter("g_confinement", g)
    confinement.addGlobalParameter("xc", xc)
    confinement.addGlobalParameter("yc", yc)
    confinement.addGlobalParameter("zc", zc)
    for i in range(N):
        confinement.addBond([i])
    confinement.setForceGroup(5)
    return confinement


def wall_lj93(
    N_per_C: list,
    normal: str,
    position: float,
    epsilon: float,
    sigma: float,
    cutoff: float,
) -> mm.CustomCompoundBondForce:
    """return a wall/lj93 force.
    Parameters
    ----------
    N_per_C  : number of beads per chain (list of int)
    normal   : normal to the wall 'x', 'y', 'z' or '-x', '-y', '-z' (str)
    position : position of the wall (float)
    epsilon  : lj93 epsilon (float)
    sigma    : lj93 sigma (float)
    cutoff   : lj93 cutoff (float)
    Returns
    -------
    OpenMM CustomCompoundBondForce wall/lj93 force.
    Raises
    ------
    if 'normal' argument is not amongst 'x', 'y', 'z', '-x', '-y', '-z', return empty force.
    """
    if not normal in ["-x", "x", "-y", "y", "-z", "z"]:
        print(
            "normal to the wall is not amongst 'x', 'y', 'z', '-x', '-y', '-z', return empty force."
        )
        return mm.CustomCompoundBondForce()
    if "-" in normal:
        expression = (
            "epsilon_wall*((2/15)*(sigma_wall/d)^9-(sigma_wall/d)^3+sqrt(10)/3)*step(cutoff_wall-d);d="
            + str(position)
            + normal
            + "1"
        )
    else:
        expression = (
            "epsilon_wall*((2/15)*(sigma_wall/d)^9-(sigma_wall/d)^3+sqrt(10)/3)*step(cutoff_wall-d);d="
            + normal
            + "1-"
            + str(position)
        )
    # print("wall/lj93 expression",expression)
    wall = mm.CustomCompoundBondForce(1, expression)
    wall.addGlobalParameter("epsilon_wall", epsilon)
    wall.addGlobalParameter("sigma_wall", sigma)
    wall.addGlobalParameter("cutoff_wall", cutoff)
    index = 0
    for n in enumerate(N_per_C):
        for i in range(n):
            wall.addBond([index])
            index += 1
    wall.setForceGroup(6)
    return wall


def wall_lj126(
    N_per_C: list,
    normal: str,
    position: float,
    epsilon: float,
    sigma: float,
    cutoff: float,
) -> mm.CustomCompoundBondForce:
    """return a wall/lj126 force.
    Parameters
    ----------
    N_per_C  : number of beads per chain (list of int)
    normal   : normal to the wall 'x', 'y', 'z' or '-x', '-y', '-z' (str)
    position : position of the wall (float)
    epsilon  : lj126 epsilon (float)
    sigma    : lj126 sigma (float)
    cutoff   : lj126 cutoff (float)
    Returns
    -------
    OpenMM CustomCompoundBondForce wall/lj126 force.
    Raises
    ------
    if 'normal' argument is not amongst 'x', 'y', 'z', '-x', '-y', '-z', return empty force.
    """
    if not normal in ["-x", "x", "-y", "y", "-z", "z"]:
        print(
            "normal to the wall is not amongst 'x', 'y', 'z', '-x', '-y', '-z', return empty force."
        )
        return mm.CustomCompoundBondForce()
    if "-" in normal:
        expression = (
            "4.0*epsilon_wall*((sigma_wall/d)^12-(sigma_wall/d)^6+0.25)*step(cutoff_wall-d);d="
            + str(position)
            + normal
            + "1"
        )
    else:
        expression = (
            "4.0*epsilon_wall*((sigma_wall/d)^12-(sigma_wall/d)^6+0.25)*step(cutoff_wall-d);d="
            + normal
            + "1-"
            + str(position)
        )
    # print("wall/lj126 expression",expression)
    wall = mm.CustomCompoundBondForce(1, expression)
    wall.addGlobalParameter("epsilon_wall", epsilon)
    wall.addGlobalParameter("sigma_wall", sigma)
    wall.addGlobalParameter("cutoff_wall", cutoff)
    index = 0
    for n in N_per_C:
        for i in range(n):
            wall.addBond([index])
            index += 1
    wall.setForceGroup(7)
    return wall


def on_off_block_copolymer(context: mm.Context, on_off: str = "off"):
    """on/off the LJ block-copolymer interactions.
    Parameters
    ----------
    context : OpenMM context
    on_off  : turn on/off (str)
    Returns
    -------
    Raises
    ------
    """
    if on_off == "off" or on_off == "on":
        context.setParameter(
            "lj_on_off", 0 * int(on_off == "off") + 1 * int(on_off == "on")
        )
    else:
        print("Input parameter 'on_off' accepts only 'on' or 'off' as value.")


def read_binders(name: str = "", states: list = []) -> tuple:
    """read and create the binders from the file 'name'.
    the function cannot be called multiple time.
    the header of the file 'name' should be : binder number mass_amu sigma_nm.
    the file 'name' is like :
    first column 'name of the binder',
    second column 'number of binders',
    third column 'mass in amu',
    fourth column 'size of the binder in nanometer'.
    Parameters
    ----------
    name    : name of the file with the description of the binders (str)
    states  : name of epigenetic states along the chain(s) (list of str)
    Returns
    -------
    list (list of str) of states (chains+binders), mass of binders (list of float), size of each binder (list of float), color of each binder (int).
    Raises
    ------
    if you call this function before the chains have been created, do nothing.
    if the file has no header, print a warning.
    """
    if len(states) == 0:
        print("no chains have been created so far, do nothing.")
        print(
            "please create the chains first and then the binders that bind to the chains."
        )
        return [], [], [], []
    else:
        # read the file and add the binders parameters
        mass = []
        sizes = []
        states_to_int = {}
        index = 0
        colors = []
        states_with_binders = [s for s in states]
        try:
            with open(name, "r") as in_file:
                lines = in_file.readlines()
                # header ?
                sl = lines[0].split()
                if not (
                    sl[0] == "binder"
                    and sl[1] == "number"
                    and sl[2] == "mass_amu"
                    and sl[3] == "sigma_nm"
                ):
                    print(
                        name
                        + " does not have header, the header of the file 'name' should be : binder number mass_amu sigma_nm."
                    )
                # binders
                for l in lines[1:]:
                    sl = l.split()
                    # 4 arguments are expected
                    if len(sl) == 4:
                        if sl[0] not in states_to_int.keys():
                            states_to_int[sl[0]] = index
                            index += 1
                        # add the number of binders 'source', mass and size of the binder
                        for i in range(int(sl[1])):
                            states_with_binders.append(str(sl[0]))
                            mass.append(float(sl[2]))
                            sizes.append(float(sl[3]))
                            colors.append(states_to_int[sl[0]])
        except IOError:
            print("The function 'read_binders' did not find the file " + name)
        return states_with_binders, mass, sizes, colors


def read_states(name: str) -> tuple:
    """read epigenetic states from the file 'name' and return a list of the particle associated state (from 1 to N).
    the header of the file has to be 'chain monomer state'.
    the first column is the 'index of the chain', the second column is the 'index of the bead' and the third column is the 'state'.
    Parameters
    ----------
    name : name of the file with the description of the epigenetic states (str)
    Returns
    -------
    chain index (list of int), list of string that are the states of the N beads, list of int that are the colors of the N beads, list of beads per chain (list of int), number of chains (int), number of beads (int).
    Raises
    ------
    exit if the file 'name' is not found or if the header does not exist.
    """
    states = []
    states_to_int = {}
    colors = []
    chain_index = []
    chains = 0
    # beads per chain
    N_per_C = [1]
    index = 0
    try:
        with open(name, "r") as in_file:
            lines = in_file.readlines()
            # header ?
            sheader = lines[0].split()
            if not (
                sheader[0] == "chain"
                and sheader[1] == "monomer"
                and sheader[2] == "state"
            ):
                print("File " + name + " has no header, exit.")
                sys.exit()
            # colors and chain index, do not read the header
            for l in lines[1:]:
                sl = l.split()
                states.append(sl[2])
                if sl[2] not in states_to_int.keys():
                    states_to_int[sl[2]] = index
                    index += 1
                colors.append(states_to_int[sl[2]])
                chain_index.append(int(sl[0]))
            # beads per chain, do not read the header
            chain0 = int((lines[1].split())[0])
            for l in lines[2:]:
                chain1 = int((l.split())[0])
                if chain0 == chain1:
                    N_per_C[chains] += 1
                else:
                    N_per_C.append(1)
                    chains += 1
                chain0 = chain1
    except IOError:
        print(name, "has not been found.")
        sys.exit()
    return chain_index, states, colors, N_per_C, len(N_per_C), sum(N_per_C)


def read_interactions(name: str, Temp: float) -> list:
    """read the matrix of interactions and return a tuple [state 'r',state 's',interaction strength between r and s : D(r,s)].
    for the interaction strength D(r,s) between state 'r' and state 's' we consider only the 'r' < 's' (indeed D(r,s)=D(s,r)).
    first column is the state 'r', second column is the state 's', third column is the interaction strength in kBT.
    Parameters
    ----------
    name : name of the file with the description of the epigenetic interactions
    Temp : temperature of the system (float)
    Returns
    -------
    interaction strengthes between state 'r' and state 's' (N-tuple of [string,string,float])
    Raises
    ------
    exit if the file 'name' is not found.
    """
    interactions = []
    try:
        with open(name, "r") as in_file:
            lines = in_file.readlines()
            for l in lines:
                sl = l.split()
                interactions.append([sl[0], sl[1], float(sl[2]) * get_kBT(Temp)])
        I = len(interactions)
        # for the interaction strength D(r,s) between state 'r' and state 's' we consider only the 'r' < 's' (indeed D(r,s)=D(s,r))
        deja_vu = [False] * I
        for i in range(I):
            r1 = interactions[i][0]
            s1 = interactions[i][1]
            for j in range(i + 1, I):
                r2 = interactions[j][0]
                s2 = interactions[j][1]
                if (r1 == r2 and s1 == s2) or (r1 == s2 and s1 == r2):
                    deja_vu[j] = True
        # build the new interaction strengthes
        interactionstmp = []
        for i in range(I):
            if not deja_vu[i]:
                r1 = interactions[i][0]
                s1 = interactions[i][1]
                interactionstmp.append([r1, s1, interactions[i][2]])
    except IOError:
        print(name, "has not been found.")
        sys.exit()
    # print(type(interactionstmp))
    return interactionstmp


def positions_linear_or_ring(
    N_per_C: list,
    z_offset: list,
    sigma: float,
    binders_size: list,
    L: float,
    linear: bool,
) -> tuple:
    """return the positions of the bead, the linear or ring polymer fits in the box (with com in the middle).
    if the chains are linear, the system is along the z-axis.
    if there is multiple linear chains, the maximum contour length is used to fit the box size.
    if there is multiple linear chains, the offset of each chain is located on a cubic lattice.
    if there is multiple ring chains, the center of each ring is located on the z-axis.
    if the chain with bond size 'sigma' does not fit the box, sigma is decreased to sigma0 that fits the box.
    if binder 'i' is added to the system, its size is (sigma0/sigma)*binders_size[i].
    you have to use 'create_initial_conformation' function to grow from sigma0 to sigma and from (sigma0/sigma)*binders_size[i] to binders_size[i].
    Parameters
    ----------
    N_per_C      : number of beads for each chain (list of int)
    z_offset     : for each chain the bead that is located at z=L/2 (list of int)
    sigma        : size of the bond (float)
    binders_size : size of each binder (list of float)
    L            : size of the box (float)
    linear       : linear (True) or ring chain (False) (bool)
    Returns
    -------
    list of OpenMM.Vec3, the com is located at the center of the box and the bond size (float),
    bond size (float),
    dx between two chains (float),
    dy between two chains (float),
    dz between two beads (float).
    Raises
    ------
    """
    a_cutoff = np.power(2.0, 1.0 / 6.0)
    cutoff = a_cutoff * sigma
    # number of chains
    C = len(N_per_C)
    # number of beads
    N = sum(N_per_C)
    # positions
    if linear:
        # chains on a lattice, x*sqrt(C)+y
        sqrt_C = int(np.sqrt(C))
        while (sqrt_C * sqrt_C) < C:
            sqrt_C += 1
        # initial bond size for linear chain
        # Lx,Ly between two chains and bond size sigma(0)
        # (L-sigma(0))/N_bonds=sigma(0) gives si
        sigma0 = min(min(L / sqrt_C, L / max(N_per_C)), sigma)
        Lx = (L - sigma0) / sqrt_C
        Ly, Lz = Lx, sigma0
        for c, n in enumerate(N_per_C):
            z_offset_c = z_offset[c]
            # the linear chains are located on a lattice
            yy = c % sqrt_C
            xx = (c - yy) // sqrt_C
            xx *= Lx
            yy *= Ly
            if c == 0:
                positions = np.array(
                    [[xx, yy, (i - z_offset_c) * sigma0] for i in range(n)]
                )
            else:
                positions = np.concatenate(
                    (
                        positions,
                        np.array(
                            [[xx, yy, (i - z_offset_c) * sigma0] for i in range(n)]
                        ),
                    ),
                    axis=0,
                )
    else:
        # initial bond size for ring chain
        # contour length of the ring chain N_per_C*sigma0
        # angle between two beads 2*pi/N_per_C
        # R*sin(angle/2)=sigma0/2 with R<L/2
        # we want sigma0 the same for all the ring chains (angle is fixed by the number of beads in the chain)
        # therefore, only R can be changed
        sigma0 = min(sigma, L / (max(N_per_C) - 1 + 1))
        Lx, Ly, Lz = sigma0, sigma0, sigma0
        for c, n in enumerate(N_per_C):
            angle = 2.0 * np.pi / n
            R = 0.5 * sigma0 / np.sin(0.5 * angle)
            zc = 0.9 * L * c / C
            if c == 0:
                positions = np.array(
                    [
                        [R * np.cos(i * angle), R * np.sin(i * angle), zc]
                        for i in range(n)
                    ]
                )
            else:
                positions = np.concatenate(
                    (
                        positions,
                        np.array(
                            [
                                [R * np.cos(i * angle), R * np.sin(i * angle), zc]
                                for i in range(n)
                            ]
                        ),
                    ),
                    axis=0,
                )
    # com
    com = np.sum(positions, axis=0)
    # com of the chains to 0.5*L,0.5*L,0.5*L
    dx, dy, dz = 0.5 * L - com[0] / N, 0.5 * L - com[1] / N, 0.5 * L - com[2] / N
    positions = [mm.Vec3(p[0] + dx, p[1] + dy, p[2] + dz) for p in positions]
    # number of binders
    B = len(binders_size)
    if B > 0:
        # space partition
        l_blocks = int(L / (2.0 * sigma))
        s_block = 1.0 / (L / l_blocks)
        n_blocks = l_blocks * l_blocks * l_blocks
        l_in_block = [0] * n_blocks
        # beads per block estimation
        for p in positions:
            xi = int(p[0] * s_block)
            yi = int(p[1] * s_block)
            zi = int(p[2] * s_block)
            index = xi * l_blocks * l_blocks + yi * l_blocks + zi
            l_in_block[index] += 1
        i_per_block = 2 * max(l_in_block)
        i_in_block = [-1] * (n_blocks * i_per_block)
        l_in_block = [0] * n_blocks
        # chains and partition
        for i, p in enumerate(positions):
            xi = int(p[0] * s_block)
            yi = int(p[1] * s_block)
            zi = int(p[2] * s_block)
            index = xi * l_blocks * l_blocks + yi * l_blocks + zi
            i_in_block[index * i_per_block + l_in_block[index]] = i
            l_in_block[index] += 1
        # rescaled sigma
        sigmas = [sigma0] * N + [(sigma0 / sigma) * b for b in binders_size]
        # positions of the binders
        for i, s in enumerate(binders_size):
            overlap = True
            while overlap:
                # nothing between 0 and s, nothing between L-s and L
                bx = s + (L - 2 * s) * np.random.random()
                by = s + (L - 2 * s) * np.random.random()
                bz = s + (L - 2 * s) * np.random.random()
                # neighborhood of particle 'i'
                overlap = False
                xi = int(bx * s_block)
                yi = int(by * s_block)
                zi = int(bz * s_block)
                for x in range(max(0, xi - 1), min(xi + 1, l_blocks)):
                    for y in range(max(0, yi - 1), min(yi + 1, l_blocks)):
                        for z in range(max(0, zi - 1), min(zi + 1, l_blocks)):
                            index = x * l_blocks * l_blocks + y * l_blocks + z
                            for j in range(l_in_block[index]):
                                jj = i_in_block[index * i_per_block + j]
                                pj = positions[jj]
                                ax, ay, az = pj[0] - bx, pj[1] - by, pj[2] - bz
                                # overlap ?
                                inv_diameter2 = 1.0 / (
                                    np.power(2.0, 2.0 / 6.0)
                                    * 0.25
                                    * (sigmas[i] + sigmas[jj])
                                    * (sigmas[i] + sigmas[jj])
                                )
                                if (
                                    (ax * ax + ay * ay + az * az) * inv_diameter2
                                ) < 1.0:
                                    overlap = True
                                    break
            index = xi * l_blocks * l_blocks + yi * l_blocks + zi
            i_in_block[index * i_per_block + l_in_block[index]] = N + i
            l_in_block[index] += 1
            positions.append(mm.Vec3(bx, by, bz))
    return positions, sigma0, Lx, Ly, Lz


def v_gaussian(Temp: float, mass: list, seed: int, bcom: bool = False) -> list:
    """return a list of velocities (of size V) distributed according to the Boltzmann distribution with a com motion to 0,0,0.
    Parameters
    ----------
    Temp : temperature (float)
    mass : mass of the beads (list of float)
    seed : seed (int)
    bcom : center-of-mass motion to 0,0,0 ? (bool)
    Returns
    -------
    V-tuple of OpenMM.Vec3
    """
    np.random.seed(seed)
    kBT = get_kBT(Temp)
    # number of beads
    V = len(mass)
    # velocities according to Boltzmann distribution
    vt = np.array([np.sqrt(kBT / m) for m in mass]).reshape(V, 1)
    vxyz = np.multiply(vt, np.random.normal(0.0, 1.0, 3 * V).reshape(V, 3))
    # com motion
    sum_vxyz = np.multiply(1.0 / V, np.sum(vxyz, axis=0))
    # com motion to 0,0,0
    if bcom:
        vxyz = np.subtract(vxyz, sum_vxyz)
    # kinetic energy
    K = np.sum(np.square(vxyz))
    # rescaling of the kinetic energy
    mean_mass = sum(mass) / V
    rK = np.sqrt(1.5 * V * kBT / (0.5 * mean_mass * K))
    return [
        mm.Vec3(vxyz[v, 0] * rK, vxyz[v, 1] * rK, vxyz[v, 2] * rK) for v in range(V)
    ]


def draw_conformation(positions: np.ndarray, L: float, colors: list, name: str):
    """draw conformation using matplotlib.
    Parameters
    ----------
    positions : positions (in nm) of the beads (list)
    L         : box size in nm (float)
    colors    : a list of colors for each beads (list)
    name      : the name of the file (string)
    Returns
    -------
    Raises
    ------
    if positions and colors do not have the same length.
    """
    N, D = positions.shape
    C = len(colors)
    if C != N:
        print(
            "positions and colors do not have the same length "
            + str(N)
            + " and "
            + str(C)
            + ", do nothing."
        )
    else:
        inv_L = 1.0 / L
        px, py, pz = zip(
            *[(p[0] * inv_L, p[1] * inv_L, p[2] * inv_L) for p in positions]
        )
        plt.figure(1)
        ax = plt.axes(projection="3d")
        ax.set_xlim(0.0, 1.0)
        ax.set_ylim(0.0, 1.0)
        ax.set_zlim(0.0, 1.0)
        # ax.plot3D(px,py,pz,'gray')
        ax.scatter3D(px, py, pz, c=colors, cmap="rainbow", alpha=0.5)
        plt.savefig(name)
        plt.clf()
        plt.close("all")


def draw_sub_conformation(
    positions: np.ndarray, L: float, colors: list, part: list, name: str
):
    """draw a sub-conformation using matplotlib.
    Parameters
    ----------
    positions : positions (in nm) of the beads return by 'get_positions_from_context' function
    L         : box size in nm (float)
    colors    : a list of colors for each beads (list)
    part      : (list)
    name      : the name of the file (string)
    Returns
    -------
    Raises
    ------
    if positions and colors do not have the same length.
    """
    N, D = positions.shape
    P = len(part)
    C = len(colors)
    part_colors = []
    if C != N and P > 0:
        print("positions and colors do not have the same length, do nothing.")
    else:
        pp = np.zeros((P, 3))
        index = 0
        for n in range(N):
            if n in part:
                pp[index] = np.multiply(1.0 / L, positions[n, :3])
                part_colors.append(colors[n])
                index += 1
        plt.figure(1)
        ax = plt.axes(projection="3d")
        ax.set_xlim(0.0, 1.0)
        ax.set_ylim(0.0, 1.0)
        ax.set_zlim(0.0, 1.0)
        # ax.plot3D(pp[:,0],pp[:,1],pp[:,2],'gray')
        # ax.scatter3D(pp[:,0],pp[:,1],pp[:,2],c=part_colors,cmap='rainbow')
        ax.scatter3D(pp[:, 0], pp[:, 1], pp[:, 2], c=part_colors)
        plt.savefig(name)
        plt.clf()
        plt.close("all")


def write_draw_conformation(
    positions: np.ndarray,
    norm_p: float = 1.0,
    xyz_lim: list = [0.0, 2.0, 0.0, 2.0, 0.0, 2.0],
    name_write: str = "write.out",
    name_draw: str = "draw.png",
    mode: str = "write_draw",
):
    """write and/or draw conformation (in nanometer) using matplotlib to file name.out/png.
    write x y z for the first position, then go to a new line and write x y z for the second position, ...
    Parameters
    ----------
    positions  : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    norm_p     : normalization factor (in nanometer) for the positions (float)
    xyz_lim    : x,y, and z range (list of float)
    name_write : name of the file to write (str)
    name_draw  : name of the file to draw (str)
    mode       : a string that contains write and/or draw (str)
    Returns
    -------
    Raises
    ------
    """
    # write
    if "write" in mode:
        with open(name_write, "w") as out_file:
            for p in positions:
                out_file.write(str(p[0]) + " " + str(p[1]) + " " + str(p[2]) + "\n")
    # draw
    if "draw" in mode:
        N, D = positions.shape
        positions = np.multiply(1.0 / norm_p, positions)
        px, py, pz = zip(*[(p[0], p[1], p[2]) for p in positions])
        fig = plt.figure("draw")
        ax = fig.add_subplot(111, projection="3d")
        ax.scatter(
            px,
            py,
            pz,
            c=[float(p) / float(N) for p in range(N)],
            s=1.5,
            cmap="rainbow",
            alpha=0.5,
        )
        ax.set_xlim(xyz_lim[0], xyz_lim[1])
        ax.set_ylim(xyz_lim[2], xyz_lim[3])
        ax.set_zlim(xyz_lim[4], xyz_lim[5])
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")
        fig.savefig(name_draw)
        fig.clf()
        plt.close("draw")


def read_write_checkpoint(context: mm.Context, name: str, mode: str) -> int:
    """read/write from/a OpenMM checkpoint file.
    Parameters
    ----------
    context : OpenMM context
    name    : name of the checkpoint file (string)
    mode    : read or wirte (string)
    Returns:
    --------
    1 if the function find or write the file, 0 otherwize.
    Raises
    ------
    if no checkpoint file has been found in read mode.
    if mode is not equal to read or to write.
    """
    if mode == "read":
        try:
            with open(name, "rb") as in_file:
                context.loadCheckpoint(in_file.read())
                return 1
        except IOError:
            print("no checkpoint file found : " + name)
            return 0
    elif mode == "write":
        with open(name, "wb") as out_file:
            out_file.write(context.createCheckpoint())
        return 1
    else:
        print("Please consider using read or write option.")
        return 0


def create_integrator(
    thermostat: str,
    Temp: float,
    dt: float,
    dof: int,
    tau: float,
    seed: int = 1,
    rlimit: bool = False,
    vlimit: bool = False,
    rescale: bool = False,
):
    """return a integrator with spatial step limit and/or rescaling of the velocities.
    the 'langevin' option for the input variable 'thermostat' does not work with input variables 'limit' and 'rescale'.
    Parameters
    ----------
    thermostat : Gronech-Jensen-Farago ("gjf"), global thermostat ("global"),  "langevin" or "nve"
    Temp       : temperature (float)
    dt         : the time step in picosecond (float)
    dof        : degrees of freedom (int)
    tau        : inverse of the coupling frequency to the thermostat (float)
    seed       : seed for the integrator (int)
    rlimit     : limit for the ||x(t+dt)-x(t)|| (False or True) (bool)
    vlimit     : limit for the velocity such that ||x(t+dt)-x(t)|| is of length xmax (False or True) (bool)
    rescale    : scale the kinetic average to its thermal average value (False or True)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    if thermostat is not equal to "nve" or "gjf" or "langevin".
    """
    kBT = get_kBT(Temp)
    if thermostat == "langevin":
        integrator = mm.LangevinIntegrator(Temp, 1.0 / tau, dt)
        integrator.setRandomNumberSeed(seed)
        integrator.setConstraintTolerance(0.00001)
    elif thermostat == "gjf":
        integrator = gjf_integrator(dt, dof, Temp, tau, seed, rlimit, vlimit, rescale)
        integrator.setConstraintTolerance(0.00001)
    elif thermostat == "global":
        integrator = global_integrator(dt, dof, Temp, tau, seed, rlimit, rescale)
        integrator.setConstraintTolerance(0.00001)
    elif thermostat == "nve":
        integrator = verlet_integrator(dt, dof, Temp, rlimit, vlimit, rescale)
        integrator.setConstraintTolerance(0.00001)
    else:
        print(
            "Please consider using 'nve', 'langevin' or 'gjf' for input 'thermostat' variable."
        )
        print(
            "'thermostat' takes the value 'langevin' with 'Temp=300.0', 'tau=1.0' and 'dt=0.001'."
        )
        integrator = mm.LangevinIntegrator(300.0, 1.0, 0.001)
        integrator.setRandomNumberSeed(1)
        integrator.setConstraintTolerance(0.00001)
    return integrator


def gjf_integrator(
    dt: float,
    dof: int,
    Temp: float = 300.0,
    tau: float = 1.0,
    seed: int = 1,
    rlimit: bool = False,
    vlimit: bool = False,
    rescale: bool = False,
) -> mm.CustomIntegrator:
    """return a Gronbech-Jensen-Farago (GJF) Langevin integrator with spatial step limit and rescaling of the velocities.
    Parameters
    ----------
    dt      : the time step (float)
    dof     : degrees of freedom (int)
    Temp    : temperature (float)
    tau     : inverse of the coupling frequency to the thermostat (float)
    seed    : seed for the integrator (int)
    rlimit  : limit for the displacement ||x(t+dt)-x(t)|| (False or True) (bool)
    vlimit  : limit for the velocity such that ||x(t+dt)-x(t)|| is length xmax (False or True) (bool)
    rescale : rescaling of the kinetic energy (False or True) (bool)
    Returns
    -------
    OpenMM CustomIntegrator
    Raises
    ------
    References
    ----------
    Niels Gronbech-Jensen and Oded Farago.
    A simple and effective Verlet-type algorithm for simulating Langevin dynamics.
    https://doi.org/10.1080/00268976.2012.760055
    """
    kBT = get_kBT(Temp)
    integrator = mm.CustomIntegrator(dt)
    integrator.setRandomNumberSeed(seed)
    # global variables
    integrator.addGlobalVariable("rlimit", int(rlimit))
    integrator.addGlobalVariable("vlimit", int(vlimit))
    integrator.addGlobalVariable("rescale", int(rescale))
    integrator.addGlobalVariable("tau", tau)
    integrator.addGlobalVariable("kBT", kBT)
    integrator.addGlobalVariable("mke", dof * 0.5 * kBT)
    integrator.addGlobalVariable("ke", 0.0 * kBT)
    integrator.addGlobalVariable("eratio", 0.0)
    # per-dof variables
    integrator.addPerDofVariable("f_n", 0.0)
    integrator.addPerDofVariable("beta_n_1", 0.0)
    integrator.addPerDofVariable("x1", 0.0)
    integrator.addGlobalVariable("a_gjf", 0.0)
    integrator.addGlobalVariable("b_gjf", 0.0)
    integrator.addPerDofVariable("x_sigma_gjf", 0.0)
    integrator.addPerDofVariable("dx_lim", 0.0)
    # compute global coefficients
    integrator.addComputeGlobal("b_gjf", "1/(1+dt/(2*tau))")
    integrator.addComputeGlobal("a_gjf", "(1-dt/(2*tau))*b_gjf")
    # integrator steps
    integrator.addUpdateContextState()
    integrator.addComputePerDof("f_n", "f")
    # integrator.addComputePerDof("beta_n_1","gaussian")
    # integrator.addComputePerDof("x_sigma_gjf","sqrt(kBT*dt/(2*m*tau))")
    integrator.addComputePerDof("x_sigma_gjf", "sqrt(kBT*dt/(2*m*tau))*gaussian")
    # do we limit the displacement to the time-step multiply by the thermic velocity ?
    integrator.beginIfBlock("rlimit=0")
    # integrator.addComputePerDof("x","x+b_gjf*dt*(v+dt*f_n/(2*m)+x_sigma_gjf*beta_n_1)")
    integrator.addComputePerDof("x", "x+b_gjf*dt*(v+dt*f_n/(2*m)+x_sigma_gjf)")
    integrator.endBlock()
    integrator.beginIfBlock("rlimit=1")
    integrator.addComputePerDof("dx_lim", "dt*sqrt(kBT/m)")
    # integrator.addComputePerDof("x","x+max(-dx_lim,min(dx_lim,b_gjf*dt*(v+dt*f_n/(2*m)+x_sigma_gjf*beta_n_1)))")
    integrator.addComputePerDof(
        "x", "x+max(-dx_lim,min(dx_lim,b_gjf*dt*(v+dt*f_n/(2*m)+x_sigma_gjf)))"
    )
    integrator.endBlock()
    integrator.addComputePerDof("x1", "x")
    integrator.addConstrainPositions()
    # velocities
    integrator.beginIfBlock("vlimit=0")
    # integrator.addComputePerDof("v","a_gjf*v+(dt/(2*m))*(a_gjf*f_n+f)+b_gjf*2*x_sigma_gjf*beta_n_1+(x-x1)/dt")
    integrator.addComputePerDof(
        "v", "a_gjf*v+(dt/(2*m))*(a_gjf*f_n+f)+b_gjf*2*x_sigma_gjf+(x-x1)/dt"
    )
    integrator.endBlock()
    integrator.beginIfBlock("vlimit=1")
    integrator.addComputePerDof("dx_lim", "dt*sqrt(kBT/m)")
    # integrator.addComputePerDof("v","max(-dx_lim/dt,min(dx_lim/dt,a_gjf*v+(dt/(2*m))*(a_gjf*f_n+f)+b_gjf*2*x_sigma_gjf*beta_n_1+(x-x1)/dt))")
    integrator.addComputePerDof(
        "v",
        "max(-dx_lim/dt,min(dx_lim/dt,a_gjf*v+(dt/(2*m))*(a_gjf*f_n+f)+b_gjf*2*x_sigma_gjf+(x-x1)/dt))",
    )
    integrator.endBlock()
    integrator.addConstrainVelocities()
    # rescale the velocities ?
    integrator.beginIfBlock("rescale=1")
    integrator.addComputeSum("ke", "0.5*m*v*v")
    integrator.addComputeGlobal("eratio", "sqrt(mke/ke)")
    integrator.addComputePerDof("v", "v*eratio")
    integrator.endBlock()
    # for s in range(40):
    #     try:
    #         print(integrator.getComputationStep(s))
    #     except:
    #         print("no computation step "+str(s))
    return integrator


def old_gjf_integrator(
    dt: float,
    dof: int,
    Temp: float = 300.0,
    tau: float = 1.0,
    seed: int = 1,
    limit: bool = False,
    rescale: bool = False,
) -> mm.CustomIntegrator:
    """return a Gronbech-Jensen-Farago (GJF) Langevin integrator with spatial step limit and rescaling of the velocities.
    Parameters
    ----------
    dt      : the time step (float)
    dof     : degrees of freedom (int)
    Temp    : temperature (float)
    tau     : inverse of the coupling frequency to the thermostat (float)
    seed    : seed for the integrator (int)
    limit   : limit for the displacement ||x(t+dt)-x(t)|| (False or True) (bool)
    rescale : rescaling of the kinetic energy (False or True) (bool)
    Returns
    -------
    OpenMM CustomIntegrator
    Raises
    ------
    References
    ----------
    Niels Gronbech-Jensen and Oded Farago.
    A simple and effective Verlet-type algorithm for simulating Langevin dynamics.
    https://doi.org/10.1080/00268976.2012.760055
    """
    kBT = get_kBT(Temp)
    integrator = mm.CustomIntegrator(dt)
    integrator.setRandomNumberSeed(seed)
    # global variables
    integrator.addGlobalVariable("rlimit", int(limit))
    integrator.addGlobalVariable("rescale", int(rescale))
    integrator.addGlobalVariable("tau", tau)
    integrator.addGlobalVariable("kBT", kBT)
    integrator.addGlobalVariable("mke", dof * 0.5 * kBT)
    integrator.addGlobalVariable("ke", 0.0 * kBT)
    integrator.addGlobalVariable("eratio", 0.0)
    # per-dof variables
    integrator.addPerDofVariable("f_n", 0.0)
    integrator.addPerDofVariable("beta_n_1", 0.0)
    integrator.addPerDofVariable("x1", 0.0)
    integrator.addPerDofVariable("alpha_gjf", 0.0)
    integrator.addPerDofVariable("a_gjf", 0.0)
    integrator.addPerDofVariable("b_gjf", 0.0)
    integrator.addPerDofVariable("x_sigma_gjf", 0.0)
    # integrator.addPerDofVariable("v_sigma_gjf",0.0)
    integrator.addPerDofVariable("dx_lim", 0.0)
    # integrator steps
    integrator.addUpdateContextState()
    integrator.addComputePerDof("beta_n_1", "gaussian")
    integrator.addComputePerDof("f_n", "f")
    # compute coefficients
    integrator.addComputePerDof("alpha_gjf", "m/tau")
    integrator.addComputePerDof("b_gjf", "1/(1+dt*alpha_gjf/(2*m))")
    # integrator.addComputePerDof("a_gjf","(1-dt*alpha_gjf/(2*m))/(1+dt*alpha_gjf/(2*m))")
    integrator.addComputePerDof("a_gjf", "(1-dt*alpha_gjf/(2*m))*b_gjf")
    # integrator.addComputePerDof("x_sigma_gjf","sqrt(alpha_gjf*kBT*dt/(2*m*m))")
    integrator.addComputePerDof("x_sigma_gjf", "sqrt(0.5*alpha_gjf*kBT*dt)/m")
    # # integrator.addComputePerDof("v_sigma_gjf","sqrt(2*alpha_gjf*kBT*dt/(m*m))")
    # integrator.addComputePerDof("v_sigma_gjf","2*x_sigma_gjf")
    # do we limit the displacement to the time-step multiply by the thermic velocity ?
    integrator.beginIfBlock("rlimit=1")
    integrator.addComputePerDof("dx_lim", "dt*sqrt(kBT/m)")
    integrator.addComputePerDof(
        "x", "x+max(-dx_lim,min(dx_lim,b_gjf*dt*(v+dt*f_n/(2*m)+x_sigma_gjf*beta_n_1)))"
    )
    integrator.endBlock()
    integrator.beginIfBlock("rlimit=0")
    integrator.addComputePerDof("x", "x+b_gjf*dt*(v+dt*f_n/(2*m)+x_sigma_gjf*beta_n_1)")
    integrator.endBlock()
    integrator.addComputePerDof("x1", "x")
    integrator.addConstrainPositions()
    # velocities
    # integrator.addComputePerDof("v","a_gjf*v+(dt/(2*m))*(a_gjf*f_n+f)+b_gjf*v_sigma_gjf*beta_n_1+(x-x1)/dt")
    integrator.addComputePerDof(
        "v", "a_gjf*v+(dt/(2*m))*(a_gjf*f_n+f)+b_gjf*2*x_sigma_gjf*beta_n_1+(x-x1)/dt"
    )
    integrator.addConstrainVelocities()
    # rescale the velocities ?
    integrator.beginIfBlock("rescale=1")
    integrator.addComputeSum("ke", "0.5*m*v*v")
    integrator.addComputeGlobal("eratio", "sqrt(mke/ke)")
    integrator.addComputePerDof("v", "v*eratio")
    integrator.endBlock()
    # for s in range(40):
    #     try:
    #         print(integrator.getComputationStep(s))
    #     except:
    #         print("no computation step "+str(s))
    return integrator


def verlet_integrator(
    dt: float,
    dof: int,
    Temp: float,
    rlimit: bool = False,
    vlimit: bool = False,
    rescale: bool = False,
) -> mm.CustomIntegrator:
    """create and return a Velocity-Verlet integrator with spatial step limit and rescaling of the velocities
    Parameters
    ----------
    dt      : time step in picosecond (float)
    dof     : degrees of freedom (int)
    Temp    : temperature (float)
    rlimit  : limit for the displacement ||x(t+dt)-x(t)|| (False or True)
    vlimit  : limit for the velocity such that ||x(t+dt)-x(t)|| is length xmax (False or True) (bool)
    rescale : rescaling of the kinetic energy (False or True)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    """
    if rlimit or vlimit or rescale:
        kBT = get_kBT(Temp)
        integrator = mm.CustomIntegrator(dt)
        # global variables
        integrator.addGlobalVariable("kBT", kBT)
        integrator.addGlobalVariable("mke", dof * 0.5 * kBT)
        integrator.addGlobalVariable("ke", 0.0 * kBT)
        integrator.addGlobalVariable("eratio", 0.0)
        integrator.addGlobalVariable("rlimit", int(rlimit))
        integrator.addGlobalVariable("vlimit", int(vlimit))
        integrator.addGlobalVariable("rescale", int(rescale))
        # per-dof variables
        integrator.addPerDofVariable("dx_lim", 0.0)
        integrator.addPerDofVariable("x1", 0.0)
        # computation steps
        integrator.addUpdateContextState()
        integrator.addComputePerDof("v", "v+0.5*dt*f/m")
        # do we limit the displacement ?
        integrator.beginIfBlock("rlimit=0")
        integrator.addComputePerDof("x", "x+dt*v")
        integrator.endBlock()
        integrator.beginIfBlock("rlimit=1")
        integrator.addComputePerDof("dx_lim", "dt*sqrt(kBT/m)")
        integrator.addComputePerDof("x", "x+max(-dx_lim,min(dx_lim,dt*v))")
        integrator.endBlock()
        integrator.addComputePerDof("x1", "x")
        integrator.addConstrainPositions()
        # velocities
        integrator.beginIfBlock("vlimit=0")
        integrator.addComputePerDof("v", "v+0.5*dt*f/m+(x-x1)/dt")
        integrator.endBlock()
        integrator.beginIfBlock("vlimit=1")
        integrator.addComputePerDof("dx_lim", "dt*sqrt(kBT/m)")
        integrator.addComputePerDof(
            "v", "max(-dx_lim/dt,min(dx_lim/dt,v+0.5*dt*f/m+(x-x1)/dt))"
        )
        integrator.endBlock()
        integrator.addConstrainVelocities()
        integrator.beginIfBlock("rescale=1")
        integrator.addComputeSum("ke", "0.5*m*v*v")
        integrator.addComputeGlobal("eratio", "sqrt(mke/ke)")
        integrator.addComputePerDof(
            "v",
            "v*(step(0.01-abs(1.0-eratio*eratio))+(1-step(0.01-abs(1.0-eratio*eratio)))*eratio)",
        )
        integrator.endBlock()
    else:
        integrator = mm.VerletIntegrator(dt)
    return integrator


def global_integrator(
    dt: float, dof: int, Temp: float, tau: float, m: float, limit: bool, rescale: bool
) -> mm.CustomIntegrator:
    """create and return a global thermostat integrator with spatial step limit and rescaling of the velocities
    Parameters
    ----------
    dt      : time step in picosecond (float)
    tau     : inverse of the coupling frequency to the thermostat (float)
    dof     : degrees of freedom (int)
    Temp    : temperature of the system (float)
    m       : mass of the beads (float)
    limit   : limit for the displacement ||x(t+dt)-x(t)|| (False or True) (bool)
    rescale : rescaling of the kinetic energy (False or True) (bool)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    References
    ----------
    Giovanni Bussi and Michele Parrinello.
    Stochastics thermostats : comparison of local and global schemes.
    http://dx.doi.org/10.1016/j.cpc.2008.01.006
    """
    kBT = get_kBT(Temp)
    # 3 particles per bead : bead (mass), virtual (mass=0) and pseudo-u bead (mass)
    integrator = mm.CustomIntegrator(dt)
    integrator.addGlobalVariable("rlimit", int(limit))
    integrator.addGlobalVariable("rescale", int(rescale))
    integrator.addGlobalVariable("c_global", np.exp(-dt / tau))
    integrator.addGlobalVariable("dof_global", dof)
    integrator.addGlobalVariable("Temp", Temp)
    integrator.addGlobalVariable("kBT", kBT)
    integrator.addGlobalVariable("mke", dof * 0.5 * kBT)
    integrator.addGlobalVariable("ke", 0.0 * kBT)
    integrator.addGlobalVariable("eratio", 0.0)
    integrator.addGlobalVariable("alpha", 0.0)
    integrator.addGlobalVariable("sign_alpha", 0)
    integrator.addGlobalVariable("R_global", 0.0)
    integrator.addGlobalVariable("S_global", 0.0)
    integrator.addPerDofVariable("dx_lim_global", 0.0)
    integrator.addPerDofVariable("x1", 0.0)
    # global thermostat coefficients
    # "S_global" is approximate because we suppose "dof_global" -> inf
    integrator.addComputeSum("ke", "0.5*m*v*v")
    integrator.addComputeGlobal("R_global", "gaussian")
    integrator.addComputeGlobal(
        "S_global", "sqrt(2*(dof_global-1))*gaussian+(dof_global-1)"
    )
    integrator.addComputeGlobal(
        "alpha",
        "sqrt(c_global+((1-c_global)*(S_global+R_global*R_global)*mke)/(dof_global*ke)+2*R_global*sqrt(c_global*(1-c_global)*mke/(dof_global*ke)))",
    )
    integrator.addComputeGlobal(
        "sign_alpha",
        "2*step(R_global+sqrt(c_global*dof_global*ke/((1-c_global)*mke)))-1",
    )
    # momenta
    integrator.addComputePerDof("v", "sign_alpha*alpha*v")
    # velocity-Verlet
    integrator.addUpdateContextState()
    integrator.addComputePerDof("v", "v+0.5*dt*f/m")
    # limit the displacement ?
    integrator.beginIfBlock("rlimit=1")
    integrator.addComputePerDof("dx_lim_global", "dt*sqrt(kBT/m)")
    integrator.addComputePerDof("x", "x+max(-dx_lim_global,min(dx_lim_global,dt*v))")
    integrator.endBlock()
    integrator.beginIfBlock("rlimit=0")
    integrator.addComputePerDof("x", "x+dt*v")
    integrator.endBlock()
    # constraints
    integrator.addComputePerDof("x1", "x")
    integrator.addConstrainPositions()
    integrator.addComputePerDof("v", "v+0.5*dt*f/m+(x-x1)/dt")
    integrator.addConstrainVelocities()
    # rescale ?
    integrator.beginIfBlock("rescale=1")
    integrator.addComputeSum("ke", "0.5*m*v*v")
    integrator.addComputeGlobal("eratio", "sqrt(mke/ke)")
    integrator.addComputePerDof("v", "v*eratio")
    integrator.endBlock()
    return integrator


def correction_flying_ice_cube(context: mm.Context) -> list:
    """the com motion is move to 0,0,0 as-well-as the inertia of the system.
    Parameters
    ----------
    context : OpenMM context (mm.Context)
    Returns
    -------
    the velocities that have been corrected (list of mm.Vec3).
    Raises
    ------
    References
    ----------
    Stephen C. Harvey, Robert K.-Z. Tan, Thomas E. Cheatham III.
    The flying ice cube : Velocity rescaling in molecular dynamics leads to violation of energy equipartition.
    https://doi.org/10.1002/(SICI)1096-987X(199805)19:7%3C726::AID-JCC4%3E3.0.CO;2-S
    The global thermostat can lead to the "flying-ice-cube" phenomena.
    We have to cancel the first (com motion) and second (inertia) momenta to minimize the effect.
    """
    velocities = context.getState(getVelocities=True).getVelocities()
    positions = context.getState(
        getPositions=True, enforcePeriodicBox=False
    ).getPositions()
    punit = positions[0][0]
    vunits = velocities[0][0]
    N = len(positions)
    # com and com motion
    xcom, ycom, zcom = 0.0, 0.0, 0.0
    vxcom, vycom, vzcom = 0.0, 0.0, 0.0
    for p, v in zip(positions, velocities):
        xcom += p[0]
        ycom += p[1]
        zcom += p[2]
        vxcom += v[0]
        vycom += v[1]
        vzcom += v[2]
    xcom /= N
    ycom /= N
    zcom /= N
    vxcom /= N
    vycom /= N
    vzcom /= N
    # linear velocity com to 0,0,0
    new_velocities = [
        mm.Vec3(v[0] - vxcom, v[1] - vycom, v[2] - vzcom) for v in velocities
    ]
    # inertia
    I3 = [0.0] * 9
    for p in positions:
        I3[0] += (p[1] - ycom) * (p[1] - ycom) + (p[2] - zcom) * (p[2] - zcom)
        I3[1] -= (p[1] - ycom) * (p[0] - xcom)
        I3[2] -= (p[2] - zcom) * (p[0] - xcom)
        I3[4] += (p[0] - xcom) * (p[0] - xcom) + (p[2] - zcom) * (p[2] - zcom)
        I3[5] -= (p[2] - zcom) * (p[0] - ycom)
        I3[8] += (p[0] - xcom) * (p[0] - xcom) + (p[1] - ycom) * (p[1] - ycom)
    I3[3] = I3[1]
    I3[6] = I3[2]
    I3[7] = I3[5]
    # inertia inverse
    inv_I3 = [0.0] * 9
    inv_I3[0] = I3[5] * I3[5] - I3[4] * I3[8]
    inv_I3[1] = I3[1] * I3[8] - I3[2] * I3[5]
    inv_I3[2] = I3[2] * I3[4] - I3[1] * I3[5]
    inv_I3[3] = inv_I3[1]
    inv_I3[4] = I3[2] * I3[2] - I3[0] * I3[8]
    inv_I3[5] = I3[0] * I3[5] - I3[1] * I3[2]
    inv_I3[6] = inv_I3[2]
    inv_I3[7] = inv_I3[5]
    inv_I3[8] = I3[1] * I3[1] - I3[0] * I3[4]
    d0 = (
        I3[0] * I3[5] * I3[5]
        + I3[1] * I3[1] * I3[8]
        - 2.0 * I3[1] * I3[2] * I3[5]
        + I3[2] * I3[2] * I3[4]
        - I3[0] * I3[4] * I3[8]
    )
    if d0 != 0.0:
        # angular momentum
        dx, dy, dz = 0.0, 0.0, 0.0
        for p, v in zip(positions, new_velocities):
            dx += (p[1] - ycom) * v[2] - (p[2] - zcom) * v[1]
            dy += (p[2] - zcom) * v[0] - (p[0] - xcom) * v[2]
            dz += (p[0] - xcom) * v[1] - (p[1] - ycom) * v[0]
        # w=I^-1l
        wx = (inv_I3[0] * dx + inv_I3[1] * dy + inv_I3[2] * dz) / d0
        wy = (inv_I3[3] * dx + inv_I3[4] * dy + inv_I3[5] * dz) / d0
        wz = (inv_I3[6] * dx + inv_I3[7] * dy + inv_I3[8] * dz) / d0
        return [
            mm.Vec3(
                v[0] - (wy * (p[2] - zcom) - wz * (p[1] - ycom)),
                v[1] - (wz * (p[0] - xcom) - wx * (p[2] - zcom)),
                v[2] - (wx * (p[1] - ycom) - wy * (p[0] - xcom)),
            )
            for p, v in zip(positions, new_velocities)
        ]
    else:
        return new_velocities


def get_positions_from_context(
    context: mm.Context, start: int, end: int, PBC: bool = False
) -> np.ndarray:
    """return the positions (in nanometer) from the OpenMM context.
    Parameters
    ----------
    context : OpenMM context
    start   : keep particles from 'start' to 'end' [start,end[ (int)
    end     : keep particles from 'start' to 'end' [start,end[ (int)
    PBC     : enforce periodic box ? (bool)
    Returns
    -------
    return the positions from start to end (exclusive) as np.ndarray.
    Raises
    ------
    """
    pbuffer = context.getState(getPositions=True, enforcePeriodicBox=PBC).getPositions()
    inv_nm = 1.0 / unit.nanometer
    positions = np.zeros((end - start, 3))
    for i in range(start, end):
        positions[i - start, :3] = (
            pbuffer[i][0] * inv_nm,
            pbuffer[i][1] * inv_nm,
            pbuffer[i][2] * inv_nm,
        )
    return positions


def get_velocities_from_context(
    context: mm.Context, start: int, end: int, PBC: bool = False
) -> np.ndarray:
    """return the velocities (in nanometer per picosecond) from the OpenMM context.
    Parameters
    ----------
    context : OpenMM context
    start   : keep particles from 'start' to 'end' [start,end[ (int)
    end     : keep particles from 'start' to 'end' [start,end[ (int)
    PBC     : enforce periodic box ? (bool)
    """
    vbuffer = context.getState(
        getVelocities=True, enforcePeriodicBox=PBC
    ).getVelocities()
    inv_nm_per_ps = 1.0 / (unit.nanometer / unit.picosecond)
    velocities = np.zeros((end - start, 3))
    for i in range(start, end):
        velocities[i - start, :3] = (
            vbuffer[i][0] * inv_nm_per_ps,
            vbuffer[i][1] * inv_nm_per_ps,
            vbuffer[i][2] * inv_nm_per_ps,
        )
    return velocities


def get_platforms(pname: str = "CPU1") -> tuple:
    """return the platform index and device index corresponding to the name of the hardware you would like to use.
    Write it to a json file 'get_platforms.json'.
    The name of the hardware is like 'GeForce_GTX_980_Ti_OpenCL' (OpenCL implementation).
    If the name of the hardware is like 'GeForce_GTX_980_GeForce_GTX_780_OpenCL' we ask for multiple devices.
    Do not forget to add '_OpenCL' or '_CUDA' to ask for the OpenCL implementation whatever it is CPU or GPU.
    Parameters
    ----------
    pname : name of the hardware (str)
    Returns
    -------
    index of the platform, index of the device (int,int).
    Raises
    ------
    exit if no platform and/or device (with name!='') have been found.
    """
    platform_and_device = {}
    index_of_platform = -1
    index_of_device = -1
    # look for the OpenCLPlatformIndex and GPU
    Np = []
    for d in range(10):
        for p in range(10):
            integrator_test = mm.VerletIntegrator(0.001)
            system_test = mm.System()
            system_test.addParticle(1.0)
            platform_test = mm.Platform.getPlatformByName("OpenCL")
            try:
                context_test = mm.Context(
                    system_test,
                    integrator_test,
                    platform_test,
                    {"OpenCLPlatformIndex": str(p), "DeviceIndex": str(d)},
                )
                bcontext = True
            except:
                bcontext = False
            if bcontext:
                devices_names = context_test.getPlatform().getPropertyValue(
                    context_test, "DeviceName"
                )
                if "GeForce" in devices_names or "Tesla" in devices_names:
                    if p not in Np:
                        Np.append(p)
                del context_test
            del integrator_test
            del system_test
            del platform_test
    # loop over implementations
    for c in ["CPU", "CUDA", "OpenCL"]:
        platform_and_device[c] = {}
        # loop over platforms
        if c == "OpenCL":
            Np_imp = Np
        else:
            Np_imp = range(10)
        for p in Np_imp:
            # loop over devices
            for d in range(10):
                integrator_test = mm.VerletIntegrator(0.001)
                system_test = mm.System()
                system_test.addParticle(1.0)
                platform_test = mm.Platform.getPlatformByName(c)
                bcontext = True
                if c == "CPU":
                    context_test = mm.Context(
                        system_test, integrator_test, platform_test, {"CpuThreads": "1"}
                    )
                elif c == "CUDA":
                    try:
                        context_test = mm.Context(
                            system_test,
                            integrator_test,
                            platform_test,
                            {"DeviceIndex": str(d)},
                        )
                    except:
                        bcontext = False
                elif c == "OpenCL":
                    if pname.count("GeForce") > 1 and d < 2:
                        try:
                            context_test = mm.Context(
                                system_test,
                                integrator_test,
                                platform_test,
                                {"OpenCLPlatformIndex": str(p), "DeviceIndex": "0,1"},
                            )
                        except:
                            bcontext = False
                    else:
                        try:
                            context_test = mm.Context(
                                system_test,
                                integrator_test,
                                platform_test,
                                {"OpenCLPlatformIndex": str(p), "DeviceIndex": str(d)},
                            )
                        except:
                            bcontext = False
                if bcontext:
                    if "DeviceName" in context_test.getPlatform().getPropertyNames():
                        if pname.count("GeForce") > 1:
                            current_DeviceName = (
                                (
                                    context_test.getPlatform().getPropertyValue(
                                        context_test, "DeviceName"
                                    )
                                )
                                .replace(" ", "_")
                                .replace(",", "_")
                            )
                        else:
                            current_DeviceName = (
                                context_test.getPlatform().getPropertyValue(
                                    context_test, "DeviceName"
                                )
                            ).replace(" ", "_")
                        # add info to dict
                        platform_and_device[c][current_DeviceName] = {}
                        for n in context_test.getPlatform().getPropertyNames():
                            platform_and_device[c][current_DeviceName][
                                n
                            ] = context_test.getPlatform().getPropertyValue(
                                context_test, n
                            )
                        # if the current device is what we ask for ...
                        if (current_DeviceName + "_" + c) == pname:
                            index_of_platform = p
                            index_of_device = d
                    del context_test
                del integrator_test
                del system_test
                del platform_test
    with open("get_platforms.json", "w") as out_file:
        json.dump(platform_and_device, out_file, indent=0)
    if (index_of_platform == -1 or index_of_device == -1) and pname != "":
        print("No platform has been found : " + pname)
        sys.exit()
    return index_of_platform, index_of_device


def create_platform(platform_name: str = "CPU2", precision: str = "double"):
    """create and return platform and properties
    Parameters
    ----------
    platform_name : 'CPU2' to run on CPU with two threads
                    'GeForce_GTX_780_OpenCL' to run on GeForce GTX 780 with OpenCL implementation
                    to use two GPUs with OpenCL implementation : 'GeForce_RTX_2080_GeForce_GTX_1080_OpenCL' (str)
    precision     : single, mixed or double (str)
    Returns
    -------
    OpenMM platform, OpenMM properties
    Raises
    ------
    """
    if (
        "CPU" in platform_name
        and not "OpenCL" in platform_name
        and not "CUDA" in platform_name
    ):
        platform = mm.Platform.getPlatformByName("CPU")
        properties = {"CpuThreads": platform_name.replace("CPU", "")}
    elif "CUDA" in platform_name:
        platform = mm.Platform.getPlatformByName("CUDA")
        platform_index, device_index = get_platforms(platform_name)
        # precision
        if precision == "double":
            if platform.supportsDoublePrecision():
                properties = {"DeviceIndex": str(device_index), "Precision": "double"}
            else:
                properties = {"DeviceIndex": str(device_index), "Precision": "single"}
        else:
            properties = {"DeviceIndex": str(device_index), "CudaPrecision": precision}
    elif "OpenCL" in platform_name:
        # get OpenCL platform
        platform = mm.Platform.getPlatformByName("OpenCL")
        platform_index, device_index = get_platforms(platform_name)
        # multiple device ?
        if platform_name.count("GeForce") > 1:
            str_device_index = "0,1"
        else:
            str_device_index = str(device_index)
        # precision
        if precision == "double":
            if platform.supportsDoublePrecision():
                properties = {
                    "OpenCLPlatformIndex": str(platform_index),
                    "DeviceIndex": str_device_index,
                    "Precision": "double",
                }
            else:
                properties = {
                    "OpenCLPlatformIndex": str(platform_index),
                    "DeviceIndex": str_device_index,
                    "Precision": "single",
                }
        else:
            properties = {
                "OpenCLPlatformIndex": str(platform_index),
                "DeviceIndex": str_device_index,
                "Precision": precision,
            }
    return platform, properties


def create_system(Lx: float, Ly: float, Lz: float, mass: list, F: int) -> mm.System:
    """create and return a system.
    Parameters
    ----------
    Lx   : box size 'x' in nanometer (float)
    Ly   : box size 'y' in nanometer (float)
    Lz   : box size 'z' in nanometer (float)
    mass : list of the mass of each particles (list of float)
    F    : remove the com motion every F steps (int)
    Returns
    -------
    OpenMM system
    Raises
    ------
    """
    system = mm.System()
    system.setDefaultPeriodicBoxVectors(
        mm.Vec3(Lx, 0.0, 0.0), mm.Vec3(0.0, Ly, 0.0), mm.Vec3(0.0, 0.0, Lz)
    )
    # particles from the chains+binders
    for m in mass:
        system.addParticle(m)
    # remove the COM motion every F steps
    system.addForce(mm.CMMotionRemover(F))
    return system


def create_context(
    system: mm.System,
    integrator,
    platform: mm.Platform,
    properties,
    positions: list,
    velocities: list,
) -> mm.Context:
    """create and return an OpenMM context.
    Parameters
    ----------
    system     : OpenMM system
    integrator : OpenMM integrator
    platform   : OpenMM platform
    properties : OpenMM properties
    positions  : positions of the beads (list of OpenMM.Vec3)
    velocities : velocities of the beads (list of OpenMM.Vec3)
    Returns
    -------
    OpenMM context
    """
    context = mm.Context(system, integrator, platform, properties)
    context.setPositions(positions)
    context.setVelocities(velocities)
    return context


def write_entanglement_info(
    bp_per_nm3: float,
    N: int,
    resolution: int,
    k_bp: int,
    k_nm: float,
    name: str = "dimensions.out",
):
    """write info about entanglement length in the system you created.
    Parameters
    ----------
    bp_per_nm3 : number of bp per unit of volume (float)
    N          : number of beads (int)
    resolution : number of bp per beads (int)
    k_bp       : Kuhn length in bp (int)
    k_nm       : Kuhn length in nm (float)
    name       : file name to write the dimensions (str)
    Returns
    -------
    Raises
    ------
    """
    B = np.power(N * resolution / bp_per_nm3, 1.0 / 3.0)  # box size in nm
    L = N * resolution * k_nm / k_bp
    rho_k = (N * resolution / k_bp) / (
        B * B * B
    )  # number of Kuhn segments per unit of volume
    e_k = rho_k * k_nm * k_nm * k_nm
    Le_nm = k_nm * (np.power(0.06 * e_k, -0.4) + np.power(0.06 * e_k, -2.0))  # nm
    Le_per_L = Le_nm / L
    Rg2_Le_nm2 = (
        Le_nm * k_nm / 6.0
    )  # gyration radius at the scale of the entanglement length
    # writing the dimensions
    with open(name, "w") as out_file:
        out_file.write("kuhn " + str(k_bp) + " bp" + "\n")
        out_file.write("kuhn " + str(k_nm) + " nm" + "\n")
        out_file.write("bond " + str(k_nm * resolution / k_bp) + " nm" + "\n")
        out_file.write("resolution " + str(resolution) + " bp" + "\n")
        out_file.write("ek " + str(e_k) + " dimensionless" + "\n")
        out_file.write("L " + str(L) + " nm" + "\n")
        out_file.write("Le " + str(Le_nm) + " nm" + "\n")
        out_file.write("Le/L " + str(Le_nm / L) + " dimensionless" + "\n")
        out_file.write("Rg2(Le) " + str(Rg2_Le_nm2) + " nm^2" + "\n")
        out_file.write("B " + str(B) + " nm" + "\n")
        out_file.write("bp_per_nm3 " + str(bp_per_nm3) + " bp_per_nm3" + "\n")
        out_file.write(
            "v/V "
            + str(
                N
                * 4.0
                * np.pi
                * np.power(0.5 * k_nm * resolution / k_bp, 3.0)
                / (3.0 * B * B * B)
            )
            + " none"
            + "\n"
        )


def get_info_system(context: mm.Context):
    """print the info about the system you created.
    Parameters
    ----------
    context : OpenMM context
    Returns
    -------
    """
    print(str(context.getSystem().getNumParticles()) + " particle(s)")
    print(
        "system uses PBC : " + str(context.getSystem().usesPeriodicBoundaryConditions())
    )
    print(
        "default periodic box : "
        + str(context.getSystem().getDefaultPeriodicBoxVectors())
    )
    for f in range(context.getSystem().getNumForces()):
        print("--- force " + str(f))
        print(
            "PBC "
            + str(context.getSystem().getForce(f).usesPeriodicBoundaryConditions())
        )
        try:
            print(str(context.getSystem().getForce(f).getNumBonds()) + " bond(s)")
        except:
            error = True
        try:
            context.getSystem().getForce(f).updateParametersInContext(context)
        except:
            print("no updateParametersInContext() for the current force")
        try:
            print(
                "non bonded method "
                + str(context.getSystem().getForce(f).getNonbondedMethod())
                + "/"
                + str(mm.NonbondedForce.CutoffPeriodic)
            )
            print(
                "cutoff distance : "
                + str(context.getSystem().getForce(f).getCutoffDistance())
            )
            print(
                "use long range correction : "
                + str(context.getSystem().getForce(f).getUseLongRangeCorrection())
            )
            print(
                "use switching function : "
                + str(context.getSystem().getForce(f).getUseSwitchingFunction())
            )
            print(
                str(context.getSystem().getForce(f).getNumPerParticleParameters())
                + " per particle parameters",
                str(context.getSystem().getForce(f).getNumGlobalParameters())
                + " global parameters",
            )
        except:
            error = True
    print(str(context.getSystem().getNumConstraints()) + " constraints")
    for n in context.getPlatform().getPropertyNames():
        print(n + " : " + str(context.getPlatform().getPropertyValue(context, n)))


@numba.njit(parallel=True)
def check_outside_box(
    positions: np.ndarray, infs: np.ndarray, sups: np.ndarray
) -> tuple:
    """calculates the number of positions outside the box [xinf,xsup] (the same for y and z coordinates).
    Parameters
    ----------
    positions   : list of positions returns by 'get_positions_from_context' function
    infs        : inf of the box (np.ndarray)
    sups        : sup of the box (np.ndarray)
    Returns
    -------
    tuple of int made of the number of positions outside the box.
    Raises
    ------
    """
    P, D = positions.shape
    outsides = np.zeros(D)
    for i in prange(D):
        outsides[i] = np.sum(
            np.logical_or(positions[:, i] < infs[i], positions[:, i] > sups[i])
        )
    return int(outsides[0]), int(outsides[1]), int(outsides[2])


@numba.jit(nopython=True, parallel=True)
def average_bond_size_chain(
    positions: np.ndarray, N_per_C: np.ndarray, L: float, Ci: int = 0, PBC: bool = True
) -> tuple:
    """return the average bond size, min and max of chain.
    Parameters
    ----------
    positions : positions (in nm) of the N beads return by 'get_positions_from_context' function
    N_per_C   : number of beads per chain (np.ndarray)
    L         : box size (float)
    Ci        : chain index (int)
    PBC       : periodic boundary conditions (bool)
    Returns
    -------
    square root of the average quadratic bond size (float), min (float) and max (float)
    Raises
    ------
    np.sqrt raises an error for average_square_bond_size<0
    """
    C = int(N_per_C.size)
    box = [L] * 3
    # start of each chain
    C0 = np.cumsum(np.concatenate((np.array([0]), N_per_C)))
    # extract chain
    s1 = int(C0[Ci])
    n1 = int(N_per_C[Ci])
    p1 = positions[np.arange(s1, s1 + n1)]
    # average, min and max
    amM = np.zeros(3)
    d2 = np.array(
        [square_distance_with_PBC(p1[n], p1[n + 1], box, PBC) for n in range(n1 - 1)]
    )
    for i in prange(3):
        if i == 0:
            amM[i] = np.sum(d2)
        elif i == 1:
            amM[i] = np.min(d2)
        elif i == 2:
            amM[i] = np.max(d2)
    return (
        np.sqrt(amM[0] / n1),
        np.sqrt(amM[1]),
        np.sqrt(amM[2]),
    )  # ,np.where(d2==amM[1]),np.where(d2==amM[2])


def test(
    positions: np.ndarray, N_per_C: list, L: float, Ci: int = 0, PBC: bool = True
) -> np.ndarray:
    """return the average bond size, min and max of chain.
    Parameters
    ----------
    positions : positions (in nm) of the N beads return by 'get_positions_from_context' function
    N_per_C   : number of beads per chain (list of int)
    L         : box size (float)
    Ci        : chain index (int)
    PBC       : periodic boundary conditions (bool)
    Returns
    -------
    square root of the average quadratic bond size (float), min (float) and max (float)
    Raises
    ------
    np.sqrt raises an error for average_square_bond_size<0
    """
    C = int(len(N_per_C))
    box = [L] * 3
    # start of each chain
    C0 = np.cumsum(np.array([0] + N_per_C))
    # extract chain
    s1 = int(C0[Ci])
    n1 = int(N_per_C[Ci])
    p1 = positions[np.arange(s1, s1 + n1)]
    d2 = np.array(
        [square_distance_with_PBC(p1[n], p1[n + 1], box, PBC) for n in range(n1 - 1)]
    )
    return np.sqrt(d2)


@numba.jit(nopython=True, parallel=True)
def average_bond_size(
    positions: np.ndarray, N_per_C: np.ndarray, L: float, PBC: bool = True
) -> tuple:
    """return the average bond size, min and max.
    Parameters
    ----------
    positions : positions (in nm) of the N beads return by 'get_positions_from_context' function
    N_per_C   : number of beads per chain (np.ndarray)
    L         : box size (float)
    PBC       : periodic boundary conditions (bool)
    Returns
    -------
    square root of the average quadratic bond size (float), min (float) and max (float)
    Raises
    ------
    np.sqrt raises an error for average_square_bond_size<0
    """
    C = int(N_per_C.size)
    N = int(np.sum(N_per_C))
    box = [L] * 3
    # start of each chain
    C0 = np.cumsum(np.concatenate((np.array([0]), N_per_C)))
    # end of each chain
    Cn = np.array([int(C0[c] + N_per_C[c] - 1) for c in range(C)])
    average_square_bond_size = np.zeros(C)
    min_b2 = np.zeros(C)
    max_b2 = np.zeros(C)
    for c in prange(C):
        average_square_bond_size[c] = np.sum(
            np.array(
                [
                    square_distance_with_PBC(positions[n], positions[n + 1], box, PBC)
                    for n in range(C0[c], Cn[c])
                ]
            )
        )
        min_b2[c] = np.min(
            np.array(
                [
                    square_distance_with_PBC(positions[n], positions[n + 1], box, PBC)
                    for n in range(C0[c], Cn[c])
                ]
            )
        )
        max_b2[c] = np.max(
            np.array(
                [
                    square_distance_with_PBC(positions[n], positions[n + 1], box, PBC)
                    for n in range(C0[c], Cn[c])
                ]
            )
        )
    return (
        np.sqrt(np.sum(average_square_bond_size) / N),
        np.sqrt(np.min(min_b2)),
        np.sqrt(np.max(max_b2)),
    )


@numba.jit(nopython=True, parallel=True)
def average_pairwize_distance_intra_chain(
    positions: np.ndarray, N_per_C: np.ndarray, L: float, C1: int = 0, PBC: bool = True
) -> tuple:
    """return the average pairwize distance.
    Parameters
    ----------
    positions : positions (in nm) of the N beads return by 'get_positions_from_context' function
    N_per_C   : number of beads per chain (np.ndarray)
    C1        : index of the chain (int)
    L         : box size in nm (float)
    PBC       : periodic boundary conditions (bool)
    Returns
    -------
    square root of the average quadratic pairwize distance (float)
    Raises
    ------
    np.sqrt raises an error for average_bond_size<0
    """
    # start of each chain
    C0 = np.cumsum(np.concatenate((np.array([0]), N_per_C)))
    n1 = int(N_per_C[C1])
    s1 = int(C0[C1])
    n_pairwize = n1 * n1
    # extract positions
    box = [L] * 3
    p1 = positions[np.arange(s1, s1 + n1)]
    # with prange
    d2s = np.zeros(n1)
    min2s = np.zeros(n1)
    max2s = np.zeros(n1)
    for i in prange(n1):
        d2s[i] = np.sum(
            np.array([square_distance_with_PBC(p1[i], p1[j], box) for j in range(n1)])
        )
        min2s[i] = np.min(
            np.array(
                [
                    square_distance_with_PBC(p1[i], p1[j], box)
                    for j in range(n1)
                    if i != j
                ]
            )
        )
        max2s[i] = np.max(
            np.array([square_distance_with_PBC(p1[i], p1[j], box) for j in range(n1)])
        )
    return (
        np.sqrt(np.sum(d2s) / n_pairwize),
        np.sqrt(np.min(min2s)),
        np.sqrt(np.max(max2s)),
    )


@numba.jit(nopython=True, parallel=True)
def average_pairwize_distance_inter_chain(
    positions: np.ndarray,
    N_per_C: np.ndarray,
    L: float,
    C1: int = 0,
    C2: int = 0,
    PBC: bool = True,
) -> tuple:
    """return the average pairwize distance.
    Parameters
    ----------
    positions : positions (in nm) of the N beads return by 'get_positions_from_context' function
    N_per_C   : number of beads per chain (np.ndarray)
    L         : box size in nm (float)
    C1        : index of the first chain (int)
    C2        : index of the second chain (int)
    PBC       : periodic boundary conditions (bool)
    Returns
    -------
    square root of the average quadratic pairwize distance (float)
    Raises
    ------
    np.sqrt raises an error for average_bond_size<0
    """
    # start of each chain
    C0 = np.concatenate((np.array([0]), N_per_C))
    n1 = int(N_per_C[C1])
    s1 = int(C0[C1])
    n2 = int(N_per_C[C2])
    s2 = int(C0[C2])
    n_pairwize = n1 * n2
    # extract positions
    box = [L] * 3
    p1 = positions[np.arange(s1, s1 + n1)]
    p2 = positions[np.arange(s2, s2 + n2)]
    # with prange
    d2s = np.zeros(n1)
    min2s = np.zeros(n1)
    max2s = np.zeros(n1)
    for i in prange(n1):
        d2s[i] = np.sum(
            np.array([square_distance_with_PBC(p1[i], p2[j], box) for j in range(n2)])
        )
        min2s[i] = np.min(
            np.array([square_distance_with_PBC(p1[i], p2[j], box) for j in range(n2)])
        )
        max2s[i] = np.max(
            np.array([square_distance_with_PBC(p1[i], p2[j], box) for j in range(n2)])
        )
    return (
        np.sqrt(np.sum(d2s) / n_pairwize),
        np.sqrt(np.min(min2s)),
        np.sqrt(np.max(max2s)),
    )


@numba.jit(nopython=True)
def wca_eval(sigma: float, r: float = -1.0, r2: float = 0.0) -> float:
    """return the evaluation of the WCA potential.
    Parameters
    ----------
    sigma : sigma of the WCA potential
    r     : distance (float)
    r2    : square distance (float)
    Returns
    -------
    evaluation of the WCA potential at r (float)
    Raises
    ------
    np.power returns an error if r=0.0 or r2=0.0
    """
    a_cutoff = np.power(2.0, 1.0 / 6.0)
    sigma2 = sigma * sigma
    sigma6 = sigma2 * sigma2 * sigma2
    if r < 0.0:
        r6 = r2 * r2 * r2
        r12 = r6 * r6
    else:
        r2 = r * r
        r6 = r2 * r2 * r2
        r12 = r6 * r6
    return (
        4.0
        * (sigma6 * sigma6 / r12 - sigma6 / r6 + 0.25)
        * int(r2 < (a_cutoff * a_cutoff * sigma2))
    )


@numba.jit(nopython=True, parallel=True)
def sum_wca_intra_chain(
    positions: np.ndarray,
    N_per_C: np.ndarray,
    L: float,
    sigma: float,
    C1: int = 0,
    PBC: bool = True,
) -> tuple:
    """return the sum of the intra WCA potential.
    Parameters
    ----------
    positions : positions (in nm) of the N beads return by 'get_positions_from_context' function
    N_per_C   : number of beads per chain (np.ndarray)
    L         : box size in nm (float)
    sigma     : sigma of the WCA potential (float)
    C1        : index of the chain (int)
    PBC       : periodic boundary conditions (bool)
    Returns
    -------
    square root of the average quadratic pairwize distance (float)
    Raises
    ------
    np.sqrt raises an error for average_bond_size<0
    """
    a_cutoff = np.power(2.0, 1.0 / 6.0)
    cutoff2 = (a_cutoff * sigma) ** 2
    # start of the chain
    C0 = np.cumsum(np.concatenate((np.array([0]), N_per_C)))
    n1 = int(N_per_C[C1])
    s1 = int(C0[C1])
    n_pairwize = n1 * n1
    # extract positions
    box = [L] * 3
    p1 = positions[np.arange(s1, s1 + n1)]
    # with prange
    wcas = np.zeros(n1)
    nz_wcas = np.full(n1, 0)
    for i in prange(n1):
        wcas[i] = np.sum(
            np.array(
                [
                    wca_eval(
                        sigma, r=-1.0, r2=square_distance_with_PBC(p1[i], p1[j], box)
                    )
                    for j in range(n1)
                    if i != j
                ]
            )
        )
        nz_wcas[i] = int(
            np.sum(
                np.array(
                    [
                        int(square_distance_with_PBC(p1[i], p1[j], box) < cutoff2)
                        for j in range(n1)
                    ]
                )
            )
        )
    return np.sum(wcas) / 2.0, (int(np.sum(nz_wcas)) - n1) // 2


@numba.jit(nopython=True, parallel=True)
def sum_wca_inter_chain(
    positions: np.ndarray,
    N_per_C: np.ndarray,
    L: float,
    sigma: float,
    C1: int = 0,
    C2: int = 1,
    PBC: bool = True,
) -> tuple:
    """return the sum of the intra WCA potential.
    Parameters
    ----------
    positions : positions (in nm) of the N beads return by 'get_positions_from_context' function
    N_per_C   : number of beads per chain (np.ndarray)
    L         : box size in nm (float)
    sigma     : sigma of the WCA potential (float)
    C1        : index of the first chain (int)
    C2        : index of the second chain (int)
    PBC       : periodic boundary conditions (bool)
    Returns
    -------
    square root of the average quadratic pairwize distance (float)
    Raises
    ------
    np.sqrt raises an error for average_bond_size<0
    """
    a_cutoff = np.power(2.0, 1.0 / 6.0)
    cutoff2 = (a_cutoff * sigma) ** 2
    # start of each chain
    C0 = np.cumsum(np.concatenate((np.array([0]), N_per_C)))
    n1 = int(N_per_C[C1])
    s1 = int(C0[C1])
    n2 = int(N_per_C[C2])
    s2 = int(C0[C2])
    n_pairwize = n1 * n2
    # extract positions
    box = [L] * 3
    p1 = positions[np.arange(s1, s1 + n1)]
    p2 = positions[np.arange(s2, s2 + n2)]
    # with prange
    wcas = np.zeros(n1)
    nz_wcas = np.full(n1, 0)
    for i in prange(n1):
        wcas[i] = np.sum(
            np.array(
                [
                    wca_eval(
                        sigma, r=-1.0, r2=square_distance_with_PBC(p1[i], p2[j], box)
                    )
                    for j in range(n2)
                ]
            )
        )
        nz_wcas[i] = int(
            np.sum(
                np.array(
                    [
                        int(square_distance_with_PBC(p1[i], p2[j], box) < cutoff2)
                        for j in range(n2)
                    ]
                )
            )
        )
    return np.sum(wcas), int(np.sum(nz_wcas))


def sum_square_velocities(context: mm.Context) -> float:
    """return the sum of the square velocities in nm^2 per ps^2.
    Parameters
    ----------
    context : OpenMM context
    Returns
    -------
    sum of the square velocities (float)
    """
    nm2_per_ps2 = 1.0 / (unit.nanometer / unit.picosecond) ** 2
    velocities = context.getState(getVelocities=True).getVelocities()
    return sum([(v[0] ** 2 + v[1] ** 2 + v[2] ** 2) * nm2_per_ps2 for v in velocities])


def create_initial_conformation(
    context: mm.Context,
    N_per_C: list,
    thermostat: str,
    sigma: float,
    sigma0: float = -1.0,
    Temp: float = 300,
    NB: int = 1000,
    nb: int = 10001,
    every: int = 4,
    name: str = "",
) -> int:
    """creates a initial conformation (from a straight line made of small bonds) with the correct bond size 'sigma'.
    the context positions have been returned by the 'positions_linear_or_ring' function.
    at the end of the function, the limitation on the displacement is removed as-well-as the rescaling of velocities.
    Parameters
    ----------
    context     : OpenMM context
    N_per_C     : number of beads for each chain (list of int)
    thermostat  : gjf, nve or langevin (str)
    sigma       : the bond size you ask for (float)
    sigma0      : initial bond size (float)
                  if no value is passed, the function calculates the minimal bond size
    Temp        : temperature (float)
    NB          : number of iterations between two change of size (int)
    nb          : number of iterations to go from sigma(t=0) to sigma (int)
    every       : change the cutoff every this many time-steps (int)
    name        : name of the file 'name.png' where to save intermediate conformations (str)
    Returns
    -------
    0 if the initial conformation procedure failed, 1 otherwize.
    Raises
    ------
    """
    inv_nm = 1.0 / unit.nanometer
    dof = (
        3 * context.getSystem().getNumParticles()
        - context.getSystem().getNumConstraints()
    )
    kBT = get_kBT(Temp)
    a_cutoff = np.power(2.0, 1.0 / 6.0)
    # get max box edge
    L = 0.0
    for c in range(3):
        for x in range(3):
            L = max(
                L,
                (((context.getSystem().getDefaultPeriodicBoxVectors())[c])[x])
                / unit.nanometer,
            )
    # number of chains and number of beads
    C = len(N_per_C)
    N = sum(N_per_C)
    C0 = np.cumsum(np.concatenate((np.array([0]), np.array(N_per_C))))
    # color of each bead along the chain
    colors = []
    for n in N_per_C:
        colors = colors + [i / n for i in range(n)]
    # color of each binder
    B = context.getSystem().getNumParticles() - N
    colors = colors + [0] * B
    # copy of the parameters
    if thermostat == "gjf":
        copy_of_tau = context.getIntegrator().getGlobalVariableByName(
            "tau"
        )  # returns in picosecond
    elif thermostat == "langevin":
        copy_of_tau = context.getIntegrator().getFriction()
    else:
        pass
    copy_of_dt = context.getIntegrator().getStepSize()  # returns value and unit
    print("copy of tau=", copy_of_tau)
    print("copy of dt=", copy_of_dt)
    # initial bond size
    if sigma0 == -1.0:
        positions = get_positions_from_context(context, 0, N, False)
        sqrt_avg_b2, min_b, max_b = average_bond_size(
            positions, np.array(N_per_C), L, PBC=False
        )
        sigma0 = min(min_b / (1.01 * a_cutoff), sigma)
    else:
        sigma0 = min(sigma0 / (1.01 * a_cutoff), sigma)
    # increment
    dsigma = (sigma - sigma0) / nb
    # increment must be inferior to the starting bond length
    while sigma0 < dsigma:
        print("increment must be inferior to the starting bond length.")
        nb *= 1000
        print(
            str(nb) + " is the new number of iterations to go from sigma(t=0) to sigma."
        )
        dsigma = (sigma - sigma0) / nb
    # wca cutoff
    cutoff_t = a_cutoff * sigma0 + every * dsigma
    # change the cutoff distance (get the force group of wca potential)
    F = context.getSystem().getNumForces()
    F0 = -1
    for f in range(F):
        try:
            if context.getSystem().getForce(f).getForceGroup() == 3:
                F0 = f
                context.getSystem().getForce(f).setCutoffDistance(cutoff_t)
                context.reinitialize(preserveState=True)
                print("change the cutoff distance to " + str(cutoff_t) + ".")
        except:
            pass
    # steps to change the bond size
    sigmat = sigma0
    draw_conformation(
        get_positions_from_context(context, 0, N + B, False), L, colors, name + "_0.png"
    )
    context.setParameter("f_wca", min(1.0, sigma0 / sigma))
    context.setParameter("f_fene", min(1.0, sigma0 / sigma))
    print(
        "start from bond length of "
        + str(sigma0)
        + " with target "
        + str(sigma)
        + " and increment of "
        + str(dsigma)
        + "."
    )
    print(
        "the function restrains the displacement to the time-step times the thermal velocity."
    )
    print(
        "initial derivative epsilon_wca="
        + str(
            (
                context.getState(
                    getParameterDerivatives=True
                ).getEnergyParameterDerivatives()
            )["epsilon_wca"]
        )
        + "."
    )
    pre_iterations = 0
    for s in range(nb):
        # changes in the thermostat
        tau = (sigmat / sigma) * copy_of_tau
        if thermostat == "gjf":
            context.getIntegrator().setGlobalVariableByName("tau", tau)
        elif thermostat == "langevin":
            context.getIntegrator().setFriction(tau)
        else:
            pass
        context.getIntegrator().setStepSize(0.001 * tau)
        # we run the model for NB iterations before to increase the size of the WCA
        t0 = time.time()
        # pr=cProfile.Profile(subcalls=True)
        # pr.enable()
        context.getIntegrator().step(NB)
        pre_iterations += NB
        # pr.disable()
        # s=io.StringIO()
        # ps=pstats.Stats(pr,stream=s).sort_stats('calls')
        # ps.print_stats()
        # print(s.getvalue())
        print("step=", time.time() - t0)
        if pre_iterations % 200000 == 0:
            draw_conformation(
                get_positions_from_context(context, 0, N + B, False),
                L,
                colors,
                name + "_" + str(pre_iterations) + ".png",
            )
        # we increase the bond size
        sigmat += dsigma
        # change the NonbondedMethod cutoff distance
        if (s % every) == 0 or s == (nb - 1):
            t0 = time.time()
            cutoff_t = a_cutoff * min(sigma, sigmat + every * dsigma)
            sqrt_avg_l2, min_l, max_l = average_bond_size(
                get_positions_from_context(context, 0, N, False),
                np.array(N_per_C),
                L,
                PBC=False,
            )
            if max_l >= (1.5 * sigmat):
                print(
                    "The maximum bond size "
                    + str(max_l)
                    + " exceeds 1.5*sigma(t), exit."
                )
                print("Try to change the input parameters 'nb' and 'NB'.")
                return 0
            else:
                print("----- " + str(pre_iterations))
                print(
                    "NB*dt*sqrt(<v^2>)/dsigma=",
                    (
                        NB
                        * (context.getIntegrator().getStepSize() / unit.picosecond)
                        * np.sqrt(sum_square_velocities(context) / N)
                    )
                    / dsigma,
                )
                print(
                    "min(l)="
                    + str(min_l / sigmat)
                    + " sqrt(<sigma^2>)/sigma(t)="
                    + str(sqrt_avg_l2 / sigmat)
                    + " max(l)="
                    + str(max_l / sigmat)
                )
                print("f_wca=" + str(context.getParameter("f_wca")))
                print("f_fene=" + str(context.getParameter("f_fene")))
                print(
                    "derivative epsilon_wca="
                    + str(
                        (
                            context.getState(
                                getParameterDerivatives=True
                            ).getEnergyParameterDerivatives()
                        )["epsilon_wca"]
                    )
                )
                print(
                    "change cutoff(t) to " + str(cutoff_t) + " sigma(t)=" + str(sigmat)
                )
                print(
                    "K/dof="
                    + str(context.getState(getEnergy=True).getKineticEnergy() / dof)
                )
            context.getSystem().getForce(F0).setCutoffDistance(cutoff_t)
            context.reinitialize(preserveState=True)
            print("change=", time.time() - t0)
        # change of the WCA potential
        context.setParameter("f_wca", min(1.0, sigmat / sigma))
        context.setParameter("f_fene", min(1.0, sigmat / sigma))
    # run for x*NB with the correct bond size
    xNB = 1
    for s in range(1001):
        print(
            "----- run for "
            + str(xNB)
            + "*NB with the correct bond size and dt="
            + str(0.001 + s * (0.01 - 0.001) / 1000)
            + "*tau."
        )
        context.getIntegrator().setStepSize((0.001 + s * (0.01 - 0.001) / 1000) * tau)
        context.getIntegrator().step(xNB * NB)
        positions = get_positions_from_context(context, 0, N, False)
        sqrt_avg_b2, min_b, max_b = average_bond_size(
            positions, np.array(N_per_C), L, PBC=False
        )
        print("min(bond)=" + str(min_b))
        print("sqrt(<sigma^2>)/sigma(t)=" + str(sqrt_avg_b2 / sigma))
        print("max(bond)=" + str(max_b))
        print("f_wca=" + str(context.getParameter("f_wca")))
        print("f_fene=" + str(context.getParameter("f_fene")))
        print(
            "derivative epsilon_wca="
            + str(
                (
                    context.getState(
                        getParameterDerivatives=True
                    ).getEnergyParameterDerivatives()
                )["epsilon_wca"]
            )
        )
        print("K/dof=" + str(context.getState(getEnergy=True).getKineticEnergy() / dof))
        # success condition (do not exceed the 1.5*size of the FENE bond)
        if max_b > (1.5 * sigma):
            print("The maximum bond size " + str(max_b) + " exceeds 1.5*sigma, exit.")
            print("Try to change the input parameters 'nb' and 'NB'.")
            return 0
    # change of the WCA potential
    context.setParameter("f_wca", 1.0)
    context.setParameter("f_fene", 1.0)
    # print average, min and max distances
    positions = get_positions_from_context(context, 0, N, False)
    sqrt_avg_d2, min_d, max_d = average_pairwize_distance_intra_chain(
        positions, np.array(N_per_C), L, 0, PBC=True
    )
    print("sqrt(<d_ij^2>)=" + str(sqrt_avg_d2))
    print("min intra distance=" + str(min_d))
    # changes in the thermostat and print
    context.getIntegrator().setStepSize(copy_of_dt / unit.picosecond)
    print("The initial conformation with the target bond size suceeds.")
    if thermostat == "gjf":
        print(
            "Therefore, the limit on displacement and/or the rescaling of velocities are no more needed."
        )
        context.getIntegrator().setGlobalVariableByName("rlimit", 0)
        context.getIntegrator().setGlobalVariableByName("vlimit", 0)
        context.getIntegrator().setGlobalVariableByName("rescale", 0)
        context.getIntegrator().setGlobalVariableByName("tau", copy_of_tau)
    elif thermostat == "langevin":
        context.getIntegrator().setFriction(copy_of_tau)
    elif thermostat == "nve":
        print("Therefore, the limit on displacement is no more needed.")
        context.getIntegrator().setGlobalVariableByName("rlimit", 0)
        context.getIntegrator().setGlobalVariableByName("vlimit", 0)
    else:
        pass
    return 1


def make_wt_Dernburg_Cell_1998_epigenetic_model(
    C: int, bp: int, g: float = 0.0, Temp: float = 300.0, path_to: str = "/scratch"
) -> tuple:
    """create simple wild-type epigenetic model from Dernburg & al, Cell 1998.
    Parameters
    ----------
    C       : number of chains (integer)
    bp      : the number of base-pair per bead (integer)
    g       : the strength of interaction (in units of kBT) between state 'r' and state 's' (float)
    Temp    : temperature of the system (float)
    path_to : path to write the epigenetic model (str)
    Returns
    -------
    number of chain(s) (int), number of beads (int)
    Raises
    ------
    """
    kBT = get_kBT(Temp)
    heterochromatin = int(11000000 / bp)
    euchromatin = int(21400000 / bp)
    N = C * (heterochromatin + euchromatin)
    # wild-type
    with open(
        path_to
        + "/wt_Dernburg_Cell_1998_"
        + str(C)
        + "chains_N"
        + str(N)
        + "_"
        + str(bp)
        + "bp.in",
        "w",
    ) as out_file:
        out_file.write("chain monomer state\n")
        for c in range(C):
            for i in range(heterochromatin):
                out_file.write(str(c) + " " + str(i) + " " + "heterochromatin" + "\n")
            for i in range(euchromatin):
                out_file.write(
                    str(c) + " " + str(heterochromatin + i) + " " + "euchromatin" + "\n"
                )
    # interactions
    with open(path_to + "/HP1_HP1_" + str(g) + "kBT.int", "w") as out_file:
        out_file.write(
            "heterochromatin" + " " + "heterochromatin" + " " + str(g) + "\n"
        )
    return C, N


def make_mutant_Dernburg_Cell_1998_epigenetic_model(
    C: int, bp: int, g: float = 0.0, Temp: float = 300.0, path_to: str = "/scratch"
) -> tuple:
    """create simple mutant epigenetic model from Dernburg & al, Cell 1998.
    Parameters
    ----------
    C       : number of chains (int)
    bp      : the number of base-pair per bead (int)
    g       : the strength of interaction (in units of kBT) between state 'r' and state 's' (float)
    Temp    : temperature of the system (float)
    path_to : path to write the epigenetic model (str)
    Returns
    -------
    number of chain(s) (int), number of beads (int)
    Raises
    ------
    """
    kBT = get_kBT(Temp)
    insertion = int(2000000 / bp)
    position = int(19415328 / bp)  # from dm6 to dm3
    heterochromatin = int(11000000 / bp)
    euchromatin = int(21400000 / bp)
    N = heterochromatin + position + insertion + euchromatin - position
    # mutant (insertion)
    with open(
        path_to
        + "/mutant_Dernburg_Cell_1998_"
        + str(C)
        + "chains_N"
        + str(N)
        + "_"
        + str(bp)
        + "bp.in",
        "w",
    ) as out_file:
        out_file.write("chain monomer state\n")
        for c in range(C):
            for i in range(heterochromatin):
                out_file.write(str(c) + " " + str(i) + " " + "heterochromatin" + "\n")
            for i in range(position):
                out_file.write(
                    str(c) + " " + str(heterochromatin + i) + " " + "euchromatin" + "\n"
                )
            for i in range(insertion):
                out_file.write(
                    str(c)
                    + " "
                    + str(heterochromatin + position + i)
                    + " "
                    + "heterochromatin"
                    + "\n"
                )
            for i in range(euchromatin - position):
                out_file.write(
                    str(c)
                    + " "
                    + str(heterochromatin + position + insertion + i)
                    + " "
                    + "euchromatin"
                    + "\n"
                )
    # interactions
    with open(path_to + "/HP1_HP1_" + str(g) + "kBT.int", "w") as out_file:
        out_file.write(
            "heterochromatin" + " " + "heterochromatin" + " " + str(g) + "\n"
        )
    return C, C * N


def make_yeast_genome(
    bp: float = 150.0, g: float = 0.0, Temp: float = 300.0, path_to: str = "/scratch"
) -> tuple:
    """create the yeast genome.
    Parameters
    ----------
    bp      : the number of base-pair per bead (integer)
    g       : the strength of interaction (in units of kBT) between state 'r' and state 's' (float)
    Temp    : temperature of the system (float)
    path_to : path to write the epigenetic model (str)
    Returns
    -------
    number of chain(s) (int), number of beads (int)
    Raises
    ------
    """
    # 5000 * N + 150 * 1000 = G(chr 12) 1078177 bp
    # N*r^3=n*a^3 donc a=100*(150/n)^1/3 nm
    # 1/3=N^3/2*b^3/(6^3/2*1000^3)
    kBT = get_kBT(Temp)
    n_rDNA = 150
    # length of each chromosome
    chains = [
        46 * 5000 / bp,
        163 * 5000 / bp,
        63 * 5000 / bp,
        306 * 5000 / bp,
        115 * 5000 / bp,
        54 * 5000 / bp,
        218 * 5000 / bp,
        113 * 5000 / bp,
        88 * 5000 / bp,
        149 * 5000 / bp,
        133 * 5000 / bp,
        (186 * 5000 + n_rDNA * 1000) / bp,
        185 * 5000 / bp,
        157 * 5000 / bp,
        218 * 5000 / bp,
        190 * 5000 / bp,
    ]
    chains = [int(c) for c in chains]
    C = len(chains)
    N = sum(chains)
    # write the state of each bead
    with open(path_to + "/yeast_genome_" + str(bp) + "bp.in", "w") as out_file:
        out_file.write("chain monomer state\n")
        for c in range(C):
            for n in range(chains[c]):
                out_file.write(str(c) + " " + str(n) + " " + "nothing" + "\n")
    # write generic interaction
    with open(path_to + "/generic_interaction_" + str(g) + "kBT.int", "w") as out_file:
        out_file.write("nothing" + " " + "nothing" + " " + str(g) + "\n")
    return C, N


def get_yeast_centromeres(bp: int = 150):
    """return the start of each chromosome, each chromosomic arm as well as the centromeres.
    Parameters
    ----------
    bp    : the number of base-pair per bead (int)
    Returns
    -------
    Raises
    ------
    """
    C = 16
    n_rDNA = 150
    chains = [
        46 * 5000 // bp,
        163 * 5000 // bp,
        63 * 5000 // bp,
        306 * 5000 // bp,
        115 * 5000 // bp,
        54 * 5000 // bp,
        218 * 5000 // bp,
        113 * 5000 // bp,
        88 * 5000 // bp,
        149 * 5000 // bp,
        133 * 5000 // bp,
        (186 * 5000 + n_rDNA * 1000) // bp,
        185 * 5000 // bp,
        157 * 5000 // bp,
        218 * 5000 // bp,
        190 * 5000 // bp,
    ]
    # centromeres
    centromeres = [29, 47, 21, 89, 29, 29, 99, 20, 70, 87, 87, 29, 52, 125, 64, 110]
    centromeres = [c * 5000 // bp for c in centromeres]
    # start of the chromosome
    chr0 = [0] * C
    arm0 = [0] * (2 * C)
    arm0[1] = 29 * 5000 // bp
    for c in range(1, C):
        chr0[c] = chr0[c - 1] + chains[c - 1]
        arm0[2 * c] = chr0[c]
        if c == 1:
            offset = 47 * 5000 // bp
        elif c == 2:
            offset = 21 * 5000 // bp
        elif c == 3:
            offset = 89 * 5000 // bp
        elif c == 4:
            offset = 29 * 5000 // bp
        elif c == 5:
            offset = 29 * 5000 // bp
        elif c == 6:
            offset = 99 * 5000 // bp
        elif c == 7:
            offset = 20 * 5000 // bp
        elif c == 8:
            offset = 70 * 5000 // bp
        elif c == 9:
            offset = 87 * 5000 // bp
        elif c == 10:
            offset = 87 * 5000 // bp
        elif c == 11:
            offset = 29 * 5000 // bp
        elif c == 12:
            offset = 52 * 5000 // bp
        elif c == 13:
            offset = 125 * 5000 // bp
        elif c == 14:
            offset = 64 * 5000 // bp
        elif c == 15:
            offset = 110 * 5000 // bp
        else:
            pass
        arm0[2 * c + 1] = arm0[2 * c] + offset
    return chr0, arm0, centromeres


def make_yeast_single_chromosome_genome(
    bp: int = 150, g: float = 0.0, Temp: float = 300.0, path_to: str = "/scratch"
) -> tuple:
    """create the yeast single chromosome genome.
    Parameters
    ----------
    bp      : the number of base-pair per bead (integer)
    g       : the strength of interaction (in units of kBT) between state 'r' and state 's' (float)
    Temp    : temperature of the system (float)
    path_to : path to write the epigenetic model (str)
    Returns
    -------
    number of chain(s) (int), number of beads (int)
    Raises
    ------
    """
    # 5000 * N + 150 * 1000 = G(chr 12) 1078177 bp
    # N*r^3=n*a^3 donc a=100*(150/n)^1/3 nm
    # 1/3=N^3/2*b^3/(6^3/2*1000^3)
    kBT = get_kBT(Temp)
    n_rDNA = 150
    model = "yeast_single_chromosome_" + str(bp) + "bp"
    order = [10, 9, 8, 7, 4, 3, 2, 1, 11, 15, 12, 13, 14, 6, 5, 16]
    chains = [
        46 * 5000 // bp,
        163 * 5000 // bp,
        63 * 5000 // bp,
        306 * 5000 // bp,
        115 * 5000 // bp,
        54 * 5000 // bp,
        218 * 5000 // bp,
        113 * 5000 // bp,
        88 * 5000 // bp,
        149 * 5000 // bp,
        133 * 5000 // bp,
        (186 * 5000 + n_rDNA * 1000) // bp,
        185 * 5000 // bp,
        157 * 5000 // bp,
        218 * 5000 // bp,
        190 * 5000 // bp,
    ]
    chains = [chains[c - 1] for c in order]
    with open(
        path_to + "/" + model + "_N" + str(sum(chains)) + "_" + str(bp) + "bp.in", "w"
    ) as out_file:
        out_file.write("chain monomer state\n")
        for n in range(sum(chains)):
            out_file.write(str(0) + " " + str(n) + " " + "nothing" + "\n")
    with open(
        path_to + "/generic_interaction_" + str(g) + "kBT_" + model + ".int", "w"
    ) as out_file:
        out_file.write("nothing" + " " + "nothing" + " " + str(g) + "\n")
    return 1, sum(chains)


def make_Yad_Ghavi_Helm_system(
    C: int = 2,
    start: int = 21000000,
    end: int = 24000000,
    bp: int = 150,
    g: float = 0.0,
    Temp: float = 300.0,
    path_to: str = "/scratch",
) -> tuple:
    """create the Yad-Ghavi Helm model one state for the 'twist' promoter and the enhancer.
    Parameters
    ----------
    C       : number of chains (int) is 2 (two copies of the genome for the cycle 14)
    start   : start of the part you want to study (int)
    end     : end of the part you want to study (int)
    bp      : the number of base-pair per bead (int)
    g       : the strength of interaction (in units of kBT) between state 'r' and state 's' (float)
    Temp    : temperature of the system (float)
    path_to : path to write the epigenetic model (str)
    Returns
    -------
    number of chain(s) (int), number of beads (int)
    Raises
    ------
    """
    kBT = get_kBT(Temp)
    promoter = 23046321 // bp
    enhancers = [
        23049440,
        23053901,
        23120077,
        22865023,
        23097536,
        22971775,
        23084945,
        21381841,
        22996502,
    ]
    types = ["wt", "m1", "m2", "m3", "m4", "m5", "m6", "m7", "m8"]
    E = len(enhancers)
    # enhancer genomic size (in bp)
    de = 23050530 - 23049440
    # number of beads
    N = C * (end - start) // bp
    title = (
        "s"
        + str(start)
        + "_e"
        + str(end)
        + "_"
        + str(C)
        + "chains_N"
        + str(N)
        + "_"
        + str(bp)
        + "bp"
    )
    # loop over the differents positions of the enhancer
    for e in range(E):
        model = "promoter_twist_enhancer_" + types[e] + "_" + str(enhancers[e])
        print(
            "Yad-Ghavi Helm system : "
            + model
            + ", creation of the states and interactions files."
        )
        # states
        with open(path_to + "/" + model + "_" + title + ".in", "w") as out_file:
            out_file.write("chain monomer state\n")
            for c in range(C):
                for s in range(start, end, bp):
                    if (s // bp) == promoter:
                        out_file.write(
                            str(c)
                            + " "
                            + str(((s - start) // bp))
                            + " "
                            + "promoter"
                            + str(c)
                            + "\n"
                        )
                    elif (enhancers[e] // bp) <= (s // bp) and (s // bp) <= (
                        (enhancers[e] + de) // bp
                    ):
                        out_file.write(
                            str(c)
                            + " "
                            + str(((s - start) // bp))
                            + " "
                            + "enhancer"
                            + str(c)
                            + "\n"
                        )
                    else:
                        out_file.write(
                            str(c)
                            + " "
                            + str(((s - start) // bp))
                            + " "
                            + "nothing"
                            + "\n"
                        )
        # interactions
        with open(
            path_to + "/enhancer_promoter_" + str(g) + "kBT.int", "w"
        ) as out_file:
            out_file.write("nothing" + " " + "nothing" + " " + str(g) + "\n")
            for c in range(C):
                for d in range(C):
                    out_file.write(
                        "enhancer"
                        + str(c)
                        + " "
                        + "enhancer"
                        + str(d)
                        + " "
                        + str(g)
                        + "\n"
                    )
                    out_file.write(
                        "promoter"
                        + str(c)
                        + " "
                        + "promoter"
                        + str(d)
                        + " "
                        + str(g)
                        + "\n"
                    )
            for c in range(C):
                for d in range(C):
                    out_file.write(
                        "enhancer"
                        + str(c)
                        + " "
                        + "promoter"
                        + str(d)
                        + " "
                        + str(g)
                        + "\n"
                    )
    return C, N


def make_simple_epigenetic_models(
    C: int = 1,
    N: int = 100,
    bp: int = 150,
    pattern: str = "A.B.C",
    nbinders: list = [0, 0, 0],
    gs: list = [0.0, 0.0, 0.0],
    Temp: float = 300.0,
    path_to: str = "/scratch",
) -> tuple:
    """create simple epigenetic models based on the 'pattern' argument (with interactions between same state : 's' with 's').
    it also creates a file with binders to the states extracted from the pattern.
    it also creates a file with a Potts model according to the pattern.
    Each state file has the header 'chain monomer state'.
    Each binder file has the header 'binder number mass_amu sigma_nm'.
    Parameters
    ----------
    C        : number of chains (int)
    N        : the number of beads (int)
    bp       : the number of base-pair per bead (int)
    pattern  : simple block-copolymer pattern (str)
               each state of the pattern has to be followed by a dot
    nbinders : number of binders per state (list of int)
    gs       : list of interaction strengths (in units of kBT) between state 'r' and state 's' (list of float)
    Temp     : temperature of the system (float)
    path_to  : path to write the epigenetic model (str)
    Returns
    -------
    number of chain(s) (int), number of beads (int)
    Raises
    ------
    """
    kBT = get_kBT(Temp)
    spattern = pattern.split(".")
    n_states = len(spattern)
    per_states = int(N / n_states) + int((N % n_states) > 0)
    # bead state
    if path_to == "":
        name = pattern + ".in"
    else:
        name = path_to + "/" + pattern + ".in"
    with open(name, "w") as out_file:
        out_file.write("chain monomer state\n")
        for c in range(C):
            for i in range(N):
                out_file.write(
                    str(c) + " " + str(i) + " " + spattern[int(i / per_states)] + "\n"
                )
    # interactions
    cumg = ""
    for g in gs:
        cumg = cumg + str(g) + "kBT"
    name = pattern + "_" + cumg + ".int"
    if path_to != "":
        name = path_to + "/" + name
    with open(name, "w") as out_file:
        for i in range(n_states):
            for j in range(i, n_states):
                if spattern[i] == spattern[j]:
                    out_file.write(
                        spattern[i] + " " + spattern[j] + " " + str(gs[i]) + "\n"
                    )
    # binders
    name = pattern + ".binders"
    if path_to != "":
        name = path_to + "/" + name
    with open(name, "w") as out_file:
        out_file.write("binder number mass_amu sigma_nm\n")
        for i, s in enumerate(spattern):
            out_file.write(
                "binder_"
                + s
                + " "
                + str(nbinders[i])
                + " "
                + str(700.0 * bp)
                + " 10.0\n"
            )
    # interactions with binders
    name = pattern + "_binders_" + cumg + ".int"
    if path_to != "":
        name = path_to + "/" + name
    with open(name, "w") as out_file:
        for s in spattern:
            out_file.write(s + " " + s + " " + str(gs[i]) + "\n")
            out_file.write(s + " binder_" + s + " " + str(gs[i]) + "\n")
            out_file.write("binder_" + s + " binder_" + s + " " + str(gs[i]) + "\n")
    return C, N


def get_bonds(context: mm.Context) -> int:
    """return the number of bonds in the context.getSystem().
    Parameters
    ----------
    context : OpenMM context (OpenMM.Context)
    Returns
    -------
    the number of bonds in the context.getSystem() (integer).
    Raises
    ------
    """
    F = context.getSystem().getNumForces()
    n_bonds = 0
    for f in range(F):
        try:
            n_bonds += context.getSystem().getForce(f).getNumBonds()
        except:
            pass
    return n_bonds


def get_number_of_chains(context: mm.Context) -> int:
    """return the number of chains in the context.getSystem().
    it assumes the number of beads per chain to be the same.
    Parameters
    ----------
    context : OpenMM context (OpenMM.Context)
    Returns
    -------
    the number of bonds in the context.getSystem() (integer).
    Raises
    ------
    """
    # C chains, N particles per chain, C*(N-1) bonds
    return context.getSystem().getNumParticles() - get_bonds(context)


def remove_all_forces(context: mm.Context):
    """remove all the forces from the context.
    Parameters
    ----------
    context : OpenMM context
    Returns
    -------
    Raises
    ------
    """
    F = context.getSystem().getNumForces()
    print("before F=", F)
    for f in range(F):
        try:
            context.getSystem().removeForce(F - 1 - f)
        except:
            pass
    print("after F=", context.getSystem().getNumForces())


@numba.jit
def get_particles_per_bead():
    return 1


def from_context_to_blender(
    context: mm.Context,
    colors: np.ndarray,
    PBC: bool = False,
    sigmas: np.ndarray = np.array([]),
    file_name: str = "/scratch/visualisation_with_blender",
    append_to: str = "no",
):
    """write files for visualisation of the context with blender (do not consider virtual sites).
    first file with '.bin' extension is the conformation.
    second file with no extension is the details like bond length ...
    Parameters
    ----------
    context   : OpenMM context
    colors    : array of colors (np.ndarray)
    PBC       : enforce periodic box ? (bool)
    sigmas    : array of bond size (np.ndarray)
    file_name : name of the file to create (str)
    append_to : append to the file no/yes (str)
    Returns
    -------
    Raises
    ------
    """
    # get max box edge
    L = 0.0
    for c in range(3):
        for x in range(3):
            L = max(
                L,
                (((context.getSystem().getDefaultPeriodicBoxVectors())[c])[x])
                / unit.nanometer,
            )
    # get positions
    positions = np.subtract(
        get_positions_from_context(
            context, 0, context.getSystem().getNumParticles(), PBC
        ),
        np.array([0.5 * L, 0.5 * L, 0.5 * L]),
    )
    # write the file
    str_wb_ab = ["wb", "ab"][int(append_to == "yes")]
    with open(file_name + ".bin", str_wb_ab) as out_file:
        for i, p in enumerate(positions):
            bd = struct.pack("ddd", p[0], p[1], p[2])
            out_file.write(bd)
            # we do not need frame
            bd = struct.pack("ddd", 0.0, 0.0, 1.0)
            out_file.write(bd)
    # write description
    with open(file_name, "w") as out_file:
        for i, p in enumerate(positions):
            out_file.write(
                "1 "
                + str(0.5 * sigmas[i])
                + " "
                + str(0.5 * sigmas[i])
                + " "
                + str(i)
                + " "
                + str(colors[i])
                + " "
                + str(colors[i])
                + "\n"
            )
