#!/bin/bash
tmpdir=${PWD};
path_to_python="/scratch/pcarriva/anaconda3/bin/python3.7";
path_to_pydoc="/scratch/pcarriva/anaconda3/bin/pydoc3.7";
what_to_do="all";

while getopts ht:p:d:w: option
do
    case "${option}"
    in
	t) tmpdir=${OPTARG};;
	p) path_to_python=${OPTARG};;
	d) path_to_pydoc=${OPTARG};;
	w) what_to_do=${OPTARG};;
	h) echo "Usage:"
	   echo "     ./run_test.sh -t <path to 'openmm_copolymer_functions.py'>"
	   echo "                   -p <path to python=/home/anaconda3/bin/python3.7>"
	   echo "                   -d <path to pydoc=/home/anaconda3/bin/pydoc3.7>"
	   exit 0
	   ;;
    esac
done

# folder test
if [[ !(-d ${tmpdir}/test) ]];
then
    mkdir ${tmpdir}/test;
fi;
echo "make html documentation";
${path_to_pydoc} -w ${tmpdir}/openmm_copolymer_functions.py;
echo "run the test (openmm_copolymer_functions)";
${path_to_python} ${tmpdir}/unittest_openmm_copolymer_functions.py;
