#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
The module 'unittest_openmm_copolymer_functions.py'
is a collection of test cases used to test the module 'openmm_copolymer_functions.py'.
The following code (has been developped by Pascal Carrivain)
is distributed under MIT licence.
"""
#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np
import openmm_copolymer_functions as ocf
import random
import simtk.openmm as mm
import simtk.unit as unit
import unittest


class openmm_copolymer_functionsTest(unittest.TestCase):
    """Test case for the 'openmm_copolymer_functions' module."""

    def test_from_i_to_bead(self):
        """Test of the function 'from_i_to_bead'."""
        self.assertEqual(ocf.from_i_to_bead(0, 100), 0)
        self.assertEqual(ocf.from_i_to_bead(99, 100), 99)
        self.assertEqual(ocf.from_i_to_bead(100, 100), 0)
        self.assertEqual(ocf.from_i_to_bead(-1, 100), 99)

    def test_create_system(self):
        """Test of the function 'create_system'."""
        L = 100.0
        B = 101
        m = 1.0
        system = ocf.create_system(L, L, L, [m] * B, 10)
        self.assertTrue(system.getNumParticles() == B)

    def test_create_context(self):
        """Test of the function 'create_context'."""
        # variables
        L = 100.0
        mass = 1.1
        sigma = 2.3
        Temp = 300.0
        dt = 0.001 * sigma * np.sqrt(mass / ocf.get_kBT(Temp))
        tau = 100.0 * dt
        seed = 3
        linear = True
        N_per_C = [100, 200, 100]
        z_offset = [0, 0, 0]
        L = 1000.0
        B = sum(N_per_C)
        # context
        system = ocf.create_system(L, L, L, [mass] * B, 10)
        integrator = ocf.create_integrator(
            "nve", Temp, dt, B - 0, tau, seed, False, False
        )
        platform, properties = ocf.create_platform("CPU1", "double")
        positions, sigma0, Lx, Ly, Lz = ocf.positions_linear_or_ring(
            N_per_C, z_offset, sigma, [], L, linear
        )
        velocities = ocf.v_gaussian(Temp, [mass] * B + [], seed)
        context = ocf.create_context(
            system, integrator, platform, properties, positions, velocities
        )
        Positions = ocf.get_positions_from_context(context, 0, B, False)
        Velocities = ocf.get_velocities_from_context(context, 0, B, False)
        Forces = context.getState(getForces=True).getForces()
        # check if we get the correct integrator
        self.assertTrue(type(context.getIntegrator()) == mm.VerletIntegrator)
        # check if we get the correct positions and velocities
        for i in range(B):
            pi = positions[i]
            Pi = Positions[i]
            self.assertTrue(np.sqrt(np.sum(np.square(np.subtract(pi, Pi)))) < 1e-6)
            vi = velocities[i]
            Vi = Velocities[i]
            self.assertTrue(np.sqrt(np.sum(np.square(np.subtract(vi, Vi)))) < 1e-2)
            fi = np.array(
                [Forces[i][0]._value, Forces[i][1]._value, Forces[i][2]._value]
            )
            self.assertTrue(np.sqrt(np.dot(fi, fi)) < 1e-6)

    def test_create_platform(self):
        """Test of the function 'create_platform'."""
        platform, properties = ocf.create_platform("CPU3", "double")
        self.assertTrue(properties["CpuThreads"] == "3")

    def test_create_integrator(self):
        """Test of the function 'create_integrator'."""
        # variables
        Temp = 300.0
        B = 1020
        C = 0
        mass = 1.0
        seed = 1
        sigma = 6.5
        dt = sigma * np.sqrt(mass / ocf.get_kBT(Temp))
        tau = 100.0 * dt
        # integrators
        integrator = ocf.create_integrator(
            "langevin", Temp, dt, B - C, tau, seed, False, False
        )
        self.assertTrue(type(integrator) == mm.LangevinIntegrator)
        integrator = ocf.create_integrator(
            "gjf", Temp, dt, B - C, tau, seed, False, False
        )
        self.assertTrue(type(integrator) == mm.CustomIntegrator)
        integrator = ocf.create_integrator(
            "global", Temp, dt, B - C, tau, seed, False, False
        )
        self.assertTrue(type(integrator) == mm.CustomIntegrator)
        integrator = ocf.create_integrator(
            "nve", Temp, dt, B - C, tau, seed, True, True
        )
        self.assertTrue(type(integrator) == mm.CustomIntegrator)
        integrator = ocf.create_integrator(
            "nve", Temp, dt, B - C, tau, seed, False, False
        )
        self.assertTrue(type(integrator) == mm.VerletIntegrator)
        integrator = ocf.create_integrator(
            "void", Temp, dt, B - C, tau, seed, False, False
        )
        self.assertTrue(type(integrator) == mm.LangevinIntegrator)

    def test_bending_rigidity(self):
        """Test of the function 'bending_rigidity'."""
        self.assertTrue(ocf.bending_rigidity(100.0, 100.0, 300.0) < 1e-8)

    def test_positions_linear_or_ring(self):
        """Test of the function 'positions_linear_or_ring'."""
        abs_tol = 1e-6
        rel_tol = 1e-4
        N_per_C = [4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048]
        sigma = 1.0
        C = len(N_per_C)
        B = int(sum(N_per_C))
        z_offset = [0] * C
        L = 1000.0
        for linear in [False, True]:
            positions, sigma0, Lx, Ly, Lz = ocf.positions_linear_or_ring(
                N_per_C, z_offset, sigma, [], L, linear
            )
            # is the number of positions equal to the number of beads we ask ?
            self.assertTrue(len(positions) == sum(N_per_C))
            # check if the com is in the middle of the box
            cx, cy, cz = 0.0, 0.0, 0.0
            for p in positions:
                cx += p[0]
                cy += p[1]
                cz += p[2]
            self.assertTrue(abs(cx / B - 0.5 * L) <= (abs_tol + rel_tol * (0.5 * L)))
            self.assertTrue(abs(cy / B - 0.5 * L) <= (abs_tol + rel_tol * (0.5 * L)))
            self.assertTrue(abs(cz / B - 0.5 * L) <= (abs_tol + rel_tol * (0.5 * L)))
            # check if the bond sizes are the same over the system
            index = 0
            S = B - C * int(linear)
            list_of_sigma = [0.0] * S
            for c in range(C):
                for n in range(N_per_C[c] - int(linear)):
                    pi = positions[index + n]
                    Pi = positions[index + ocf.from_i_to_bead(n + 1, N_per_C[c])]
                    list_of_sigma[index - c * int(linear) + n] = np.sqrt(
                        np.sum(np.square(np.subtract(pi, Pi)))
                    )
                index += N_per_C[c]
            for s in range(S - 1):
                self.assertTrue(
                    abs(list_of_sigma[s + 1] - list_of_sigma[s])
                    <= (abs_tol + rel_tol * list_of_sigma[s])
                )

    def test_fene_wca(self):
        """Test of the function 'fene'."""
        N_per_C = [101, 39, 478, 431]
        sigma = 1.0
        Temp = 300.0
        # check the total number of bonds
        for linear in [False, True]:
            for PBC in [False, True]:
                fene_wca = ocf.fene_wca(N_per_C, Temp, sigma, linear, PBC)
                self.assertTrue(
                    fene_wca.getNumBonds()
                    == (sum(N_per_C) - int(linear) * len(N_per_C))
                )
                self.assertTrue(fene_wca.getNumGlobalParameters() == 6)

    def test_pairing(self):
        """Test of the function 'pairing'."""
        N_per_C = [101, 39, 478, 431, 101]
        sigma = 1.0
        # check the total number of bonds (no pairing file)
        for PBC in [False, True]:
            pairing = ocf.pairing(N_per_C, "", PBC)
            self.assertTrue(pairing.getNumBonds() == 0)
            self.assertTrue(pairing.getNumGlobalParameters() == 0)
            self.assertTrue(pairing.getNumPerBondParameters() == 2)
        # check the total number of bonds
        # nothing happen if the two chains do not have the same length
        with open("test/pairing.in", "w") as out_file:
            out_file.write("chainA chainB U sigma\n")
            out_file.write("0 1 2.0 10.0\n")
            out_file.write("1 2 2.0 10.0\n")
            out_file.write("2 3 2.0 10.0\n")
            out_file.write("3 4 2.0 10.0\n")
        for PBC in [False, True]:
            pairing = ocf.pairing(N_per_C, "test/pairing.in", PBC)
            self.assertTrue(pairing.getNumBonds() == 0)
            self.assertTrue(pairing.getNumGlobalParameters() == 0)
            self.assertTrue(pairing.getNumPerBondParameters() == 2)
        # check the total number of bonds
        # 5 chains
        # pairing between two consecutive chains
        N_per_C = [101] * 5
        for PBC in [False, True]:
            pairing = ocf.pairing(N_per_C, "test/pairing.in", PBC)
            self.assertTrue(pairing.getNumBonds() == (101 * 4))
            self.assertTrue(pairing.getNumGlobalParameters() == 0)
            self.assertTrue(pairing.getNumPerBondParameters() == 2)

    def test_tt_bending(self):
        """Test of the function 'tt_bending'."""
        N_per_C = [121, 359, 278, 18, 263]
        Temp = 300.0
        k_bp = 100.0
        resolution = 10.5
        sigma = (resolution / k_bp) * 100.0
        Kb = ocf.bending_rigidity(k_bp, resolution, Temp)
        N = sum(N_per_C)
        C = len(N_per_C)
        # check the total number of angles
        for linear in [False, True]:
            int_linear = int(linear)
            for PBC in [False, True]:
                tt_bending = ocf.tt_bending(
                    N_per_C, [Kb] * (N - int_linear * 2 * C), linear, PBC
                )
                self.assertTrue(tt_bending.getNumGlobalParameters() == 2)
                self.assertTrue(tt_bending.getNumPerAngleParameters() == 1)
                self.assertTrue(tt_bending.getNumAngles() == (N - int_linear * 2 * C))

    def test_wca(self):
        """Test of the function 'wca'."""
        N_per_C = [121, 359, 278, 18, 263]
        Temp = 300.0
        k_bp = 100.0
        resolution = 10.5
        sigma = (resolution / k_bp) * 100.0
        N = sum(N_per_C)
        C = len(N_per_C)
        for linear in [False, True]:
            for PBC in [False, True]:
                wca = ocf.wca(N_per_C, Temp, sigma, [], linear, PBC, True)
                self.assertTrue(wca.getNumGlobalParameters() == 3)
                self.assertTrue(wca.getNumParticles() == N)
                self.assertTrue(wca.getNumExclusions() == (N - C * int(linear)))

    def test_lj_copolymer(self):
        """Test of the function 'lj_copolymer', 'go_copolymer' is a simple copy-paste of 'lj_copolymer'."""
        resolution = 150
        k_bp = 300
        k_nm = 30.0
        sigma = k_nm * resolution / k_bp
        C = 10
        N = 200
        N_per_C = [N for c in range(C)]
        B = sum(N_per_C)
        Temp = 300.0
        g = 0.2
        linear = True
        PBC = True
        # simple epigenetic model
        model = "A.B.C.D"
        C, N = ocf.make_simple_epigenetic_models(
            C,
            N,
            resolution,
            model,
            [20] * len(model.split(".")),
            [g] * len(model.split(".")),
            Temp,
            "test",
        )
        # read the states
        chains, states, colors, N_per_C, C, N = ocf.read_states(
            "test/bead_state_"
            + model
            + "_"
            + str(C)
            + "chains_N"
            + str(N)
            + "_"
            + str(resolution)
            + "bp.in"
        )
        # get the epigenetic interactions
        model_of_interactions = ocf.read_interactions(
            "test/Potts_" + str(g) + "kBT.in", Temp
        )
        # copolymer interactions (lj)
        lj_copolymer = ocf.lj_copolymer(
            resolution,
            k_bp,
            k_nm,
            [sigma] * B,
            states,
            model_of_interactions,
            N_per_C,
            linear,
            PBC,
        )
        # check the number of exclusions (no epigenetics interactions between beads of the same bond)
        self.assertTrue(
            lj_copolymer.getNumExclusions() == (sum(N_per_C) - C * int(linear))
        )
        # check the number of per particle parameters (Potts+bond size
        self.assertTrue(
            lj_copolymer.getNumPerParticleParameters() == (len(model.split(".")) + 1)
        )
        # check the number of global parameters (Potts)
        self.assertTrue(
            lj_copolymer.getNumGlobalParameters() == (len(model.split(".")) + 1)
        )
        # check cutoff
        self.assertTrue(
            abs(
                1.0
                - (lj_copolymer.getCutoffDistance() / unit.nanometer) / (2.15 * sigma)
            )
            < 0.000000001
        )
        if PBC:
            self.assertTrue(
                lj_copolymer.getNonbondedMethod() == mm.NonbondedForce.CutoffPeriodic
            )
        else:
            self.assertTrue(
                lj_copolymer.getNonbondedMethod() == mm.NonbondedForce.CutoffNonPeriodic
            )
        self.assertTrue(type(lj_copolymer) == mm.CustomNonbondedForce)
        self.assertTrue(lj_copolymer.getForceGroup() == 4)
        # self.assertTrue(lj_copolymer.getEnergyFunction()=="lj_on_off*4*D_r_s*((sigma_lj/r)^12-(sigma_lj/r)^6);D_r_s=p_A1*p_A2*D_A_A+p_B1*p_B2*D_B_B+p_C1*p_C2*D_C_C+p_D1*p_D2*D_D_D;sigma_lj=sqrt(sigma_lj1*sigma_lj2)")
        self.assertTrue(
            lj_copolymer.getEnergyFunction()
            == "lj_on_off*4*D_r_s*((sigma_lj/r)^12-(sigma_lj/r)^6);D_r_s=p_A1*p_A2*D_A_A+p_B1*p_B2*D_B_B+p_C1*p_C2*D_C_C+p_D1*p_D2*D_D_D;sigma_lj=0.5*(sigma_lj1+sigma_lj2)"
        )

    def test_confinement(self):
        """Test of the function 'confinement'."""
        N = 3000
        Temp = 300.0
        confinement = ocf.confinement(N, 1000.0, 0.0, 0.0, 0.0, 2.0 * ocf.get_kBT(Temp))
        self.assertTrue(confinement.getNumGlobalParameters() == 5)
        self.assertTrue(type(confinement) == mm.CustomCompoundBondForce)
        self.assertTrue(confinement.getNumBonds() == N)

    def test_read_states(self):
        """Test of the function 'read_states'."""
        C = 20
        N = 100
        resolution = 100
        model = "A.B.C"
        g = 0.0
        Temp = 300.0
        # simple epigenetic model
        C, N = ocf.make_simple_epigenetic_models(
            C,
            N,
            resolution,
            model,
            [20] * len(model.split(".")),
            [g] * len(model.split(".")),
            Temp,
            "test",
        )
        chains, states, colors, N_per_C, C, N = ocf.read_states(
            "test/bead_state_"
            + model
            + "_"
            + str(C)
            + "chains_N"
            + str(N)
            + "_"
            + str(resolution)
            + "bp.in"
        )
        # check the chain index
        index = 0
        for c in range(C):
            for n in range(N_per_C[c]):
                self.assertTrue(chains[index + n] == c)
            index += N_per_C[c]
        # are the chains identical ?
        states0 = states[: N_per_C[0]]
        cum = 0
        for c in range(1, C):
            self.assertTrue(states0 == states[cum : (cum + N_per_C[c])])
            cum += N_per_C[c]
        # one color per state
        n_mismatches = 0
        for n in range(N):
            n_mismatches += int(states[n] == "A" and colors[n] != 0)
            n_mismatches += int(states[n] == "B" and colors[n] != 1)
            n_mismatches += int(states[n] == "C" and colors[n] != 2)
        self.assertTrue(n_mismatches == 0)
        # check the number of chains, the number of beads and the number of beads per chain
        self.assertTrue(C == 20)
        self.assertTrue(N == (20 * 100))
        for c in range(C):
            self.assertTrue(N_per_C[c] == 100)

    def test_read_binders(self):
        """Test of the function 'read_states'."""
        C = 20
        N = 100
        resolution = 100
        model = "A.B.C"
        g = 0.0
        Temp = 300.0
        # simple epigenetic model
        C, N = ocf.make_simple_epigenetic_models(
            C,
            N,
            resolution,
            model,
            [20] * len(model.split(".")),
            [g] * len(model.split(".")),
            Temp,
            "test",
        )
        chains, states, colors, N_per_C, C, N = ocf.read_states(
            "test/bead_state_"
            + model
            + "_"
            + str(C)
            + "chains_N"
            + str(N)
            + "_"
            + str(resolution)
            + "bp.in"
        )
        # simple binders
        with open("test/test_binders.in", "w") as out_file:
            out_file.write("binder number mass_amu sigma_nm\n")
            out_file.write("A 10 1.0 1.0\n")
            out_file.write("B 20 1.0 1.0\n")
            out_file.write("C 10 1.0 1.0")
        states_with_binders, mass, sizes, colors = ocf.read_binders(
            "test/test_binders.in", states
        )
        # check the number of states
        self.assertTrue(len(states_with_binders) == (len(states) + 10 + 20 + 10))
        self.assertTrue(len(mass) == (10 + 20 + 10))
        self.assertTrue(len(sizes) == (10 + 20 + 10))
        self.assertTrue(len(colors) == (10 + 20 + 10))
        for c in colors:
            self.assertTrue(c < 3)

    def test_read_write_checkpoint(self):
        """Test of the function 'read_write_checkpoint'."""
        # variables
        N_per_C = [300, 100, 200, 100]
        B = int(sum(N_per_C))
        z_offset = [0, 0, 0, 0]
        L = 1000.0
        C = 0
        mass = 1.1
        sigma = 1.5
        Temp = 300.0
        dt = 0.001 * sigma * np.sqrt(mass / ocf.get_kBT(Temp))
        tau = 100.0 * dt
        seed = 1
        linear = True
        # context
        system = ocf.create_system(L, L, L, [mass] * B, 10)
        integrator = ocf.create_integrator(
            "nve", Temp, dt, B - C, tau, seed, False, False
        )
        platform, properties = ocf.create_platform("CPU1", "double")
        positions, sigma0, Lx, Ly, Lz = ocf.positions_linear_or_ring(
            N_per_C, z_offset, sigma, [], L, linear
        )
        velocities = ocf.v_gaussian(Temp, [mass] * B + [], seed)
        context = ocf.create_context(
            system, integrator, platform, properties, positions, velocities
        )
        # step the context
        context.getIntegrator().step(10)
        positions = ocf.get_positions_from_context(context, 0, B, False)
        velocities = ocf.get_velocities_from_context(context, 0, B, False)
        # save it
        ocf.read_write_checkpoint(context, "test/test_restart.chk", "write")
        # read what has just been saved
        ocf.read_write_checkpoint(context, "test/test_restart.chk", "read")
        Positions = ocf.get_positions_from_context(context, 0, B, False)
        Velocities = ocf.get_velocities_from_context(context, 0, B, False)
        # check if we get the correct positions and velocities
        for i in range(B):
            pi = positions[i]
            Pi = Positions[i]
            self.assertTrue(np.sqrt(np.sum(np.square(np.subtract(pi, Pi)))) < 1e-6)
            vi = velocities[i]
            Vi = Velocities[i]
            self.assertTrue(np.sqrt(np.sum(np.square(np.subtract(vi, Vi)))) < 1e-2)

    # def test_create_initial_conformation(self):
    #     """Test of the function 'create_initial_conformation'."""
    #     # variables
    #     N_per_C=[25,50]
    #     B=int(sum(N_per_C))
    #     z_offset=[0,0]
    #     L=25.0
    #     C=0
    #     m=1.0
    #     sigma=1.0
    #     Temp=300.0
    #     dt=0.001*sigma*np.sqrt(m/ocf.get_kBT(Temp))
    #     tau=100.0*dt
    #     seed=1
    #     linear=True
    #     PBC=True
    #     # context
    #     system=ocf.create_system(L,L,L,[m]*B,10)
    #     system.addForce(ocf.fene(N_per_C,Temp,sigma,linear,PBC))
    #     system.addForce(ocf.wca(N_per_C,Temp,sigma,[],linear,PBC))
    #     integrator=ocf.create_integrator("gjf",Temp,dt,B-C,tau,seed,True,True)
    #     platform,properties=ocf.create_platform("CPU4","double")
    #     positions=ocf.positions_linear_or_ring(N_per_C,z_offset,sigma,[],L,linear)
    #     velocities=ocf.v_gaussian(Temp,[m]*B+[],seed)
    #     context=ocf.create_context(system,integrator,platform,properties,positions,velocities)
    #     # initial conformation with small bond size increment
    #     success=ocf.create_initial_conformation(context,N_per_C,"gjf",sigma,m,Temp,1000,10001,"test/test_initial_xyz")
    #     self.assertTrue(success==1)
    #     # # initial conformation with huge bond size increment
    #     # success=ocf.create_initial_conformation(context,N_per_C,"gjf",sigma,m,Temp,1000,11,"test/test_initial_xyz")
    #     # self.assertTrue(success==0)

    def test_get_close_ij(self):
        """Test of the function 'get_close_ij'."""
        # --- straight-line polymer
        N_per_C = np.array([200, 100, 300])
        B = int(np.sum(N_per_C))
        chains = N_per_C.size
        sigma = 1.0
        linear = True
        positions = np.zeros((B, 3))
        index = 0
        for c in range(chains):
            for n in range(N_per_C[c]):
                positions[index, :3] = c * 2.5 * sigma, 0.0, n * sigma
                index += 1
        # no d(pairwize)<0.1*(bond length)
        close_ij, C = ocf.get_close_ij(positions, 0.1 * sigma, 1, linear)
        self.assertTrue(C == 0)
        # each bond make contact with the next one inside the chain for d(pairwize)<1.1*(bond length)
        close_ij, C = ocf.get_close_ij(positions, 1.1 * sigma, 1, linear)
        self.assertTrue(C == (B - chains))

    def test_check_outside_box(self):
        """Test of the function 'check_outside_box'."""
        P = 10
        positions = np.zeros((P, 3))
        for i in range(P):
            positions[i, :3] = -1.0, -1.0, -1.0
        out_x, out_y, out_z = ocf.check_outside_box(
            positions, np.array([0.0, 0.0, 0.0]), np.array([1.0, 1.0, 1.0])
        )
        self.assertTrue(out_x == P and out_y == P and out_z == P)
        for i in range(P):
            positions[i, :3] = 0.5, 0.5, 0.5
        out_x, out_y, out_z = ocf.check_outside_box(
            positions, np.array([0.0, 0.0, 0.0]), np.array([1.0, 1.0, 1.0])
        )
        self.assertTrue(out_x == 0 and out_y == 0 and out_z == 0)
        for i in range(P):
            positions[i, :3] = 0.5, -1.0, 0.5
        out_x, out_y, out_z = ocf.check_outside_box(
            positions, np.array([0.0, 0.0, 0.0]), np.array([1.0, 1.0, 1.0])
        )
        self.assertTrue(out_x == 0 and out_y == P and out_z == 0)


if __name__ == "__main__":
    unittest.main()
