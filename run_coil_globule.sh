#!/bin/bash
if [[ $1 == "" ]];
then
    echo "usage: ./run_coil_globule.sh <path to python>";
    exit 0;
fi;

echo "create models";
${1}/python3.7 models.py -C 1 -N 200 -r 150 -g 0.2 --tmpdir=${PWD}/epigenetics;

# parameters
seed=1;
thermostat="gjf";
bond="fene";
kuhn_bp=1;
kuhn_nm=1;
resolution=1;
rerun=0;
pair_style="lj";
platform="CPU2";
linear="yes";
bp_per_nm3=0.02;
precision="double";
tmpdir=${PWD};
path_to_python="/scratch/pcarriva/anaconda3/envs/my_py37/bin"
model="A"
interactions="A_0.2kBT"
binders=""
every=100000
steps=1000000
mode="all";
mkdir outputs;
(/usr/bin/time -f "%M %e %E %S %U %P" ${path_to_python}/python3.7 ${tmpdir}/openmm_copolymer.py -r $resolution -k $kuhn_bp -K $kuhn_nm -b $bp_per_nm3 -s $seed -R $rerun -g $platform -m $mode -t $thermostat -c $pair_style -l $linear --precision=$precision --model=$model --interactions=$interactions --binders=$binders --every=$every --steps=$steps --tmpdir=$tmpdir) 2>> ${PWD}/outputs/metrology.out;
# ./run_model.sh -s 1 -t gjf -b fene -r 150 -R 0 -c lj -g CPU2 -l yes -P double -m all -M A -I A_0.2kBT -e 20000 -S 20000000 -T ${PWD}/outputs -a $1;
