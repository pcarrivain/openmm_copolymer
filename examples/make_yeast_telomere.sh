#!/bin/bash

echo "leave the examples folder to the main one";
cd ../;

echo "get the path to openmm_copolymer folder";
path_to=${PWD}

echo "write the file with HP1/telomere and 500kb chromosome.";
echo "chain monomer state" > epigenetics/bead_state_yeast_telomere_150.0bp.in
for c in {0..1};
do
    for i in {0..66..1};
    do
	echo "${c} ${i} HP1" >> epigenetics/bead_state_yeast_telomere_150.0bp.in
    done;
    for i in {0..3266..1};
    do
	echo "${c} $((${i}+67)) nothing" >> epigenetics/bead_state_yeast_telomere_150.0bp.in;
    done;
done;

echo "write 1000 binders/HP1.";
echo "binder number mass_amu sigma_nm" > epigenetics/binders_yeast_telomere_150.0bp.in;
echo "binder_HP1 1000 1000.0 5.0" >> epigenetics/binders_yeast_telomere_150.0bp.in;

echo "write the interactions file.";
echo "nothing nothing 0.1" > epigenetics/u_yeast_telomere_150.0bp.in;
echo "HP1 binder_HP1 0.3" >> epigenetics/u_yeast_telomere_150.0bp.in;
echo "binder_HP1 binder_HP1 0.3" >> epigenetics/u_yeast_telomere_150.0bp.in;

echo "generate the initial conformation.";
./run_model.sh -s 1 -t gjf -b fene -r 150 -R 0 -c lj -g GeForce_GTX_780_OpenCL -l yes -B 0.02 -P single -m initial -M yeast_telomere_150.0bp -I u_yeast_telomere_150.0bp -J binders_yeast_telomere_150.0bp -T ${path_to} -a /scratch/anaconda3/bin;

echo "run the model.";
./run_model.sh -s 1 -t gjf -b fene -r 150 -R 0 -c lj -g GeForce_GTX_780_OpenCL -l yes -B 0.02 -P single -m run -M yeast_telomere_150.0bp -I u_yeast_telomere_150.0bp -J binders_yeast_telomere_150.0bp -T ${path_to} -a /scratch/anaconda3/bin;

