#!/usr/bin/python
# -*- coding: utf-8 -*-
"""@package docstring
The module 'openmm_copolymer_functions.py' is a collection of functions used to define
a Kremer-Grest polymer with bending rigidity and uni-dimensionnal epigenetic informations.
This module goes with a python script 'openmm_copolymer.py' that is an example on how to
create and run your model.
The following code (has been developped by Pascal Carrivain) is distributed under MIT licence.
"""
import numba
import json
import math
import sys
import random
import simtk.openmm as mm
import simtk.unit as unit
from sys import stdout
from mpl_toolkits import mplot3d
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np

def get_kBT(Temp: unit.Quantity) -> unit.Quantity:
    return unit.BOLTZMANN_CONSTANT_kB*unit.AVOGADRO_CONSTANT_NA*Temp
    #return unit.BOLTZMANN_CONSTANT_kB*Temp

def minimal_time_scale(mass: list,sigmas: list,Temp: unit.Quantity=300.0*unit.kelvin) -> unit.Quantity:
    """return the minimal time-scale based on the mass and size of the particle.
    Parameters
    ----------
    mass   : mass of each particle (list of unit.Quantity)
    sigmas : size of each particle (list of unit.Quantity)
    Temp   : temperatire of the system (unit.Quantity)
    Returns
    -------
    the minimal time-scale (unit.Quantity)
    Raises
    ------
    """
    kBT=get_kBT(Temp)
    M=len(mass)
    tau=sigmas[0]*unit.sqrt(mass[0]/kBT)
    for i in range(1,M):
        if (sigmas[i]*unit.sqrt(mass[i]/kBT)).__lt__(tau):
            tau=sigmas[i]*unit.sqrt(mass[i]/kBT)
    return tau

def check_volume_fraction(sizes: list,L: unit.Quantity):
    """check the volume fraction, exit if it is greater than one.
    Parameters
    ----------
    sizes : size of each particle (list of unit.Quantity)
    L     : size of the box (unit.Quantity)
    Returns
    -------
    Raises
    ------
    """
    # volume
    V=0.0*unit.nanometer*unit.nanometer*unit.nanometer
    for s in sizes:
        V+=4.0*math.pi*(0.5*s)*(0.5*s)*(0.5*s)/3.0
    # volume fraction
    if (V/(L*L*L))>=1.0:
        print("fraction of occupied volume "+str(V/(L*L*L))+" is greater than one, exit.")
        sys.exit()

@numba.jit
def from_i_to_bead(i: int,N: int) -> int:
    """return the index 'i' projected to the ring chain.
    Parameters
    ----------
    i : index (int) from the start of the chain
    N : the number of beads in the ring chain (int)
    Returns
    -------
    return the index 'i' (int) projected onto the ring chain.
    Raises
    ------
    """
    if i<0:
        return N-(-i)%N
    elif i>=N:
        return i%N
    else:
        return i

@numba.jit(nopython=True)
def build_distance2_matrix(d2_matrix: list,positions: list,sample: int=0,sbin: int=1) -> tuple:
    """return the square distance matrix (the positions are unwrapped).
    Parameters
    ----------
    d2_matrix : square distance matrix, the new snapshot is aggregated to the matrix (N by N list of float)
    positions : list of the positions (in nanometer) from 'get_positions_from_context' function
    sample    : the number of matrices we already aggregated (int)
    sbin      : bin size
                if bin > 1 the distance are calculated between com made of 'bin' particles
    Returns
    -------
    average of the square distance matrix (list of float), size of the sample (integer)
    Raises
    ------
    if the length of matrix is different of G*(G+1)/2, resize to G*(G+1)/2 and sample=0.
    """
    sbin=max(sbin,1)
    # # number of chains
    # C=get_number_of_chains(context)
    # number of particles
    N=len(positions)//get_particles_per_bead()
    # number of particles per bin
    G=N//sbin+int((N%sbin)>0)
    if len(d2_matrix)!=(G*(G+1)//2):
        print("The length of the distances matrix is different from G*(G+1)/2, resize to G*(G+1)/2 and sample=0.")
        sample=0
    # new square distances matrix
    new_d2_matrix=[0.0]*(G*(G+1)//2)
    # com of 'bin' particles
    cx=[0.0]*G
    cy=[0.0]*G
    cz=[0.0]*G
    for i in range(0,N-sbin+1,sbin):
        ii=i//sbin
        for j in range(i,i+sbin):
            pj=positions[j]
            cx[ii]+=pj[0]
            cy[ii]+=pj[1]
            cz[ii]+=pj[2]
        cx[ii],cy[ii],cz[ii]=cx[ii]/sbin,cy[ii]/sbin,cz[ii]/sbin
    X=len(cx)
    # loop over the pairwize (i,j)
    if sample==0:
        for i in range(X-1):
            cx_ii=cx[i]
            cy_ii=cy[i]
            cz_ii=cz[i]
            for j in range(i+1,X):
                # distance
                dx=cx_ii-cx[j]
                dy=cy_ii-cy[j]
                dz=cz_ii-cz[j]
                index=i*G-i*(i+1)//2+j
                new_d2_matrix[index]=dx*dx+dy*dy+dz*dz
    else:
        sample_inverse=1/(sample+1)
        for i in range(X-1):
            cx_ii=cx[i]
            cy_ii=cy[i]
            cz_ii=cz[i]
            for j in range(i+1,X):
                # distance
                dx=cx_ii-cx[j]
                dy=cy_ii-cy[j]
                dz=cz_ii-cz[j]
                index=i*G-i*(i+1)//2+j
                new_d2_matrix[index]=(d2_matrix[index]*sample+dx*dx+dy*dy+dz*dz)*sample_inverse
    new_sample=sample+1
    return new_d2_matrix,new_sample

@numba.jit(nopython=True)
def from_distance2_to_distance(matrix: list) -> list:
    """return the square root of each element from the distance^2 matrix D(i,j).
    Parameters
    ----------
    matrix : the matrix you want to take the square root of each element (list of float)
    Returns
    -------
    the matrix made of sqrt(D(i,j)) (list of float).
    Raises
    ------
    """
    return [math.sqrt(m) for m in matrix]

def pairwize_function(positions: list,locii: list,bin: float=10.0,L: float=1000.0,PBC: bool=False,gr: list=[],plot: bool=False,name: str="") -> list:
    """return the pairwize function of a specific part of the system.
    Parameters
    ----------
    positions : positions (in nanometer) return by 'get_positions_from_context' function
    locii     : specific part of the system (list of int)
    gr        : pairwize function (list of int)
    bin       : size of the bin (in nanometer) for the pairwize function calculation (float)
    L         : size of the box (in nanometer) (float)
    PBC       : use or not the periodic boundary conditions (bool)
    plot      : False or True (bool)
    title     : title for the plot (str)
    Returns
    -------
    the pairwize function (size may change from the one passed to the function) as a list of int.
    Raises
    ------
    """
    locii_positions=[positions[l] for l in locii]
    # number of particles in the part
    L=len(locii)
    # pairwize function length
    G=len(gr)
    for i in range(L-1):
        pi=locii_positions[i]
        for j in range(i+1,L):
            pj=locii_positions[j]
            dx=pi[0]-pj[0]
            dy=pi[1]-pj[1]
            dz=pi[2]-pj[2]
            if PBC:
                dx-=L*np.rint(dx/L)
                dy-=L*np.rint(dy/L)
                dz-=L*np.rint(dz/L)
            bin_index=int(math.sqrt(dx*dx+dy*dy+dz*dz)/bin)
            # bin outside the length of gr ?
            if bin_index>=G:
                gr=gr+[0]*(bin_index-G+1)
                G=bin_index+1
            gr[bin_index]+=1
    # plot ?
    if plot:
        sum_gr=0.0/(bin*bin*bin)
        for g in range(G):
            sum_gr+=gr[g]/(4.0*math.pi*((g+0.5)*bin*(g+0.5)*bin)*bin)
        fig=plt.figure("gr")
        ax=fig.add_axes([0.1,0.1,0.8,0.8],xlim=(0,G*bin),ylim=(0.0,2))#,xscale='log',yscale='log')
        ax.plot([(g+0.5)*bin for g in range(G)],[(gr[g]/(4.0*math.pi*((g+0.5)*bin*(g+0.5)*bin)*bin))/sum_gr for g in range(G)])
        ax.xaxis.set_label("r in "+str(bin))
        ax.yaxis.set_label("g(r)")
        fig.savefig(name)
        fig.clf()
        plt.close("gr")
    return gr

@numba.jit(nopython=True)
def where_the_beads_are(positions: list,size: float) -> tuple:
    """return a partition of the space.
    Parameters
    ----------
    positions : list of positions from 'get_positions_from_context'
    size      : size of each partition block (in nanometer)
    Returns
    -------
    the partition as a python dictionnary (dict), the bead to block (list) correspondance.
    Raises
    ------
    """
    size=1.0/size
    N=len(positions)
    # space partitioning
    min_x=positions[0][0]*size
    min_y=positions[0][1]*size
    min_z=positions[0][2]*size
    max_x=min_x
    max_y=min_y
    max_z=min_z
    for p in positions:
        min_x=min(min_x,p[0]*size-1)
        min_y=min(min_y,p[1]*size-1)
        min_z=min(min_z,p[2]*size-1)
        max_x=max(max_x,p[0]*size+1)
        max_y=max(max_y,p[1]*size+1)
        max_z=max(max_z,p[2]*size+1)
    Px=int(max_x-min_x)+1
    Py=int(max_y-min_y)+1
    Pz=int(max_z-min_z)+1
    P=Px*Py*Pz
    # space partitioning
    # step 1
    bead_to_block=[-1]*N
    beads_in_block=[0]*P
    for i in range(N):
        pi=positions[i]
        index=int(pi[0]*size-min_x)*Py*Pz+int(pi[1]*size-min_y)*Pz+int(pi[2]*size-min_z)
        beads_in_block[index]+=1
        bead_to_block[i]=index
    # step 2
    max_beads_in_block=0
    for i in range(N):
        index=bead_to_block[i]
        max_beads_in_block=max(max_beads_in_block,beads_in_block[index])
    # step 3
    block_to_beads=np.full((P,max_beads_in_block),-1)
    beads_in_block=[0]*P
    for i in range(N):
        index=bead_to_block[i]
        block_to_beads[index][beads_in_block[index]]=i
        beads_in_block[index]+=1
    return block_to_beads,beads_in_block,bead_to_block,Px,Py,Pz

@numba.jit(nopython=True)
def get_close_ij(positions: list,cut: float=1.0,cut_ij: int=1,linear: bool=True) -> tuple:
    """simple algorithm to return the pairwize with distance <= cut.
    Parameters
    ----------
    positions : list of positions returns by 'get_positions_from_context'
    cut       : cutoff (in nanometer) to define close (i,j) (float)
    cut_ij    : keep only the pairwize with |j-i|>=cut_ij (integer)
    linear    : linear polymer (True) or ring polymer (False)
    Returns
    -------
    a list made of the close (i,j) with |j-i|>=cut_ij.
    Raises
    ------
    """
    N=len(positions)
    # space partitioning
    block_to_beads,beads_in_block,bead_to_block,Px,Py,Pz=where_the_beads_are(positions,cut)
    # loop over cells
    close_ij=[]
    C=0
    for i in range(N):
        # block bead 'i'
        block_i=bead_to_block[i]
        iz=block_i%Pz
        iy=((block_i-iz)//Pz)%Py
        ix=(block_i-iz-iy*Pz)//(Py*Pz)
        sx=max(0,ix-1)
        ex=min(ix+1,Px-1)+1
        sy=max(0,iy-1)
        ey=min(iy+1,Py-1)+1
        sz=max(0,iz-1)
        ez=min(iz+1,Pz-1)+1
        # position bead 'i'
        pi=positions[i]
        px=pi[0]
        py=pi[1]
        pz=pi[2]
        # loop over neighboor cells
        for x in range(sx,ex):
            for y in range(sy,ey):
                for z in range(sz,ez):
                    block=x*Py*Pz+y*Pz+z
                    partition_block=block_to_beads[block][:beads_in_block[block]]
                    # loop over bead "j"
                    for j in partition_block:
                        if j>=i:
                            break
                        pj=positions[j]
                        dx=pj[0]-px
                        dy=pj[1]-py
                        dz=pj[2]-pz
                        ij=(i-j)*int(linear)+min(N-(i-j),i-j)*int(not linear)
                        if (dx*dx+dy*dy+dz*dz)<=(cut*cut) and ij>=cut_ij:
                            close_ij.append([j,i])
                            C+=1
    return close_ij,C

@numba.jit(nopython=True)
def where_is_x_in_the_list(x: int, l: list) -> int:
    ll=len(l)
    for i in range(ll-1):
        if x>=l[i] and x<=l[i+1]:
            return i
    return -1

# @numba.jit(nopython=True)
def contact_matrix(matrix: list,Ps: np.ndarray,sample: int=0,sbin: int=1,positions: list=[],N_per_C: list=[0],icut: float=0.0,scut: float=1.0,linear: bool=True) -> tuple:
    """return the contact matrix calculated from the overlap of pairwize (i,j).
    if the matrix is N by N and the bin size is B the function returns a G by G matrix with G=N/B.
    Parameters
    ----------
    matrix    : contact matrix, the new snapshot is aggregated to the matrix (list of G*(G+1)/2 float)
    Ps        : contact probability of each chain, one column per chain (np.ndarray)
    sample    : the number of snapshots (int)
    bin       : size of the bin for the contact matrix (int)
    positions : positions return by 'get_positions_from_context' function (list)
    N_per_C   : number of beads for each chain (list of int)
    icut      : inf threshold in nanometer for the +1 contact (float)
    scut      : sup threshold in nanometer for the +1 contact (float)
    linear    : True (linear chain) or False (ring chain) (bool)
    Returns
    -------
    sum of contact matrices (N*(N+1)/2 tuple of float).
    the new list for the contact probability (N-tuple of float).
    the new number of snapshots (int).
    Raises
    ------
    if sbin < 1, the function takes sbin = 1.
    if the length of the matrix is different of G*(G+1)/2, do nothing.
    if the length of the matrix is 0, resize to G*(G+1)/2.
    """
    int_linear=int(linear)
    if sbin<1:
        sbin=1
        print("sbin is < 1, the function sets sbin to 1.")
    else:
        pass
    # sbin=max(sbin,1)
    # number of particles 
    N=len(positions)//get_particles_per_bead()
    # number of particles per bin
    G=0
    for n in N_per_C:
        G+=n//sbin+int((n%sbin)>0)
    # G=N//sbin+int((N%sbin)>0)
    # chains
    chains=int(len(N_per_C))
    max_N=max(N_per_C)#int(np.amax(N_per_C))
    # contact matrix
    if len(matrix)!=(G*(G+1)//2):
        print("The length of matrix is different of G*(G+1)/2, resize to G*(G+1)/2.")
        matrix=[0]*(G*(G+1)//2)
        Ps=np.zeros(shape=(max_N,chains))
        sample=0
    else:
        pass
    # P(s)
    if Ps.shape!=(max_N,chains):
        print("Resize the P(s) to the correct value ...")
        # Ps=np.zeros(shape=(max_N,chains),type=float)
        Ps=np.zeros(shape=(max_N,chains))
        # Ps=np.array([[0.0],[0.0]]).reshape(max(N_per_C),chains)
        # for n in N_per_C:
        #     M=n*int_linear+(n//2+1)*(1-int_linear)
        #     Ps=Ps+[0]*M
    # array for the instantaneous P(s,t)
    # Pst=np.zeros(shape=(max_N,chains),type=int)
    Pst=np.zeros(shape=(max_N,chains))
    # start of each chain
    chain0=[0]*chains
    for i in range(chains-1):
        chain0[i+1]=chain0[i]+N_per_C[i]
    # chain0=np.cumsum([0]+N_per_C)
    # get the close (i,j)
    close_ij,C=get_close_ij(positions,scut,1,linear)
    for c in close_ij:
        ii=c[0]
        jj=c[1]
        pi=positions[ii]
        pj=positions[jj]
        dx=pj[0]-pi[0]
        dy=pj[1]-pi[1]
        dz=pj[2]-pi[2]
        # contact matrix
        d2=dx*dx+dy*dy+dz*dz
        if d2>=(icut*icut) and d2<=(scut*scut):
            mi=ii//sbin
            Mi=jj//sbin
            matrix[mi*G-mi*(mi+1)//2+Mi]+=1
            # which chain(s) ?
            ci=where_is_x_in_the_list(ii,chain0)
            Ci=where_is_x_in_the_list(jj,chain0)
            # P(s,t) : same chain ?
            if ci==Ci:
                ji=(jj-ii)*int_linear+min(jj-ii,N_per_C[ci]-jj+ii)*(1-int_linear)
                # Pst[chain0[ci]+ji]+=1
                Pst[ji,ci]+=1
    # P(s) normalization
    if linear:
        for i in range(chains):
            M=N_per_C[i]
            for m in range(M):
                Ps[m,i]=(sample*Ps[m,i]+Pst[m,i]/(M-m))/(sample+1)
    else:
        for i in range(chains):
            M=N_per_C[i]//2+1
            if (N_per_C[i]%2)==0:
                Ps[M-1,i]=(sample*Ps[M-1,i]+Pst[M-1,i]/(N_per_C[i]//2))/(sample+1)
                for m in range(M-1):
                    Ps[m,i]=(sample*Ps[m,i]+Pst[m,i]/N_per_C[i])/(sample+1)
            else:
                for m in range(M):
                    Ps[m,i]=(sample*Ps[m,i]+Pst[m,i]/N_per_C[i])/(sample+1)
    sample+=1
    return matrix,Ps,sample

def draw_Ps(Ps: list,name: str="/scratch/Ps"):
    """draw the contact probability to a file 'name'.
    Parameters
    ----------
    Ps        : a list for the contact probability (list of float)
    name      : file name of the *.png (str)
    Returns
    -------
    Raises
    ------
    """
    # write the P(s) down to a file
    with open(name+".out","w") as out_file:
        out_file.write("P(s)"+"\n")
        for p in Ps:
            out_file.write(str(p)+"\n")
    M=len(Ps)
    # plot the P(s)
    if not all(v==0.0 for v in Ps):
        fig=plt.figure("Ps")
        ax=fig.add_axes([0.1,0.1,0.8,0.8],xlim=(1,M),ylim=(0.001,2),xlabel="s",ylabel="P(s)",xscale='log',yscale='log')
        ax.plot([i for i in range(M)],Ps)
        plt.savefig(name+".png")
        fig.clf()
        plt.close("Ps")

@numba.jit(nopython=True)
def contacts_4C(positions: list,contacts: list=[0],locus: int=0,icut: float=0.0,scut: float=1.0,linear: bool=True) -> tuple:
    """return the contacts between locus and locii.
    Parameters
    ----------
    positions : positions return by 'get_positions_from_context' function
    contacts  : the list of contacts locus v. all, the last element is the sample size (int)
    locus     : the locus index v. all for the 4C experiment (int)
    icut      : inf threshold (in nano-meter) for the +1 contact (float)
    scut      : sup threshold (in nano-meter) for the +1 contact (float)
    linear    : True (linear chain) or False (ring chain)
    Returns
    -------
    sum of contacts (list of N integer).
    the new number of snapshots (integer).
    Raises
    ------
    if the length of the list of contacts is different of N, resize to N.
    if the length of the list of contacts is 0, resize to N.
    """
    # number of particles
    N=len(positions)//get_particles_per_bead()
    C=len(contacts)
    # size of the 4C contacts
    if C!=(N+1):
        print("contacts_4C : the length of the list of contacts is different of N+1, resize to N+1.")
        contacts=[0]*(N+1)
    else:
        pass
    # 4C
    position=positions[locus]
    ip=0
    non_zero=0
    for p in positions:
        dx=p[0]-position[0]
        dy=p[1]-position[1]
        dz=p[2]-position[2]
        d2=dx*dx+dy*dy+dz*dz
        if d2>=(icut*icut) and d2<=(scut*scut):
            non_zero+=1
            contacts[ip]+=1
        ip+=1
    contacts[locus]=0
    contacts[N]+=1
    return contacts

def draw_4C(contacts: list,resolution: int=1,x_inf: int=0,x_sup: int=1000000,path_to_draw: str="/scratch/4C.png",path_to_write: str="/scratch/4C.out"):
    """draw and write the 4C contacts.
    Parameters
    ----------
    contacts      : the list of contacts locus v. all, the last element is the sample size (int)
    resolution    : bp (int)
    x_inf         : x axis inf for the plot (int)
    x_sup         : x axis sup for the plot (int)
    path_to_draw  : file name (str) to draw the 4C
    path_to_write : file name (str) to write the 4C
    Returns
    -------
    Raises
    ------
    """
    # number of contacts
    L=len(contacts)
    C=L-1
    # sample size
    sample=contacts[L-1]
    contacts=contacts[:C]
    # draw
    ymin=0.001
    fig=plt.figure("4C")
    ax=fig.add_axes([0.1,0.1,0.8,0.8],xlim=(x_inf*resolution,x_sup*resolution),ylim=(ymin,1),xlabel="genomic position in bp",ylabel="contacts",yscale='log')
    ax.plot([i*resolution for i in range(C)],[max(0.1*ymin,c/sample) for c in contacts])
    fig.savefig(path_to_draw)
    fig.clf()
    plt.close("4C")
    # write
    with open(path_to_write,"w") as out_file:
        out_file.write("contacts sample\n")
        for c in contacts:
            out_file.write(str(c)+" "+str(sample)+"\n")

def distance2(a: mm.Vec3,b: mm.Vec3) -> unit.Quantity:
    dx=a[0]-b[0]
    dy=a[0]-b[0]
    dz=a[0]-b[0]
    return dx*dx+dy*dy+dz*dz
def is_distance2_inf_to_x(a: mm.Vec3,b: mm.Vec3,c: unit.Quantity) -> int:
    dx=a[0]-b[0]
    dy=a[0]-b[0]
    dz=a[0]-b[0]
    return int(((dx*dx+dy*dy+dz*dz)/c)<1.0)

def draw_symmetric_matrix(matrix: list,name: str="",log: bool=True):
    """draw the matrix and save it to file name.png.
    Parameters
    ----------
    matrix : the symmetric matrix to draw (list of N*(N+1)/2tuple float)
    name   : the name of the file to save the colormap (string)
    log    : if True plot the log of each matrix element (bool)
    Returns
    -------
    Raises
    ------
    """
    # The symmetric matrix NxN is stored in a L-tuple of float
    # get the L=N*(N+1)/2
    L=len(matrix)
    # get the N=(sqrt(8*L+1)-1)/2
    N=int((math.sqrt(8*L+1)-1)/2)
    # build the numpy matrix from the L-tuple
    np_matrix=np.zeros(shape=(N,N))
    for i in range(N):
        for j in range(i,N):
            np_matrix[i,j]=matrix[i*N-int(i*(i+1)/2)+j]
            np_matrix[j,i]=np_matrix[i,j]
    # minimum of the matrix
    min_matrix=max(np.amin(np_matrix),1)
    np_matrix=np.where(np_matrix==0,min_matrix,np_matrix)
    # draw the numpy matrix
    cmap=clr.LinearSegmentedColormap.from_list('from_blue_to_red',['blue','white','red'],N=256)
    plt.figure(1)
    if log:
        plt.matshow(np_matrix,cmap=cmap,norm=clr.LogNorm(vmin=min_matrix,vmax=np.amax(np_matrix)))
    else:
        plt.matshow(np_matrix,cmap=cmap)
    plt.savefig(name+".png")
    plt.clf()
    plt.close("all")

def flangevin(x: float) -> float:
    """return the value of the Langevin function at x : 1/tanh(x)-1/x.
    Parameters
    ----------
    x : the x where to evaluate the Langevin function (float)
    Returns
    -------
    the value of the Langevin function at x (float)
    Raises
    ------
    """
    if x==0.0:
        return 0.0
    else:
        return 1./math.tanh(x)-1./x

def bending_rigidity(k_bp: int,resolution: int,Temp: unit.Quantity) -> unit.Quantity:
    """return the bending rigidity for a specific resolution (in bp) and Kuhn length (in bp).
    Parameters
    ----------
    k_bp       : Kuhn length in bp (int)
    resolution : resolution of a segment in bp (int)
    Temp       : temperature (unit.Quantity)
    Returns
    -------
    bending rigidity in units of kB*Temperature (float)
    Raises
    ------
    """
    if resolution>=k_bp:
        return 0.0
    mcos=(k_bp/resolution-1.0)/(k_bp/resolution+1.0)
    a=1e-10
    b=1e2
    precision=1e-10
    fa=flangevin(a)-mcos;
    fb=flangevin(b)-mcos;
    m=.5*(a+b);
    fm=flangevin(m)-mcos;
    while abs(a-b)>precision:
        m=0.5*(a+b)
        fm=flangevin(m)-mcos
        fa=flangevin(a)-mcos
        if fm==0.0:
            break
        if (fa*fm)>0.:
            a=m
        else:
            b=m;
    return m*get_kBT(Temp)

def fene(N_per_C: list,Temp: unit.Quantity,sigma: unit.Quantity,linear: bool,PBC: bool) -> mm.CustomBondForce:
    """return a FENE force between consecutive beads.
    Parameters
    ----------
    N_per_C : number of beads for each chain (list of int)
    Temp    : temperature (unit.Quantity)
    sigma   : size of the bond between two consecutive beads (unit.Quantity)
    linear  : True (linear chain) or False (ring chain)
    PBC     : does it use the periodic boundary conditions, False or True ?
    Returns
    -------
    OpenMM CustomBondForce (FENE potential between two consecutive bonds).
    """
    kBT=get_kBT(Temp)
    fene=mm.CustomBondForce('-0.5*K_fene*R0*R0*log(1-(r*r/(R0*R0)))*step(0.99*R0-r)+4*epsilon_fene*((sigma_fene/r)^12-(sigma_fene/r)^6+0.25)*step(cut_fene-r)')
    fene.addGlobalParameter('K_fene',30.0*kBT/(sigma*sigma))
    fene.addGlobalParameter('R0',1.5*sigma)
    fene.addGlobalParameter('epsilon_fene',kBT)
    fene.addGlobalParameter('sigma_fene',sigma)
    fene.addGlobalParameter('cut_fene',math.pow(2.0,1.0/6.0)*sigma)
    index=0
    for n in N_per_C:
        for i in range(n-int(linear)):
            fene.addBond(index+i,index+from_i_to_bead(i+1,n))
        index+=n
    fene.setUsesPeriodicBoundaryConditions(PBC)
    return fene

def pairing(N_per_C: list,C1: int,C2: int,sigma: unit.Quantity,g: unit.Quantity=0.0*unit.joule,PBC: bool=True) -> mm.CustomBondForce:
    """return a shot-range attractive pairing force between homologous beads.
    if the two chains do not have the same length, do nothing.
    Parameters
    ----------
    N_per_C : number of beads for each chain (list of int)
    C1      : fisrt chain (int)
    C2      : second chain (int)
    sigma   : size of the Lennard-Jones potential
    g       : short-range pairing strength (unit.Quantity)
    PBC     : does it use the periodic boundary conditions, False or True ? (bool)
    Returns
    -------
    OpenMM CustomBondForce (short-range attractive potential for pairing between two homologous chains).
    if the two chains do not have the same length, return an empty bond custom force.
    Raises
    ------
    """
    pairing=mm.CustomBondForce('4*epsilon_pairing*((sigma_pairing/r)^12-(sigma_pairing/r)^6+0.25)')
    pairing.addGlobalParameter('epsilon_pairing',g)
    pairing.addGlobalParameter('sigma_pairing',sigma)
    if N_per_C[C1]==N_per_C[C2]:
        N=sum(N_per_C)
        N1=N-sum(N_per_C[C1:])
        N2=N-sum(N_per_C[C2:])
        L1=N_per_C[C1]
        for i in range(L1):
            pairing.addBond(N1+i,N2+i)
    pairing.setUsesPeriodicBoundaryConditions(PBC)
    return pairing

def tt_bending(N_per_C: list,Kb: list,linear: bool,PBC: bool) -> mm.CustomAngleForce:
    """return a bending force between three consecutive beads.
    Parameters
    ----------
    N_per_C : number of beads for each chain (list of int)
    Kb      : bending rigidity along the chain (list of unit.Quantity)
    linear  : linear (True) or ring chain (False) (bool)
    PBC     : does it use the periodic boundary conditions, False or True ? (bool)
    Returns
    -------
    OpenMM CustomAngleForce bending force between two consecutive tangent vector t.
    Raises
    ------
    if the number of angles does not correspond to the length of bending rigidity input argument 'Kb'.
    """
    int_linear=int(linear)
    # number of bonds
    N=sum(N_per_C)-len(N_per_C)*int_linear
    # number of angles
    A=sum(N_per_C)-2*len(N_per_C)*int_linear
    # bending as mm.CustomAngleForce
    bending=mm.CustomAngleForce("f_tt*Kb_tt*(1.0-cos(theta-A_tt))")
    bending.addPerAngleParameter("Kb_tt")
    bending.addGlobalParameter("f_tt",1.0)
    bending.addGlobalParameter("A_tt",math.pi)
    bending.addEnergyParameterDerivative("f_tt")
    if len(Kb)==A:
        # add angles
        index=0
        angles=0
        for n in N_per_C:
            for i in range(n-2*int_linear):
                bending.addAngle(index+i,index+from_i_to_bead(i+1,n),index+from_i_to_bead(i+2,n),[Kb[angles]])
                angles+=1
            index+=n
    else:
        print("The number of angles does not correspond to the length of bending rigidity, return empty 'CustomAngleForce'.")
    bending.setUsesPeriodicBoundaryConditions(PBC)
    return bending

def wca(N_per_C: list,Temp: unit.Quantity,sigma: unit.Quantity,binders_size: list,linear: bool,PBC: bool=True) -> mm.CustomNonbondedForce:
    """return a WCA potential to resolve excluded volume constraints.
    Parameters
    ----------
    N_per_C      : number of beads for each chain (list of int)
    Temp         : temperature (unit.Quantity)
    sigma        : size of the bead (unit.Quantity)
    binders_size : size of each binder (list of unit.Quantity)
    linear       : linear (True) or ring chain (False) (bool)
    PBC          : does it use periodic boundary conditions ? (bool)
    Returns
    -------
    OpenMM CustomNonbondedForce (WCA potential)
    """
    a_cutoff=math.pow(2.0,1.0/6.0)
    # wca=mm.CustomNonbondedForce('4*epsilon_wca*((sigma_wca/r)^12-(sigma_wca/r)^6+0.25)*step(a_cutoff_wca*sigma_wca-r);sigma_wca=f_wca*sqrt(sigma_wca1*sigma_wca2)')
    wca=mm.CustomNonbondedForce('4*epsilon_wca*((sigma_wca/r)^12-(sigma_wca/r)^6+0.25)*step(a_cutoff_wca*sigma_wca-r);sigma_wca=f_wca*0.5*(sigma_wca1+sigma_wca2)')
    wca.addGlobalParameter('a_cutoff_wca',a_cutoff)
    wca.addGlobalParameter('epsilon_wca',get_kBT(Temp))
    wca.addGlobalParameter('f_wca',1.0)
    wca.addPerParticleParameter('sigma_wca')
    wca.addEnergyParameterDerivative("epsilon_wca")
    wca.addEnergyParameterDerivative("f_wca")
    if PBC:
        wca.setNonbondedMethod(mm.NonbondedForce.CutoffPeriodic)
    else:
        wca.setNonbondedMethod(mm.NonbondedForce.CutoffNonPeriodic)
    # add N particles
    N=sum(N_per_C)
    for i in range(N):
        wca.addParticle([sigma])
    # add the binders
    max_sigma=sigma
    for s in binders_size:
        wca.addParticle([s])
        if s.__gt__(max_sigma):
            max_sigma=s
    # cutoff
    wca.setCutoffDistance(a_cutoff*max_sigma)
    # no WCA between particles of the same bond
    index=0
    for n in N_per_C:
        for i in range(n-int(linear)):
            wca.addExclusion(index+i,index+from_i_to_bead(i+1,n))
        index+=n
    wca.setForceGroup(0)# group 0 for the WCA interactions
    return wca

def go_copolymer(resolution: int,k_bp: int,k_nm: float,states: list,interactions,N_per_C: list,linear: bool,PBC: bool) -> mm.CustomNonbondedForce:
    """return a gaussian overlap for copolymer interactions.
    Parameters
    ----------
    resolution   : the resolution of the bead in bp (integer)
    k_bp         : Kuhn length in bp (integer)
    k_nm         : Kuhn length in nm (float)
    states       : a list of states, example [1,0,3,4,...,3,4,4,4] (list of int)
    interactions : a list of tuple, example [[0,0,-3.0],[0,1,0.0],...,[1,4,1.0]] (N-tuple of [integer,integer,unit.Quantity])
    N_per_C      : number of beads for each chain (list of int)
                   it is needed for the bond exclusion.
    linear       : linear (True) or ring chain (False)
    PBC          : does it use periodic boundary conditions ? (bool)
    Returns
    -------
    OpenMM CustomNonbondedForce (gaussian overlap for copolymer interactions)
    """
    # counting the number of states (based on their names)
    S=len(states)
    unique_states=[states[0]]
    for r in range(1,S):
        unique=True
        for s in range(0,r):
            unique=(unique and not states[r]==states[s])
        if unique:
            unique_states.append(states[r])
    n_states=len(unique_states)
    print("The function 'go_copolymer' found "+str(n_states)+" states.")
    # creating the custom force expression
    go=mm.CustomNonbondedForce("")
    for s in range(n_states):
        go.addPerParticleParameter("p_"+unique_states[s])# 0 or 1 for the state "u"
    # no need to add a zero interaction to the custom force expression
    expression=""
    for i in interactions:
        r0=i[0]
        s0=i[1]
        print("interaction between "+r0+" and "+s0+" is "+str(i[2])+".")
        expression+="p_"+r0+"1"+"*"+"p_"+s0+"2*D_"+r0+"_"+s0+"+"
        if r0!=s0:
            expression+="p_"+s0+"1"+"*"+"p_"+r0+"2*D_"+r0+"_"+s0+"+"
        # interaction strength between state "r" and state "s" with D(r,s)=D(s,r)
        go.addGlobalParameter("D_"+r0+"_"+s0,i[2])
    expression=expression[:-1]
    print("The epigenetic interaction expression built from the interaction file is : "+expression+".")
    Rg2=math.pow(k_nm*resolution/k_bp,2.0)/6.0
    go.setEnergyFunction("D_r_s*go_cst*exp(exp_cst*r^2);D_r_s="+expression)
    go.addGlobalParameter("go_cst",math.pow(0.75/math.pi,1.5)*resolution*resolution/(Rg2*math.sqrt(Rg2)))
    go.addGlobalParameter("exp_cst",-0.75/Rg2)
    if PBC:
        go.setNonbondedMethod(mm.NonbondedForce.CutoffPeriodic)
    else:
        go.setNonbondedMethod(mm.NonbondedForce.CutoffNonPeriodic)
    go.setCutoffDistance(5.0*math.sqrt(Rg2))
    # per particle parameter p(s)
    for i in range(S):
        state=states[i]
        go.addParticle([1*int(state==unique_states[s]) for s in range(n_states)])
    # no go interactions between particles of the same bond
    index=0
    for n in N_per_C:
        for i in range(n-int(linear)):
            go.addExclusion(index+i,index+from_i_to_bead(i+1,n))
        index+=n
    go.setForceGroup(1)
    return go

def lj_copolymer(resolution: int,k_bp: int,k_nm: float,sigma: list,states: list,interactions: list,N_per_C: list,linear: bool,PBC: bool) -> mm.CustomNonbondedForce:
    """return a lj potential for copolymer interactions.
    Parameters
    ----------
    resolution   : the resolution of the bead in bp (integer)
    k_bp         : Kuhn length in bp (integer)
    k_nm         : Kuhn length in nm (float)
    sigma        : size of each bead (list of unit.Quantity)
    states       : a list of states, example [1,0,3,4,...,3,4,4,4] (list of int)
    interactions : a list of tuple, example [[0,0,-3.0],[0,1,0.0],...,[1,4,1.0]] (N-tuple of [integer,integer,unit.Quantity])
    N_per_C      : number of beads for each chain (list of int)
                   it is needed for the bond exclusion.
    linear       : linear (True) or ring chain (False)
    PBC          : does it use periodic boundary conditions ? (bool)
    Returns
    -------
    OpenMM CustomNonbondedForce (gaussian overlap for copolymer interactions)
    Raises
    ------
    """
    # counting the number of states (based on their names)
    S=len(states)
    unique_states=[states[0]]
    for r in range(1,S):
        unique=True
        for s in range(0,r):
            unique=(unique and not states[r]==states[s])
        if unique:
            unique_states.append(states[r])
    n_states=len(unique_states)
    print("The function 'lj_copolymer' found "+str(n_states)+" states.")
    # max(sigma)
    max_sigma=sigma[0]
    for s in sigma:
        if s.__gt__(max_sigma):
            max_sigma=s
    # creating the custom force expression
    lj=mm.CustomNonbondedForce("")
    lj.addPerParticleParameter("sigma_lj")
    for u in unique_states:
        lj.addPerParticleParameter("p_"+u)# 0 or 1 for the state "u"
    # no need to add a zero interaction to the custom force expression
    expression=""
    for i in interactions:
        r0=i[0]
        s0=i[1]
        print("interaction between "+r0+" and "+s0+" is "+str(i[2])+".")
        expression+="p_"+r0+"1"+"*"+"p_"+s0+"2*D_"+r0+"_"+s0+"+"
        if r0!=s0:
            expression+="p_"+s0+"1"+"*"+"p_"+r0+"2*D_"+r0+"_"+s0+"+"
        # interaction strength between state "r" and state "s" with D(r,s)=D(s,r)
        lj.addGlobalParameter("D_"+r0+"_"+s0,i[2])
    expression=expression[:-1]
    print("The epigenetic interaction expression built from the interaction file is : "+expression+".")
    # on/off the LJ copolymer potential
    lj.addGlobalParameter("lj_on_off",1)
    # lj.setEnergyFunction("lj_on_off*4*D_r_s*((sigma_lj/r)^12-(sigma_lj/r)^6);D_r_s="+expression+";sigma_lj=sqrt(sigma_lj1*sigma_lj2)")
    lj.setEnergyFunction("lj_on_off*4*D_r_s*((sigma_lj/r)^12-(sigma_lj/r)^6);D_r_s="+expression+";sigma_lj=0.5*(sigma_lj1+sigma_lj2)")
    if PBC:
        lj.setNonbondedMethod(mm.NonbondedForce.CutoffPeriodic)
    else:
        lj.setNonbondedMethod(mm.NonbondedForce.CutoffNonPeriodic)
    # cutoff at U~0.01*D_r_s
    lj.setCutoffDistance(2.15*max_sigma)
    # per particle parameter p(s)
    for i in range(S):
        state=states[i]
        lj.addParticle([sigma[i]]+[1*int(state==u) for u in unique_states])
    # no lj interactions between particles of the same bond
    index=0
    for n in N_per_C:
        for i in range(n-int(linear)):
            lj.addExclusion(index+i,index+from_i_to_bead(i+1,n))
        index+=n
    lj.setForceGroup(1)
    return lj

def on_off_block_copolymer(context: mm.Context,on_off: str="off"):
    """on/off the LJ block-copolymer interactions.
    Parameters
    ----------
    context : OpenMM context
    on_off  : turn on/off (str)
    Returns
    -------
    Raises
    ------
    """
    if on_off=="off" or on_off=="on":
        context.setParameter("lj_on_off",0*int(on_off=="off")+1*int(on_off=="on"))
    else:
        print("Input parameter 'on_off' accepts only 'on' or 'off' as value.")

def read_binders(name: str="",states: list=[]) -> tuple:
    """read and create the binders from the file 'name'.
    the function cannot be called multiple time.
    the header of the file 'name' should be : binder number mass_amu sigma_nm.
    the file 'name' is like :
    first column 'name of the binder',
    second column 'number of binders',
    third column 'mass in amu',
    fourth column 'size of the binder in nanometer'.
    Parameters
    ----------
    name    : name of the file with the description of the binders (str)
    states  : name of epigenetic states along the chain(s) (list of str)
    Returns
    -------
    list (list of str) of states (chains+binders), mass of binders (list of unit.Quantity), size of each binder (list of unit.Quantity), color of each binder (int).
    Raises
    ------
    if you call this function before the chains have been created, do nothing.
    if the file has no header, print a warning.
    """
    if len(states)==0:
        print("no chains have been created so far, do nothing.")
        print("please create the chains first and then the binders that bind to the chains.")
        return [],[],[],[]
    else:
        # read the file and add the binders parameters
        mass=[]
        sizes=[]
        states_to_int={}
        index=0
        colors=[]
        states_with_binders=[s for s in states]
        try:
            with open(name,"r") as in_file:
                lines=in_file.readlines()
                # header ?
                sl=lines[0].split()
                if not(sl[0]=="binder" and sl[1]=="number" and sl[2]=="mass_amu" and sl[3]=="sigma_nm"):
                    print(name+" does not have header, the header of the file 'name' should be : binder number mass_amu sigma_nm.")
                # binders
                for l in lines[1:]:
                    sl=l.split()
                    if sl[0] not in states_to_int.keys():
                        states_to_int[sl[0]]=index
                        index+=1
                    # add the number of binders 'source', mass and size of the binder
                    for i in range(int(sl[1])):
                        states_with_binders.append(str(sl[0]))
                        mass.append(float(sl[2])*unit.amu)
                        sizes.append(float(sl[3])*unit.nanometer)
                        colors.append(states_to_int[sl[0]])
        except IOError:
            print("The function 'read_binders' did not find the file "+name)
        return states_with_binders,mass,sizes,colors

def read_states(name: str) -> tuple:
    """read epigenetic states from the file 'name' and return a list of the particle associated state (from 1 to N).
    the header of the file has to be 'chain monomer state'.
    the first column is the 'index of the chain', the second column is the 'index of the bead' and the third column is the 'state'.
    Parameters
    ----------
    name : name of the file with the description of the epigenetic states (str)
    Returns
    -------
    list (int) of the chain index, list of string that are the states of the N beads, list of int that are the states of the N beads, list of beads per chain (int), number of chains (int), number of beads (int).
    Raises
    ------
    exit if the file 'name' is not found or if the header does not exist.
    """
    states=[]
    states_to_int={}
    colors=[]
    chain_index=[]
    chains=0
    # beads per chain
    N_per_C=[1]
    index=0
    try:
        with open(name,"r") as in_file:
            lines=in_file.readlines()
            # header ?
            sheader=lines[0].split()
            if not(sheader[0]=="chain" and sheader[1]=="monomer" and sheader[2]=="state"):
                print("File "+name+" has no header, exit.")
                sys.exit()
            # colors and chain index, do not read the header
            for l in lines[1:]:
                sl=l.split()
                states.append(sl[2])
                if sl[2] not in states_to_int.keys():
                    states_to_int[sl[2]]=index
                    index+=1
                colors.append(states_to_int[sl[2]])
                chain_index.append(int(sl[0]))
            # beads per chain, do not read the header
            chain0=int((lines[1].split())[0])
            for l in lines[2:]:
                chain1=int((l.split())[0])
                if chain0==chain1:
                    N_per_C[chains]+=1
                else:
                    N_per_C.append(1)
                    chains+=1
                chain0=chain1
    except IOError:
        print(name,"has not been found.")
        sys.exit()
    return chain_index,states,colors,N_per_C,len(N_per_C),sum(N_per_C)

def read_interactions(name: str,Temp: unit.Quantity) -> list:
    """read the matrix of interactions and return a tuple [state 'r',state 's',interaction strength between r and s : D(r,s)].
    for the interaction strength D(r,s) between state 'r' and state 's' we consider only the 'r' < 's' (indeed D(r,s)=D(s,r)).
    first column is the state 'r', second column is the state 's', third column is the interaction strength in kBT.
    Parameters
    ----------
    name : name of the file with the description of the epigenetic interactions
    Temp : temperature of the system (unit.Quantity)
    Returns
    -------
    interaction strengthes between state 'r' and state 's' (N-tuple of [string,string,unit.Quantity])
    Raises
    ------
    exit if the file 'name' is not found.
    """
    interactions=[]
    try:
        with open(name,"r") as in_file:
            lines=in_file.readlines()
            for l in lines:
                sl=l.split()
                interactions.append([sl[0],sl[1],float(sl[2])*get_kBT(Temp)])
        I=len(interactions)
        # for the interaction strength D(r,s) between state 'r' and state 's' we consider only the 'r' < 's' (indeed D(r,s)=D(s,r))
        deja_vu=[False]*I
        for i in range(I):
            r1=interactions[i][0]
            s1=interactions[i][1]
            for j in range(i+1,I):
                r2=interactions[j][0]
                s2=interactions[j][1]
                if (r1==r2 and s1==s2) or (r1==s2 and s1==r2):
                    deja_vu[j]=True
        # build the new interaction strengthes
        interactionstmp=[]
        for i in range(I):
            if not deja_vu[i]:
                r1=interactions[i][0]
                s1=interactions[i][1]
                interactionstmp.append([r1,s1,interactions[i][2]])
    except IOError:
        print(name,"has not been found.")
        sys.exit()
    # print(type(interactionstmp))
    return interactionstmp

def positions_linear_or_ring(N_per_C: list,z_offset: list,sigma: unit.Quantity,binders_size: list,L: unit.Quantity,linear: bool) -> list:
    """return the positions of the bead, the linear or ring polymer fits in the box (with com in the middle).
    if there is multiple linear chains, the maximum contour length is used to fit the box size.
    if there is multiple linear chains, the offset of each chain is located on a cubic lattice.
    if there is multiple ring chains, the center of each ring is located on the z-axis.
    if the chain with bond size 'sigma' does not fit the box, sigma is decreased to sigma0 that fits the box.
    if binder 'i' is added to the system, its size is (sigma0/sigma)*binders_size[i].
    you have to use 'create_initial_conformation' function to grow from sigma0 to sigma and from (sigma0/sigma)*binders_size[i] to binders_size[i].
    Parameters
    ----------
    N_per_C      : number of beads for each chain (list of int)
    z_offset     : for each chain the bead that is located at z=L/2 (list of int)
    sigma        : size of the bond (unit.Quantity)
    binders_size : size of each binder (list of unit.Quantity)
    L            : size of the box (unit.Quantity)
    linear       : linear (True) or ring chain (False)
    Returns
    -------
    list of OpenMM.Vec3, the com is located at the center of the box.
    Raises
    ------
    """
    # number of chains
    C=len(N_per_C)
    # number of beads
    N=sum(N_per_C)
    # positions
    tx,ty,tz=0.0,0.0,1.0
    positions=[]
    if linear:
        # chains on a lattice, x*sqrt(C)+y
        sqrt_C=int(math.sqrt(C))+1
        Lx,Ly=max(sigma,0.25*L)/sqrt_C,max(sigma,0.25*L)/sqrt_C
        # initial bond size for linear chain
        sigma0=(L-4.0*sigma)/max(N_per_C)
        if sigma0.__gt__(sigma):
            sigma0=sigma
        for c in range(C):
            # the linear chains are located on a lattice
            yy=c%sqrt_C
            xx=(c-yy)//sqrt_C
            positions=positions+[mm.Vec3(xx*Lx+i*sigma0*tx,yy*Ly+i*sigma0*ty,(i-z_offset[c])*sigma0*tz) for i in range(N_per_C[c])]
    else:
        # initial bond size for ring chain
        # contour length of the ring chain N_per_C*sigma0
        # angle between two beads 2*pi/N_per_C
        # R*sin(angle/2)=sigma0/2 with R<L/2
        # we want sigma0 the same for all the ring chains (angle is fixed by the number of beads in the chain)
        # therefore, only R can be changed
        sigma0=(L-4.0*sigma)/max(N_per_C)
        if sigma0.__gt__(sigma):
            sigma0=sigma
        for c in range(C):
            angle=2.0*math.pi/N_per_C[c]
            R=0.5*sigma0/math.sin(0.5*angle)
            zc=0.9*L*c/(C-1)
            positions=positions+[mm.Vec3(R*math.cos(i*angle),R*math.sin(i*angle),zc) for i in range(N_per_C[c])]
    # com
    cx=0.0*unit.nanometer
    cy=0.0*unit.nanometer
    cz=0.0*unit.nanometer
    for p in positions:
        cx+=p[0]
        cy+=p[1]
        cz+=p[2]
    # com of the chains to 0.5*L,0.5*L,0.5*L
    dx,dy,dz=0.5*L-cx/N,0.5*L-cy/N,0.5*L-cz/N
    positions=[mm.Vec3(p[0]+dx,p[1]+dy,p[2]+dz) for p in positions]
    # number of binders
    B=len(binders_size)
    # space partition
    l_blocks=int(L/(2.0*sigma))
    s_block=1.0/(L/l_blocks)
    n_blocks=l_blocks*l_blocks*l_blocks
    l_in_block=[0]*n_blocks
    # beads per block estimation
    for p in positions:
        xi=int(p[0]*s_block)
        yi=int(p[1]*s_block)
        zi=int(p[2]*s_block)
        index=xi*l_blocks*l_blocks+yi*l_blocks+zi
        l_in_block[index]+=1
    i_per_block=2*max(l_in_block)
    i_in_block=[-1]*(n_blocks*i_per_block)
    # chains and partition
    for i,p in enumerate(positions):
        xi=int(p[0]*s_block)
        yi=int(p[1]*s_block)
        zi=int(p[2]*s_block)
        index=xi*l_blocks*l_blocks+yi*l_blocks+zi
        i_in_block[index*i_per_block+l_in_block[index]]=i
        l_in_block[index]+=1
    # rescaled sigma
    sigmas=[sigma0]*N+[(sigma0/sigma)*b for b in binders_size]
    # positions of the binders
    for i,s in enumerate(binders_size):
        overlap=True
        # print(i,s)
        while overlap:
            bx=s+(L-2*s)*np.random.random()
            by=s+(L-2*s)*np.random.random()
            bz=s+(L-2*s)*np.random.random()
            # neighborhood of particle 'i'
            overlap=False
            xi=int(bx*s_block)
            yi=int(by*s_block)
            zi=int(bz*s_block)
            for x in range(max(0,xi-1),min(xi+1,l_blocks)):
                for y in range(max(0,yi-1),min(yi+1,l_blocks)):
                    for z in range(max(0,zi-1),min(zi+1,l_blocks)):
                        index=x*l_blocks*l_blocks+y*l_blocks+z
                        for j in range(l_in_block[index]):
                            jj=i_in_block[index*i_per_block+j]
                            pj=positions[jj]
                            ax=pj[0]-bx
                            ay=pj[1]-by
                            az=pj[2]-bz
                            # overlap ?
                            inv_diameter2=1.0/(math.pow(2.0,2.0/6.0)*0.25*(sigmas[i]+sigmas[jj])*(sigmas[i]+sigmas[jj]))
                            if ((ax*ax+ay*ay+az*az)*inv_diameter2)<1.0:
                                overlap=True
                                break
        index=xi*l_blocks*l_blocks+yi*l_blocks+zi
        i_in_block[index*i_per_block+l_in_block[index]]=N+i
        l_in_block[index]+=1
        positions.append(mm.Vec3(bx,by,bz))
    return positions

def v_gaussian(Temp: unit.Quantity,mass: list,seed: int) -> list:
    """return a list of velocities (of size V) distributed according to the Boltzmann distribution with a com motion to 0,0,0.
    Parameters
    ----------
    Temp : temperature (unit.Quantity)
    mass : mass of the beads (list unit.Quantity)
    seed : seed (int)
    Returns
    -------
    V-tuple of OpenMM.Vec3
    """
    random.seed(seed)
    kBT=get_kBT(Temp)
    # number of beads
    V=len(mass)
    # velocities according to Boltzmann distribution
    vx=[unit.sqrt(kBT/mass[v])*random.gauss(0.0,1.0) for v in range(V)]
    vy=[unit.sqrt(kBT/mass[v])*random.gauss(0.0,1.0) for v in range(V)]
    vz=[unit.sqrt(kBT/mass[v])*random.gauss(0.0,1.0) for v in range(V)]
    # com motion
    mean_mass=0.0*unit.amu
    vcom_x,vcom_y,vcom_z=0.0*unit.sqrt(kBT/mass[0]),0.0*unit.sqrt(kBT/mass[0]),0.0*unit.sqrt(kBT/mass[0])
    for v in range(V):
        vcom_x+=vx[v]
        vcom_y+=vy[v]
        vcom_z+=vz[v]
        mean_mass+=mass[v]
    mean_mass/=V
    # com motion to 0,0,0 and kinetic energy
    K=0.0*kBT/mass[0]
    for v in range(V):
        vx[v]-=vcom_x/V
        vy[v]-=vcom_y/V
        vz[v]-=vcom_z/V
        K+=(vx[v]**2+vy[v]**2+vz[v]**2)
    # rescaling of the kinetic energy
    for v in range(V):
        rK=unit.sqrt(1.5*V*kBT/(0.5*mean_mass*K))
        vx[v]*=rK
        vy[v]*=rK
        vz[v]*=rK
    return [mm.Vec3(vx[v],vy[v],vz[v]) for v in range(V)]

# @numba.jit(nopython=True)
def msd(positions: list,bead: int,window: int,iterations: int,msd: dict={"trajectory":[],"msd":[]}) -> tuple:
    """the function calculates the "Mean-Square-Displacement" of the 'bead' or the msd of the 'bead' plus the neighborhood 'window'.
    Parameters
    ----------
    positions       : positions (in nanometer) return by 'get_positions_from_context' function
    bead            : index of the bead (int)
    window          : size of the neighborhood (int)
    iterations      : current iteration (int)
    msd             : dictionnary = {"msd" : ..., "trajectory" : ...}
                      "msd" previous "Mean-Square-Displacement" (list of unit.Quantity)
                      "trajectory" a list of the previous positions of the 'bead' (list of OpenMM.Vec3)
    Returns
    -------
    dictionnary of the "Mean-Square-Displacement" of the 'bead' (list), the trajectory (list) {"trajectory":...,"msd":...}.
    each element of the list 'msd['trajectory']' is like [position (OpenMM.Vec3),iteration (int)].
    each element of the list 'msd['msd'] is like [cumulative displacement (float),sample size (int)].
    Raises
    ------
    """
    # current position
    position_t=positions[bead-window//2]
    xt=position_t[0]
    yt=position_t[1]
    zt=position_t[2]
    for w in range(bead-window//2+1,bead-window//2+window):
        xt+=positions[w][0]
        yt+=positions[w][1]
        zt+=positions[w][2]
    xt/=window
    yt/=window
    zt/=window
    # does 'msd' key exist in the msd dictionnary ?
    if "msd" not in msd.keys():
        msd["msd"]={}
    # does a trajectory exist in the msd dictionnary ?
    if "trajectory" in msd.keys():
        trajectory=msd["trajectory"]
    else:
        msd["trajectory"]=[]
        trajectory=[]
    # loop over the trajectory (does not include difference between the same time)
    T=0
    for t in trajectory:
        dt=iterations-t[1]
        dx=xt-t[0][0]
        dy=yt-t[0][1]
        dz=zt-t[0][2]
        d2=dx*dx+dy*dy+dz*dz
        str_dt=str(dt)
        if str_dt not in msd["msd"].keys():
            msd["msd"][str_dt]=[d2,1]
        else:
            msd["msd"][str_dt][0]+=d2
            msd["msd"][str_dt][1]+=1
        T+=1
    # update the trajectory
    msd["trajectory"].append([position_t,iterations])
    return msd

def draw_msd(msd: dict,mapping: bool=False,name_mapping: str="",name: str="") -> tuple:
    """draw the "Mean-Square-Displacement".
    Parameters
    ----------
    msd          : dictionnary = {"msd" : ..., "trajectory" : ...}
                   "msd" previous "Mean-Square-Displacement" (list of unit.Quantity)
                   "trajectory" a list of the previous positions of the 'bead' (list of OpenMM.Vec3)
    mapping      : temporal mapping only works if plot is True (bool)
    name_mapping : file name for the mapping (str)
                   it is expected the file is two columns with the first one the time in seconds and the second one the MSD in nm^2
                   the header of the file should be 'second msd_nm2'
    name         : name for the MSD plot (str), .png is added to the end
    Returns
    -------
    Raises
    ------
    """
    # plot the MSD ?
    T=len(msd["trajectory"])
    if T>2:
        min_msd=10000.0
        msd_keys=msd["msd"].keys()
        dt=[int(k) for k in msd_keys]
        current_msd=[msd["msd"][k][0]/msd["msd"][k][1] for k in msd_keys]
        # msd : no temporal mapping
        dt_2points=[1000.0,10000.0]
        fig=plt.figure("no_mapping")
        ax=plt.gca()
        ax.plot(dt,current_msd,label="msd")
        ax.plot(dt_2points,[min_msd*t/dt_2points[0] for t in dt_2points],label="t")
        ax.plot(dt_2points,[min_msd*math.pow(t/dt_2points[0],0.75) for t in dt_2points],label="t^3/4")
        ax.plot(dt_2points,[min_msd*math.pow(t/dt_2points[0],0.5) for t in dt_2points],label="t^1/2")
        ax.plot(dt_2points,[min_msd*math.pow(t/dt_2points[0],0.25) for t in dt_2points],label="t^1/4")
        ax.grid(True)
        ax.set_xlim(1000,100000000)
        ax.set_ylim(1000.0,1000.0*1000.0)
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.xaxis.set_label('t in iterations')
        ax.yaxis.set_label('msd(t) in nm^2')
        ax.legend()
        fig.savefig(name+"_no_mapping.png")
        fig.clf()
        plt.close("no_mapping")
        # temporal mapping ?
        if mapping:
            # read the time mapping file
            TM_seconds=[]
            TM_MSD=[]
            with open(name_mapping,"r") as in_file:
                lines=in_file.readlines()
                for l in lines[1:]:
                    sl=l.split()
                    TM_seconds.append(float(sl[0]))
                    TM_MSD.append(float(sl[1]))
            TM_seconds=np.array(TM_seconds)
            TM_MSD=np.array(TM_MSD)
            # does the msd overlaps with the experimental one ?
            mapping=False
            iterations_per_seconds=[]
            C=len(current_msd)//4
            for c in range(C):
                index=(np.abs(TM_MSD/current_msd[c]-1.0)).argmin()
                # is it 5% close ?
                if abs(TM_MSD[index]/current_msd[c]-1.0)<0.05:
                    iterations_per_seconds.append(dt[c]/TM_seconds[index])
                    mapping=True
            if mapping:
                factor=len(iterations_per_seconds)/sum(iterations_per_seconds)
                dt=[d*factor for d in dt]
        # plot with mapping
        if mapping:
            dt_2points=[min(dt),3.0*min(dt)]
            min_msd=10000.0
            fig=plt.figure("mapping")
            ax=plt.gca()
            ax.plot(dt,current_msd,label="msd")
            ax.plot(dt_2points,[min_msd*t/dt_2points[0] for t in dt_2points],label="t")
            ax.plot(dt_2points,[min_msd*math.pow(t/dt_2points[0],0.75) for t in dt_2points],label="t^3/4")
            ax.plot(dt_2points,[min_msd*math.pow(t/dt_2points[0],0.5) for t in dt_2points],label="t^1/2")
            ax.plot(dt_2points,[min_msd*math.pow(t/dt_2points[0],0.25) for t in dt_2points],label="t^1/4")
            ax.plot(TM_seconds,TM_MSD,'bo',label="experiment")
            ax.grid(True)
            ax.set_xlim(min(dt),max(dt))
            ax.set_ylim(1000.0,1000.0*1000.0)
            ax.set_xscale('log')
            ax.set_yscale('log')
            ax.xaxis.set_label('t in seconds')
            ax.yaxis.set_label('msd(t) in nm^2')
            ax.legend()
            fig.savefig(name+".png")
            fig.clf()
            plt.close("mapping")
    return msd

def draw_conformation(context: mm.Context,colors: list,name: str):
    """draw conformation using matplotlib.
    Parameters
    ----------
    context : OpenMM context
    colors  : a list of colors for each beads (list)
    name    : the name of the file (string)
    Returns
    -------
    Raises
    ------
    if positions and colors do not have the same length.
    """
    L=((context.getSystem().getDefaultPeriodicBoxVectors())[0])[0]
    positions=context.getState(getPositions=True,enforcePeriodicBox=False).getPositions()
    N=len(positions)
    if len(colors)!=N:
        print("positions and colors do not have the same length, do nothing.")
    else:
        px=[p[0]/L for p in positions]
        py=[p[1]/L for p in positions]
        pz=[p[2]/L for p in positions]
        plt.figure(1)
        ax=plt.axes(projection='3d')
        ax.set_xlim(0.0,1.0)
        ax.set_ylim(0.0,1.0)
        ax.set_zlim(0.0,1.0)
        # ax.plot3D(px,py,pz,'gray')
        ax.scatter3D(px,py,pz,c=colors,cmap='rainbow',alpha=0.5)
        plt.savefig(name)
        plt.clf()
        plt.close("all")

def draw_sub_conformation(context: mm.Context,colors: list,part: list,name: str):
    """draw a sub-conformation using matplotlib.
    Parameters
    ----------
    context : OpenMM context
    colors  : a list of colors for each beads (list)
    part    : (list)
    name    : the name of the file (string)
    Returns
    -------
    Raises
    ------
    if positions and colors do not have the same length.
    """
    L=((context.getSystem().getDefaultPeriodicBoxVectors())[0])[0]
    positions=context.getState(getPositions=True,enforcePeriodicBox=False).getPositions()
    N=len(positions)
    P=len(part)
    C=len(colors)
    if C!=P and P>0:
        print("positions and colors do not have the same length, do nothing.")
    else:
        px=[]
        py=[]
        pz=[]
        for n in range(N):
            if n in part:
                p1=positions[n]
                px.append(p1[0]/L)
                py.append(p1[1]/L)
                pz.append(p1[2]/L)
        plt.figure(1)
        ax=plt.axes(projection='3d')
        ax.set_xlim(0.0,1.0)
        ax.set_ylim(0.0,1.0)
        ax.set_zlim(0.0,1.0)
        # ax.plot3D(px,py,pz,'gray')
        # ax.scatter3D(px,py,pz,c=colors,cmap='rainbow')
        ax.scatter3D(px,py,pz,c=colors)
        plt.savefig(name)
        plt.clf()
        plt.close("all")

def get_distances(positions: list,locii: list,locii_name: list,distances: dict,iterations: int,plot: bool=False,save: bool=False,name_d: str="/scratch/distance",name_h: str="/scratch/histogram") -> dict:
    """return a dictionnary with the distances between all pairwize built from locii.
    Parameters
    ----------
    context    : OpenMM context
    locii      : a list of locii (list)
    locii_name : a list of the name of locii (list)
    distances  : a dictionnary with all the pairwize distances (dict)
                 the dictionnary keys are the name of the pairwize (locus1_locus2).
                 each key gives access to a list of (int,float)=(iterations,distance).
    iterations : current iteration (integer)
    plot       : does the function plot the timeline evolution of the distances ? (bool)
    save       : does the function save (json format) the dictionnary ? (bool)
    name_d     : name for the plot (distances) and/or the output json file (str)
    name_h     : name for the plot (histograms) (str)
    Returns
    -------
    a dictionnary with all the pairwize distances and the new ones (dict).
    Raises
    ------
    """
    L=min(len(locii),len(locii_name))
    positions2=[positions[l] for l in locii]
    d_min=0.0
    d_max=0.0
    for i in range(L-1):
        pi=positions2[i]
        locii_name_i=locii_name[i]
        distances_keys=distances.keys()
        for j in range(i+1,L):
            locii_name_j=locii_name[j]
            pairwize=locii_name_i+"_"+locii_name_j
            if pairwize not in distances_keys:
                distances[pairwize]=[]
            pj=positions2[j]
            dx=pi[0]-pj[0]
            dy=pi[1]-pj[1]
            dz=pi[2]-pj[2]
            dr_nm=math.sqrt(dx*dx+dy*dy+dz*dz)
            d_max=max(d_max,dr_nm)
            distances[pairwize].append([iterations,dr_nm])
    # plot the distances ?
    if plot:
        distances_keys=distances.keys()
        for pairwize in distances_keys:
            dp=distances[pairwize]
            # as a function of time
            dp0,dp1=zip(*[(p[0],p[1]) for p in dp])
            fig=plt.figure(pairwize+"1")
            ax=fig.add_axes([0.1,0.1,0.8,0.8],xlim=(0,iterations),ylim=(d_min,1.5*d_max),xlabel="iterations",ylabel="distance "+pairwize+" in nm")
            ax.plot(dp0,dp1)
            fig.savefig(name_d+"_"+pairwize+".png")
            fig.clf()
            plt.close(pairwize+"1")
            # as a distribution
            fig=plt.figure(pairwize+"2")
            ax=fig.add_axes([0.1,0.1,0.8,0.8],xlim=(d_min,d_max),ylim=(0.0001,1.0),xlabel="distance "+pairwize+" in nm",ylabel="P(distance)",yscale='log')
            ax.hist(dp1,50,density=True,facecolor='b',alpha=0.75)
            fig.savefig(name_h+"_"+pairwize+".png")
            fig.clf()
            plt.close(pairwize+"2")
    # save the distances to json file ?
    if save:
        with open(name_d+"_in_nm.json","w") as out_file:
            json.dump(distances,out_file,indent=0)
    return distances

def write_conformation(context: mm.Context,name: str):
    """write conformation to a file, x y z for the first bead, then x y z of the second bead and so on
    Parameters
    ----------
    context : OpenMM context
    name    : name of the file (string)
    Returns
    -------
    Raises
    ------
    """
    positions=context.getState(getPositions=True,enforcePeriodicBox=False).getPositions()
    with open(name,"w") as out_file:
        for p in positions:
            out_file.write(str(p[0]._value)+' '+str(p[1]._value)+' '+str(p[2]._value)+'\n')

def read_write_checkpoint(context: mm.Context,name: str,mode: str) -> int:
    """read/write from/a OpenMM checkpoint file.
    Parameters
    ----------
    context : OpenMM context
    name    : name of the checkpoint file (string)
    mode    : read or wirte (string)
    Returns:
    --------
    1 if the function find or write the file, 0 otherwize.
    Raises
    ------
    if no checkpoint file has been found in read mode.
    if mode is not equal to read or to write.
    """
    if mode=="read":
        try:
            with open(name,"rb") as in_file:
                context.loadCheckpoint(in_file.read())
                return 1
        except IOError:
            print("no checkpoint file found : "+name)
            return 0
    elif mode=="write":
        with open(name,"wb") as out_file:
            out_file.write(context.createCheckpoint())
        return 1
    else:
        print("Please consider using read or write option.")
        return 0

def create_integrator(thermostat: str,Temp: unit.Quantity,dt: unit.Quantity,dof: int,tau: unit.Quantity,seed: int=1,limit: bool=False,rescale: bool=False):
    """return a integrator with spatial step limit and/or rescaling of the velocities.
    the 'langevin' option for the input variable 'thermostat' does not work with input variables 'limit' and 'rescale'.
    Parameters
    ----------
    thermostat : Gronech-Jensen-Farago ("gjf"), "langevin" or "nve"
    Temp       : temperature (unit.Quantity)
    dt         : the time step in picosecond (unit.Quantity)
    dof        : degrees of freedom (int)
    tau        : inverse of the coupling frequency to the thermostat (unit.Quantity)
    seed       : seed for the integrator (int)
    limit      : limit for the ||x(t+dt)-x(t)|| (False or True) (bool)
    rescale    : scale the kinetic average to its thermal average value (False or True)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    if thermostat is not equal to "nve" or "gjf" or "langevin".
    """
    kBT=get_kBT(Temp)
    if thermostat=="langevin":
        integrator=mm.LangevinIntegrator(Temp,1.0/tau,dt)
        integrator.setRandomNumberSeed(seed)
        integrator.setConstraintTolerance(0.00001)
    elif thermostat=="gjf":
        integrator=gjf_integrator(dt,dof,Temp,tau,seed,limit,rescale)
        integrator.setConstraintTolerance(0.00001)
    elif thermostat=="nve":
        integrator=verlet_integrator(dt,dof,Temp,limit,rescale)
        integrator.setConstraintTolerance(0.00001)
    else:
        print("Please consider using 'nve', 'langevin' or 'gjf' for input 'thermostat' variable.")
        print("'thermostat' takes the value 'langevin' with 'Temp=300.0*unit.kelvin', 'tau=1.0/unit.picosecond' and 'dt=0.001*unit.picosecond'.")
        integrator=mm.LangevinIntegrator(300.0*unit.kelvin,1.0/unit.picosecond,0.001*unit.picosecond)
        integrator.setRandomNumberSeed(1)
        integrator.setConstraintTolerance(0.00001)
    return integrator

def gjf_integrator(dt: unit.Quantity,dof: int,Temp: unit.Quantity=300.0*unit.kelvin,tau: unit.Quantity=unit.picosecond,seed: int=1,limit: bool=False,rescale: bool=False) -> mm.CustomIntegrator:
    """return a Gronbech-Jensen-Farago (GJF) Langevin integrator with spatial step limit and rescaling of the velocities.
    Parameters
    ----------
    dt      : the time step (unit.Quantity)
    dof     : degrees of freedom (int)
    Temp    : temperature (unit.Quantity)
    tau     : inverse of the coupling frequency to the thermostat (unit.Quantity)
    seed    : seed for the integrator (int)
    limit   : limit for the displacement ||x(t+dt)-x(t)|| (False or True) (bool)
    rescale : rescaling of the kinetic energy (False or True) (bool)
    Returns
    -------
    OpenMM CustomIntegrator
    Raises
    ------
    References
    ----------
    Niels Gronbech-Jensen and Oded Farago.
    A simple and effective Verlet-type algorithm for simulating Langevin dynamics.
    https://doi.org/10.1080/00268976.2012.760055
    """
    kBT=get_kBT(Temp)
    integrator=mm.CustomIntegrator(dt)
    integrator.setRandomNumberSeed(seed)
    # global variables
    integrator.addGlobalVariable("rlimit",int(limit))
    integrator.addGlobalVariable("rescale",int(rescale))
    integrator.addGlobalVariable("tau",tau)
    integrator.addGlobalVariable("kBT",kBT)
    integrator.addGlobalVariable("mke",dof*0.5*kBT)
    integrator.addGlobalVariable("ke",0.0*kBT)
    # per-dof variables
    integrator.addPerDofVariable("f_n",0.0)
    integrator.addPerDofVariable("beta_n_1",0.0)
    integrator.addPerDofVariable("x1",0.0)
    integrator.addPerDofVariable("alpha_gjf",0.0)
    integrator.addPerDofVariable("a_gjf",0.0)
    integrator.addPerDofVariable("b_gjf",0.0)
    integrator.addPerDofVariable("x_sigma_gjf",0.0)
    integrator.addPerDofVariable("v_sigma_gjf",0.0)
    integrator.addPerDofVariable("dx_lim",0.0)
    integrator.addPerDofVariable("DX_lim",0.0)
    # integrator steps
    integrator.addUpdateContextState()
    integrator.addComputePerDof("beta_n_1","gaussian")
    integrator.addComputePerDof("f_n","f")
    # compute coefficients
    integrator.addComputePerDof("alpha_gjf","m/tau")
    integrator.addComputePerDof("a_gjf","(1-dt*alpha_gjf/(2*m))/(1+dt*alpha_gjf/(2*m))")
    integrator.addComputePerDof("b_gjf","1/(1+dt*alpha_gjf/(2*m))")
    integrator.addComputePerDof("x_sigma_gjf","sqrt(alpha_gjf*kBT*dt/(2*m*m))")
    integrator.addComputePerDof("v_sigma_gjf","sqrt(2*alpha_gjf*kBT*dt/(m*m))")
    # do we limit the displacement to the time-step multiply by the thermic velocity ?
    integrator.beginIfBlock("rlimit=1")
    integrator.addComputePerDof("dx_lim","-dt*sqrt(kBT/m)")
    integrator.addComputePerDof("DX_lim","dt*sqrt(kBT/m)")
    integrator.addComputePerDof("x","x+max(dx_lim,min(DX_lim,b_gjf*dt*(v+dt*f_n/(2*m)+x_sigma_gjf*beta_n_1)))")
    integrator.endBlock()
    integrator.beginIfBlock("rlimit=0")
    integrator.addComputePerDof("x","x+b_gjf*dt*(v+dt*f_n/(2*m)+x_sigma_gjf*beta_n_1)")
    integrator.endBlock()
    integrator.addComputePerDof("x1","x")
    integrator.addConstrainPositions()
    # velocities
    integrator.addComputePerDof("v","a_gjf*v+(dt/(2*m))*(a_gjf*f_n+f)+b_gjf*v_sigma_gjf*beta_n_1+(x-x1)/dt")
    integrator.addConstrainVelocities()
    # rescale the velocities ?
    integrator.beginIfBlock("rescale=1")
    integrator.addComputeSum("ke","0.5*m*v*v")
    integrator.addComputePerDof("v","v*sqrt(mke/ke)")
    integrator.endBlock()
    # for s in range(20):
    #     try:
    #         print(integrator.getComputationStep(s))
    #     except:
    #         print("no computation step "+str(s))
    return integrator

def verlet_integrator(dt: unit.Quantity,dof: int,Temp: unit.Quantity,limit: bool,rescale: bool) -> mm.CustomIntegrator:
    """create and return a Velocity-Verlet integrator with spatial step limit and rescaling of the velocities
    Parameters
    ----------
    dt      : time step in picosecond (unit.Quantity)
    dof     : degrees of freedom (int)
    Temp    : temperature (unit.Quantity)
    limit   : limit for the displacement ||x(t+dt)-x(t)|| (False or True)
    rescale : rescaling of the kinetic energy (False or True)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    """
    if limit or rescale:
        kBT=get_kBT(Temp)
        integrator=mm.CustomIntegrator(dt)
        # global variables
        integrator.addGlobalVariable("mke",dof*0.5*kBT)
        integrator.addGlobalVariable("ke",0.0*kBT)
        integrator.addGlobalVariable("rlimit",int(limit))
        integrator.addGlobalVariable("rescale",int(rescale))
        # per-dof variables
        integrator.addPerDofVariable("dx_lim",0.0)
        integrator.addPerDofVariable("DX_lim",0.0)
        integrator.addPerDofVariable("x1",0.0)
        # computation steps
        integrator.addUpdateContextState()
        integrator.addComputePerDof("v","v+0.5*dt*f/m")
        # do we limit the displacement ?
        integrator.beginIfBlock("rlimit=1")
        integrator.addComputePerDof("dx_lim","-dt*sqrt(kBT/m))")
        integrator.addComputePerDof("DX_lim","dt*sqrt(kBT/m))")
        integrator.addComputePerDof("x","x+max(-dx_lim,min(DX_lim,dt*v))")
        integrator.endBlock()
        integrator.beginIfBlock("rlimit=0")
        integrator.addComputePerDof("x","x+dt*v")
        integrator.endBlock()
        integrator.addComputePerDof("x1","x")
        integrator.addConstrainPositions()
        # velocities
        integrator.addComputePerDof("v","v+0.5*dt*f/m+(x-x1)/dt")
        integrator.addConstrainVelocities()
        integrator.beginIfBlock("rescale=1")
        integrator.addComputeSum("ke","0.5*m*v*v")
        integrator.addComputePerDof("v","v*(step(0.01-abs(1.0-mke/ke))+(1-step(0.01-abs(1.0-mke/ke)))*sqrt(mke/ke))")
        integrator.endBlock()
    else:
        integrator=mm.VerletIntegrator(dt)
    return integrator

def global_integrator(dt: unit.Quantity,tau: unit.Quantity,N: int,C: int,Temp: unit.Quantity,m: unit.Quantity,limit: bool,rescale: bool) -> mm.CustomIntegrator:
    """create and return a global thermostat integrator with spatial step limit and rescaling of the velocities
    Parameters
    ----------
    dt      : time step in picosecond (unit.Quantity)
    tau     : inverse of the coupling frequency to the thermostat (unit.Quantity)
    N       : number of particles (integer)
    C       : number of constraints (integer)
    Temp    : temperature of the system (unit.Quantity)
    m       : mass of the beads (unit.Quantity)
    limit   : limit for the displacement ||x(t+dt)-x(t)|| (False or True)
    rescale : rescaling of the kinetic energy (False or True)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    References
    ----------
    Giovanni Bussi and Michele Parrinello.
    Stochastics thermostats : comparison of local and global schemes.
    http://dx.doi.org/10.1016/j.cpc.2008.01.006
    """
    kBT=get_kBT(Temp)
    # 3 particles per bead : bead (mass), virtual (mass=0) and pseudo-u bead (mass)
    dof=3*N*(get_particles_per_bead()-1)-C
    integrator=mm.CustomIntegrator(dt)
    integrator.addGlobalVariable("rlimit",int(limit))
    integrator.addGlobalVariable("rescale",int(rescale))
    integrator.addGlobalVariable("c_global",math.exp(-dt/tau))
    integrator.addGlobalVariable("dof_global",dof)
    integrator.addGlobalVariable("Temp",Temp)
    integrator.addGlobalVariable("kBT",kBT)
    integrator.addGlobalVariable("mke",dof*0.5*kBT)
    integrator.addGlobalVariable("ke",0.0*kBT)
    integrator.addGlobalVariable("alpha",0.0)
    integrator.addGlobalVariable("sign_alpha",0)
    integrator.addGlobalVariable("R_global",0.0)
    integrator.addGlobalVariable("S_global",0.0)
    integrator.addGlobalVariable("dx_lim_global",0.0)
    integrator.addGlobalVariable("DX_lim_global",0.0)
    integrator.addPerDofVariable("x1",0.0)
    # global thermostat coefficients
    # "S_global" is approximate because we suppose "dof_global" -> inf
    integrator.addComputeSum("ke","0.5*m*v*v")
    integrator.addComputeGlobal("R_global","gaussian")
    integrator.addComputeGlobal("S_global","sqrt(2*(dof_global-1))*gaussian+(dof_global-1)")
    integrator.addComputeGlobal("alpha","sqrt(c_global+((1-c_global)*(S_global+R_global*R_global)*mke)/(dof_global*ke)+2*R_global*sqrt(c_global*(1-c_global)*mke/(dof_global*ke)))")
    integrator.addComputeGlobal("sign_alpha","2*step(R_global+sqrt(c_global*dof_global*ke/((1-c_global)*mke)))-1")
    # momenta
    integrator.addComputePerDof("v","sign_alpha*alpha*v")
    # velocity-Verlet
    integrator.addUpdateContextState()
    integrator.addComputePerDof("v","v+0.5*dt*f/m")
    # limit the displacement ?
    integrator.beginIfBlock("rlimit=1")
    integrator.addComputePerDof("dx_lim_global","-dt*sqrt(kBT/m)")
    integrator.addComputePerDof("DX_lim_global","dt*sqrt(kBT/m)")
    integrator.addComputePerDof("x","x+max(dx_lim_global,min(DX_lim_global,dt*v))")
    integrator.endBlock()
    integrator.beginIfBlock("rlimit=0")
    integrator.addComputePerDof("x","x+dt*v")
    integrator.endBlock()
    # constraints
    integrator.addComputePerDof("x1","x")
    integrator.addConstrainPositions()
    integrator.addComputePerDof("v","v+0.5*dt*f/m+(x-x1)/dt")
    integrator.addConstrainVelocities()
    # rescale ?
    integrator.beginIfBlock("rescale=1")
    integrator.addComputeSum("ke","0.5*m*v*v")
    integrator.addComputePerDof("v","v*sqrt(mke/ke)")
    integrator.endBlock()    
    return integrator

def correction_flying_ice_cube(context: mm.Context) -> list:
    """the com motion is move to 0,0,0 as-well-as the inertia of the system.
    Parameters
    ----------
    context : OpenMM context (mm.Context)
    Returns
    -------
    the velocities that have been corrected (list of mm.Vec3).
    Raises
    ------
    References
    ----------
    Stephen C. Harvey, Robert K.-Z. Tan, Thomas E. Cheatham III.
    The flying ice cube : Velocity rescaling in molecular dynamics leads to violation of energy equipartition.
    https://doi.org/10.1002/(SICI)1096-987X(199805)19:7%3C726::AID-JCC4%3E3.0.CO;2-S
    The global thermostat can lead to the "flying-ice-cube" phenomena.
    We have to cancel the first (com motion) and second (inertia) momenta to minimize the effect.
    """
    velocities=context.getState(getVelocities=True).getVelocities()
    positions=context.getState(getPositions=True,enforcePeriodicBox=False).getPositions()
    N=len(positions)
    # com and com motion
    xcom,ycom,zcom=0.0*unit.nanometer,0.0*unit.nanometer,0.0*unit.nanometer
    vxcom,vycom,vzcom=0.0*unit.nanometer/unit.picosecond,0.0*unit.nanometer/unit.picosecond,0.0*unit.nanometer/unit.picosecond
    for p,v in zip(positions,velocities):
        xcom+=p[0]
        ycom+=p[1]
        zcom+=p[2]
        vxcom+=v[0]
        vycom+=v[1]
        vzcom+=v[2]
    xcom/=N
    ycom/=N
    zcom/=N
    vxcom/=N
    vycom/=N
    vzcom/=N
    # linear velocity com to 0,0,0
    new_velocities=[mm.Vec3(v[0]-vxcom,v[1]-vycom,v[2]-vzcom) for v in velocities]
    # inertia
    I3=[0.0*unit.nanometer*unit.nanometer]*9
    for p in positions:
        I3[0]+=(p[1]-ycom)*(p[1]-ycom)+(p[2]-zcom)*(p[2]-zcom)
        I3[1]-=(p[1]-ycom)*(p[0]-xcom)
        I3[2]-=(p[2]-zcom)*(p[0]-xcom)
        I3[4]+=(p[0]-xcom)*(p[0]-xcom)+(p[2]-zcom)*(p[2]-zcom)
        I3[5]-=(p[2]-zcom)*(p[0]-ycom)
        I3[8]+=(p[0]-xcom)*(p[0]-xcom)+(p[1]-ycom)*(p[1]-ycom)
    I3[3]=I3[1]
    I3[6]=I3[2]
    I3[7]=I3[5]
    # inertia inverse
    inv_I3=[0.0/(unit.nanometer*unit.nanometer)]*9
    inv_I3[0]=I3[5]*I3[5]-I3[4]*I3[8]
    inv_I3[1]=I3[1]*I3[8]-I3[2]*I3[5]
    inv_I3[2]=I3[2]*I3[4]-I3[1]*I3[5]
    inv_I3[3]=inv_I3[1]
    inv_I3[4]=I3[2]*I3[2]-I3[0]*I3[8]
    inv_I3[5]=I3[0]*I3[5]-I3[1]*I3[2]
    inv_I3[6]=inv_I3[2]
    inv_I3[7]=inv_I3[5]
    inv_I3[8]=I3[1]*I3[1]-I3[0]*I3[4]
    d0=I3[0]*I3[5]*I3[5]+I3[1]*I3[1]*I3[8]-2.0*I3[1]*I3[2]*I3[5]+I3[2]*I3[2]*I3[4]-I3[0]*I3[4]*I3[8]
    if d0!=0.0:
        # angular momentum
        dx=0.0*unit.nanometer*unit.nanometer/unit.picosecond
        dy=0.0*unit.nanometer*unit.nanometer/unit.picosecond
        dz=0.0*unit.nanometer*unit.nanometer/unit.picosecond
        for p,v in zip(positions,new_velocities):
            dx+=(p[1]-ycom)*v[2]-(p[2]-zcom)*v[1]
            dy+=(p[2]-zcom)*v[0]-(p[0]-xcom)*v[2]
            dz+=(p[0]-xcom)*v[1]-(p[1]-ycom)*v[0]
        # w=I^-1l
        wx=(inv_I3[0]*dx+inv_I3[1]*dy+inv_I3[2]*dz)/d0
        wy=(inv_I3[3]*dx+inv_I3[4]*dy+inv_I3[5]*dz)/d0
        wz=(inv_I3[6]*dx+inv_I3[7]*dy+inv_I3[8]*dz)/d0
        return [mm.Vec3(v[0]-(wy*(p[2]-zcom)-wz*(p[1]-ycom)),v[1]-(wz*(p[0]-xcom)-wx*(p[2]-zcom)),v[2]-(wx*(p[1]-ycom)-wy*(p[0]-xcom))) for p,v in zip(positions,new_velocities)]
    else:
        return new_velocities

def get_positions_from_context(context: mm.Context,start: int,end: int,PBC: bool=False) -> list:
    """return the positions (in nanometer) from the OpenMM context.
    Parameters
    ----------
    context : OpenMM context
    start   : keep particles from 'start' to 'end' [start,end[ (int)
    end     : keep particles from 'start' to 'end' [start,end[ (int)
    PBC     : enforce periodic box ? (bool)
    """
    pbuffer=context.getState(getPositions=True,enforcePeriodicBox=PBC).getPositions()
    pbuffer=[pbuffer[i] for i in range(start,end)]
    positions=[(p[0]/unit.nanometer,p[1]/unit.nanometer,p[2]/unit.nanometer) for p in pbuffer]
    return positions

def get_platforms(name: str =""):
    """return the platform index and device index corresponding to the name of the
    hardware you would like to use. Write it to a json file "get_platforms.json".
    The name of the hardware is like "GeForce_GTX_980_Ti_OpenCL".
    Do not forget to add "_OpenCL" or "_CUDA" to ask for the OpenCL implementation whatever it is CPU or GPU.
    Parameters
    ----------
    name : name of the hardware, default to "" (string)
    Returns
    -------
    index of the platform, index of the device (integer,integer).
    Raises
    ------
    exit if no platform and/or device (with name!="") have been found.
    """
    platform_and_device={}
    integrator_test=[]
    system_test=[]
    platform_test=[]
    context_test=[]
    itest=0
    stest=0
    ptest=0
    ctest=0
    index_of_platform=-1
    index_of_device=-1
    for c in ["CPU","CUDA","OpenCL"]:
        platform_and_device[c]={}
        for p in range(10):# loop over platforms
            for d in range(10):# loop over device
                try:
                    integrator_test.append(mm.VerletIntegrator(0.001))
                    itest+=1
                    system_test.append(mm.System())
                    system_test[stest].addParticle(1.0)
                    stest+=1
                    platform_test.append(mm.Platform.getPlatformByName(c))
                    ptest+=1
                    if c=="CPU":
                        context_test.append(mm.Context(system_test[stest-1],integrator_test[itest-1],platform_test[ptest-1],{"CpuThreads":"1"}))
                    elif c=="CUDA":
                        context_test.append(mm.Context(system_test[stest-1],integrator_test[itest-1],platform_test[ptest-1],{"CudaPlatformIndex":str(p),"CudaDeviceIndex":str(d)}))
                    elif c=="OpenCL":
                        context_test.append(mm.Context(system_test[stest-1],integrator_test[itest-1],platform_test[ptest-1],{"OpenCLPlatformIndex":str(p),"OpenCLDeviceIndex":str(d)}))
                    ctest+=1
                    current_DeviceName=(context_test[ctest-1].getPlatform().getPropertyValue(context_test[ctest-1],'DeviceName')).replace(" ","_")
                    platform_and_device[c][current_DeviceName]={}
                    for n in context_test[ctest-1].getPlatform().getPropertyNames():
                        platform_and_device[c][current_DeviceName][n]=context_test[ctest-1].getPlatform().getPropertyValue(context_test[ctest-1],n)
                    if (current_DeviceName+"_"+c)==name:# if the current device is what we ask for ...
                        index_of_platform=p
                        index_of_device=d
                except:
                    error=True
    with open("get_platforms.json","w") as out_file:
        json.dump(platform_and_device,out_file,indent=0)
    if (index_of_platform==-1 or index_of_device==-1) and name!="":
        print("No platform has been found : "+name)
        sys.exit()
    return index_of_platform,index_of_device

def create_platform(platform_name: str="CPU1",precision: str="double"):
    """create and return platform and properties based on :
    Parameters
    ----------
    platform_name : "CPU2", "GeForce_GTX_780_OpenCL" ... (str)
                    if 'CPU' is in input argument platform_name, the number of threads is the integer after 'CPU'.
    precision     : single, mixed or double (str)
    Returns
    -------
    OpenMM platform, OpenMM properties
    Raises
    ------
    """
    if "CPU" in platform_name and not "OpenCL" in platform_name and not "CUDA" in platform_name:
        platform=mm.Platform.getPlatformByName("CPU")
        properties={"CpuThreads":platform_name.replace('CPU','')}
    elif "CUDA" in platform_name:
        platform=mm.Platform.getPlatformByName("CUDA")
        platform_index,device_index=get_platforms(platform_name)
        if precision=="double":
            if platform.supportsDoublePrecision():
                properties={"CudaPlatformIndex":str(platform_index),"CudaDeviceIndex":str(device_index),"CudaPrecision":"double"}
            else:
                properties={"CudaPlatformIndex":str(platform_index),"CudaDeviceIndex":str(device_index),"CudaPrecision":"single"}
        else:
            properties={"CudaPlatformIndex":str(platform_index),"CudaDeviceIndex":str(device_index),"CudaPrecision":precision}
    elif "OpenCL" in platform_name:
        platform=mm.Platform.getPlatformByName("OpenCL")
        platform_index,device_index=get_platforms(platform_name)
        if precision=="double":
            if platform.supportsDoublePrecision():
                properties={"OpenCLPlatformIndex":str(platform_index),"OpenCLDeviceIndex":str(device_index),"OpenCLPrecision":"double"}
            else:
                properties={"OpenCLPlatformIndex":str(platform_index),"OpenCLDeviceIndex":str(device_index),"OpenCLPrecision":"single"}
        else:
            properties={"OpenCLPlatformIndex":str(platform_index),"OpenCLDeviceIndex":str(device_index),"OpenCLPrecision":precision}
    return platform,properties

def create_system(L: unit.Quantity,mass: list,F: int) -> mm.System:
    """create and return a system.
    Parameters
    ----------
    L    : box size in nanometer (unit.Quantity)
    mass : list of the mass of each particles (list of unit.Quantity)
    F    : remove the com motion every F steps (integer)
    Returns
    -------
    OpenMM system
    Raises
    ------
    """
    system=mm.System()
    system.setDefaultPeriodicBoxVectors(mm.Vec3(L,0.0,0.0),mm.Vec3(0.0,L,0.0),mm.Vec3(0.0,0.0,L))
    # particles from the chains+binders
    for m in mass:
        system.addParticle(m)
    # remove the COM motion every F steps
    system.addForce(mm.CMMotionRemover(F))
    return system

def create_context(system: mm.System,integrator,platform: mm.Platform,properties,positions: list,velocities: list) -> mm.Context:
    """create and return an OpenMM context.
    Parameters
    ----------
    system     : OpenMM system
    integrator : OpenMM integrator
    platform   : OpenMM platform
    properties : OpenMM properties
    positions  : positions of the beads (list of OpenMM.Vec3)
    velocities : velocities of the beads (list of OpenMM.Vec3)
    Returns
    -------
    OpenMM context
    """
    context=mm.Context(system,integrator,platform,properties)
    context.setPositions(positions)
    context.setVelocities(velocities)
    return context

def write_entanglement_info(bp_per_nm3: float,N: int,resolution: int,k_bp: int,k_nm: float,name: str="dimensions.out"):
    """write info about entanglement length in the system you created.
    Parameters
    ----------
    bp_per_nm3 : number of bp per unit of volume (float)
    N          : number of beads (integer)
    resolution : number of bp per beads (integer)
    k_bp       : Kuhn length in bp (integer)
    k_nm       : Kuhn length in nm (float)
    name       : file name to write the dimensions (string)
    Returns
    -------
    Raises
    ------
    """
    B=math.pow(N*resolution/bp_per_nm3,1.0/3.0)# box size in nm
    L=N*resolution*k_nm/k_bp
    rho_k=(N*resolution/k_bp)/(B*B*B)# number of Kuhn segments per unit of volume
    e_k=rho_k*k_nm*k_nm*k_nm
    Le_nm=k_nm*(math.pow(0.06*e_k,-0.4)+math.pow(0.06*e_k,-2.0))# nm
    Le_per_L=Le_nm/L
    Rg2_Le_nm2=Le_nm*k_nm/6.0# gyration radius at the scale of the entanglement length
    # writing the dimensions
    with open(name,"w") as out_file:
        out_file.write('kuhn '+str(k_bp)+' bp'+'\n')
        out_file.write('kuhn '+str(k_nm)+' nm'+'\n')
        out_file.write('bond '+str(k_nm*resolution/k_bp)+' nm'+'\n')
        out_file.write('resolution '+str(resolution)+' bp'+'\n')
        out_file.write('ek '+str(e_k)+' dimensionless'+'\n')
        out_file.write('L '+str(L)+' nm'+'\n')
        out_file.write('Le '+str(Le_nm)+' nm'+'\n')
        out_file.write('Le/L '+str(Le_nm/L)+' dimensionless'+'\n')
        out_file.write('Rg2(Le) '+str(Rg2_Le_nm2)+' nm^2'+'\n')
        out_file.write('B '+str(B)+' nm'+'\n')
        out_file.write('bp_per_nm3 '+str(bp_per_nm3)+' bp_per_nm3'+'\n')
        out_file.write('v/V '+str(N*4.0*math.pi*math.pow(0.5*k_nm*resolution/k_bp,3.0)/(B*B*B))+' none'+'\n')

def get_info_system(context: mm.Context):
    """print the info about the system you created.
    Parameters
    ----------
    context : OpenMM context
    Returns
    -------
    """
    print("system uses PBC : "+str(context.getSystem().usesPeriodicBoundaryConditions()))
    print("default periodic box : "+str(context.getSystem().getDefaultPeriodicBoxVectors()))
    for f in range(context.getSystem().getNumForces()):
        print("force "+str(f))
        print('PBC '+str(context.getSystem().getForce(f).usesPeriodicBoundaryConditions()))
        try:
            context.getSystem().getForce(f).updateParametersInContext(context)
        except:
            print("no updateParametersInContext() for the current force")
        try:
            print('non bonded method '+str(context.getSystem().getForce(f).getNonbondedMethod())+'/'+str(mm.NonbondedForce.CutoffPeriodic))
            print("cutoff distance : "+str(context.getSystem().getForce(f).getCutoffDistance()))
            print("use long range correction : "+str(context.getSystem().getForce(f).getUseLongRangeCorrection()))
            print("use switching function : "+str(context.getSystem().getForce(f).getUseSwitchingFunction()))
            print(str(context.getSystem().getForce(f).getNumPerParticleParameters())+" per particle parameters",str(context.getSystem().getForce(f).getNumGlobalParameters())+" global parameters")
        except:
            error=True
    print(str(context.getSystem().getNumConstraints())+" constraints")
    for n in context.getPlatform().getPropertyNames():
        print(n+" : "+str(context.getPlatform().getPropertyValue(context,n)))

@numba.jit(nopython=True)
def Rg2_and_shape(positions: list,start: int,end: int,linear: bool) -> tuple:
    """calculates the tensor and radius of gyration for the part within [start,end].
    Parameters
    ----------
    positions   : list of positions returns by 'get_positions_from_context'
    start       : start of the part (int)
    end         : end of the part (int)
    linear      : (True) linear polymer or (False) ring polymer (bool)
    Returns
    -------
    A tuple made of the radius of gyration (float), the asphericity (float), the acylindricity (float), the shape factor (float), the 1d size of each part.
    Raises
    ------
    if start is equal to end, return a ZeroDivisionError.
    """
    N=len(positions)
    # if the polymer is linear the start < end for each parts we ask for
    if linear and start>end:
        start,end=end,start
    # com
    pi=positions[end]
    cx=pi[0]
    cy=pi[1]
    cz=pi[2]
    ii=start
    P=1
    while ii!=end:
        pi=positions[ii]
        cx+=pi[0]
        cy+=pi[1]
        cz+=pi[2]
        ii=from_i_to_bead(ii+1,N)
        P+=1
    cx/=P
    cy/=P
    cz/=P
    # gyration tensor
    Tij=[0.0]*9
    Rg2=0.0
    ii=start
    for p in range(P):
        pi=positions[ii]
        ax=pi[0]-cx
        ay=pi[1]-cy
        az=pi[2]-cz
        Tij[0]+=ax*ax
        Tij[1]+=ax*ay
        Tij[2]+=ax*az
        Tij[4]+=ay*ay
        Tij[5]+=ay*az
        Tij[8]+=az*az
        Rg2+=ax*ax+ay*ay+az*az
        ii=from_i_to_bead(ii+1,N)
    Tij[3]=Tij[1]
    Tij[6]=Tij[2]
    Tij[7]=Tij[5]
    Rg2/=P
    # eigenvalues of the gyration tensor
    np_Tij=np.array([[Tij[0]/P,Tij[1]/P,Tij[2]/P],[Tij[3]/P,Tij[4]/P,Tij[5]/P],[Tij[6]/P,Tij[7]/P,Tij[8]/P]])
    # print(np.linalg.eigvals(np_Tij))
    li=np.linalg.eigvals(np_Tij)
    li.sort()
    # li=np.sort(np.linalg.eigvals(np_Tij),axis=-1,kind='mergesort',order=None)
    # shape
    asphericity=li[2]-0.5*(li[0]+li[1])
    acylindricity=li[1]-li[0]
    kappa2=(asphericity*asphericity+0.75*acylindricity*acylindricity)/((li[0]+li[1]+li[2])*(li[0]+li[1]+li[2]))
    return [Rg2,asphericity,acylindricity,kappa2,P]

def average_bond_size(positions: list,N_per_C: list) -> unit.Quantity:
    """return the average bond size.
    Parameters
    ----------
    positions : positions of the N beads return by context.getState(getPositions=True).getPositions()
    N_per_C   : number of beads per chain (list of int)
    Returns
    -------
    square root of the average quadratic bond size (unit.Quantity)
    Raises
    ------
    unit.sqrt raises an error for average_bond_size<0
    """
    average_bond_size=0.0*positions[0][0]*positions[0][0]
    C=len(N_per_C)
    N=sum(N_per_C)
    index=0
    for c in range(C):
        for n in range(N_per_C[c]-1):
            pi=positions[index+n]
            Pi=positions[index+from_i_to_bead(n+1,N_per_C[c])]
            dx=pi[0]-Pi[0]
            dy=pi[1]-Pi[1]
            dz=pi[2]-Pi[2]
            average_bond_size+=dx*dx+dy*dy+dz*dz
        index+=N_per_C[c]
    return unit.sqrt(average_bond_size/float(N-C))

def average_pairwize_distance(positions: list) -> unit.Quantity:
    """return the average pairwize distance.
    Parameters
    ----------
    positions : positions of the N beads return by context.getState(getPositions=True).getPositions()
    Returns
    -------
    square root of the average quadratic pairwize distance (unit.Quantity)
    Raises
    ------
    unit.sqrt raises an error for average_bond_size<0
    """
    average_distance=0.0*positions[0][0]*positions[0][0]
    min_distance=positions[0][0]
    N=len(positions)
    for i in range(N-1):
        pi=positions[i]
        for j in range(i+1,N):
            pj=positions[j]
            dx=pi[0]-pj[0]
            dy=pi[1]-pj[1]
            dz=pi[2]-pj[2]
            average_distance+=dx*dx+dy*dy+dz*dz
            if unit.sqrt(dx*dx+dy*dy+dz*dz).__lt__(min_distance):
                min_distance=unit.sqrt(dx*dx+dy*dy+dz*dz)
    return unit.sqrt(average_distance/N),min_distance

def create_initial_conformation(context: mm.Context,N_per_C: list,thermostat: str,sigma: unit.Quantity,Temp: unit.Quantity,NB: int=1000,nb: int=10001,name: str="") -> int:
    """creates a initial conformation (from a straight line made of small bonds) with the correct bond size 'sigma'.
    Parameters
    ----------
    context     : OpenMM context
    N_per_C     : number of beads for each chain (list of int)
    thermostat  : gjf, nve or langevin (str)
    sigma       : the bond size you ask for (unit.Quantity)
    Temp        : temperature (unit.Quantity)
    NB          : number of iterations between two change of size (int)
    nb          : number of iterations to go from sigma(t=0) to sigma (int)
    name        : name of the file 'name.png' where to save intermediate conformations (str)
    Returns
    -------
    0 if the initial conformation procedure failed, 1 otherwize.
    Raises
    ------
    """
    kBT=get_kBT(Temp)
    a_cutoff=math.pow(2.0,1.0/6.0)
    # copy of the parameters
    if thermostat=="gjf":
        copy_of_tau=context.getIntegrator().getGlobalVariableByName("tau")
    elif thermostat=="langevin":
        copy_of_tau=context.getIntegrator().getFriction()
    else:
        pass
    copy_of_dt=context.getIntegrator().getStepSize()
    # initial bond size
    positions=context.getState(getPositions=True,enforcePeriodicBox=False).getPositions()
    index=0
    sigma0=sigma
    for n in N_per_C:
        for i in range(n-1):
            pi=positions[index+i]
            Pi=positions[index+from_i_to_bead(i+1,n)]
            dx=pi[0]-Pi[0]
            dy=pi[1]-Pi[1]
            dz=pi[2]-Pi[2]
            dd=unit.sqrt(dx*dx+dy*dy+dz*dz)
            if dd.__lt__(sigma0):
                sigma0=dd
        index+=n
    dsigma=(sigma-sigma0)/float(nb)
    cutoff_t=2.0*a_cutoff*(dsigma+sigma0)
    print("Start from bond length of "+str(sigma0)+" with target "+str(sigma)+" and increment of "+str(dsigma))
    print("The function restrains the displacement to the time-step times the thermal velocity.")
    # color of each bead along the chain
    colors=[]
    for n in N_per_C:
        colors=colors+[i/n for i in range(n)]
    # color of each binder
    colors=colors+[0 for i in range(context.getSystem().getNumParticles()-sum(N_per_C))]
    # change the cutoff distance
    F=context.getSystem().getNumForces()
    for f in range(F):
        try:
            if context.getSystem().getForce(f).getNonbondedMethod()==mm.NonbondedForce.CutoffPeriodic:
                context.getSystem().getForce(f).setCutoffDistance(cutoff_t)
                context.reinitialize(preserveState=True)
        except:
            pass
    sigmat=sigma0
    # steps to change the bond size
    draw_conformation(context,colors,name+"_0.png")
    pre_iterations=0
    for s in range(nb):
        # change of the WCA potential (fene)
        context.setParameter('sigma_fene',sigmat)
        context.setParameter('cut_fene',a_cutoff*sigmat)
        # change of the WCA potential
        context.setParameter('f_wca',(sigma0+(s+1)*dsigma)/sigma)
        # changes in the thermostat
        if s==0:
            # tau=((sigma0+(s+1)*dsigma)/sigma)*copy_of_tau*unit.picosecond# getGlobalVariableByName returns in unit.picosecond, why ?
            tau=(sigma0/sigma)*copy_of_tau*unit.picosecond# getGlobalVariableByName returns in unit.picosecond, why ?
            context.getIntegrator().setStepSize(0.001*tau)
            if thermostat=="gjf":
                context.getIntegrator().setGlobalVariableByName("tau",tau)
            elif thermostat=="langevin":
                context.getIntegrator().setFriction(tau)
            elif thermostat=="nve":
                pass
            else:
                pass
        # we run the model for NB iterations before to increase the size of the WCA
        context.getIntegrator().step(NB)
        pre_iterations+=NB
        if pre_iterations%200000==0:
            draw_conformation(context,colors,name+"_"+str(pre_iterations)+".png")
        # we increase the bond size
        sigmat+=dsigma
        # change the NonbondedMethod cutoff distance
        if (a_cutoff*sigmat)>cutoff_t or s==0:
            for f in range(F):
                try:
                    # if context.getSystem().getForce(f).getNonbondedMethod()==mm.NonbondedForce.CutoffPeriodic:
                    if context.getSystem().getForce(f).getForceGroup()==0:
                        cutoff_t=a_cutoff*min(sigma/unit.nanometer,(1000.0*dsigma+sigmat)/unit.nanometer)*unit.nanometer
                        print("----- "+str(pre_iterations),F,context.getSystem().getForce(f).getCutoffDistance(),context.getSystem().getForce(f).getNumExclusions())
                        print("<sigma>="+str(average_bond_size(context.getState(getPositions=True).getPositions(),N_per_C)))
                        print("f_wca="+str(context.getParameter('f_wca')))
                        print("derivative epsilon_wca="+str((context.getState(getParameterDerivatives=True).getEnergyParameterDerivatives())["epsilon_wca"]))
                        print("derivative f_wca="+str((context.getState(getParameterDerivatives=True).getEnergyParameterDerivatives())["f_wca"]))
                        print("change cutoff(t) to "+str(cutoff_t)+"/"+str(context.getSystem().getForce(f).getCutoffDistance())+" sigma(t)="+str(sigmat))
                        print("sigma increment="+str(dsigma/unit.nanometer)+" nm")
                        print("K/<K>="+str(context.getState(getEnergy=True).getKineticEnergy()/((3*context.getSystem().getNumParticles()-context.getSystem().getNumConstraints())*0.5*kBT)))
                        context.getSystem().getForce(f).setCutoffDistance(cutoff_t)
                        context.reinitialize(preserveState=True)
                except:
                    pass
    # run for 200*NB with the correct bond size
    context.getIntegrator().step(200*NB)
    # success condition (do not exceed the 1.5*size of the FENE bond)
    pafter=context.getState(getPositions=True,enforcePeriodicBox=False).getPositions()
    index=0
    min_bond_size=10000.0*unit.nanometer
    max_bond_size=0.0*unit.nanometer
    for n in N_per_C:
        pi=pafter[index]
        for i in range(1,n):
            Pi=pafter[index+from_i_to_bead(i-1,n)]
            dx=pi[0]-Pi[0]
            dy=pi[1]-Pi[1]
            dz=pi[2]-Pi[2]
            dd=unit.sqrt(dx*dx+dy*dy+dz*dz)
            if dd.__lt__(min_bond_size):
                min_bond_size=dd
            if dd.__gt__(max_bond_size):
                max_bond_size=dd
            pi=Pi
        index+=n
    if max_bond_size.__gt__(1.5*sigma):
        print("The maximum bond size "+str(max_bond_size)+" exceeds "+str(1.5*sigma)+", exit.")
        print("Try to change the input parameters 'nb' and 'NB'.")
        return 0
    # change of the WCA potential (fene)
    context.setParameter('sigma_fene',sigma)
    context.setParameter('cut_fene',a_cutoff*sigma)
    # change of the WCA potential
    context.setParameter('f_wca',1.0)
    # changes in the thermostat
    tau=copy_of_tau*unit.picosecond# getGlobalVariableByName returns in unit.picosecond, why ?
    context.getIntegrator().setStepSize(copy_of_dt)
    if thermostat=="gjf":
        print("The initial conformation with the target bond size suceeds.")
        print("Therefore, the limit on displacement and/or the rescaling of velocities are no more needed.")
        context.getIntegrator().setGlobalVariableByName("rlimit",0)
        context.getIntegrator().setGlobalVariableByName("rescale",0)
        context.getIntegrator().setGlobalVariableByName("tau",tau)
    elif thermostat=="langevin":
        context.getIntegrator().setFriction(tau)
    elif thermostat=="nve":
        print("The initial conformation with the target bond size suceeds.")
        print("Therefore, the limit on displacement is no more needed.")
        context.getIntegrator().setGlobalVariableByName("rlimit",0)
    else:
        pass
    return 1

def confinement(N: int,R: unit.Quantity,g: unit.Quantity) -> mm.CustomCompoundBondForce:
    """return a spherical confinement force.
    Parameters
    ----------
    N : number of beads (int)
    R : radius of the spherical confinement (unit.Quantity)
    g : strength of the spherical confinement (unit.Quantity)
    Returns
    -------
    OpenMM CustomCompoundBondForce to deal with spherical confinement.
    Raises
    ------
    """
    expression="step(r-R_confinement)*g_confinement*(exp(r/R_confinement-1)-1);r=sqrt((x1-R_confinement)*(x1-R_confinement)+(y1-R_confinement)*(y1-R_confinement)+(z1-R_confinement)*(z1-R_confinement))"
    confinement=mm.CustomCompoundBondForce(1,expression)
    confinement.addGlobalParameter("R_confinement",R)
    confinement.addGlobalParameter("g_confinement",g)
    for i in range(N):
        confinement.addBond([i])
    return confinement

def make_wt_Dernburg_Cell_1998_epigenetic_model(C: int,bp: int,g: float=0.0,Temp: unit.Quantity=300.0*unit.kelvin,path_to: str="/scratch") -> tuple:
    """create simple wild-type epigenetic model from Dernburg & al, Cell 1998.
    Parameters
    ----------
    C       : number of chains (integer)
    bp      : the number of base-pair per bead (integer)
    g       : the strength of interaction (in units of kBT) between state 'r' and state 's' (float)
    Temp    : temperature of the system (unit.Quantity)
    path_to : path to write the epigenetic model (str)
    Returns
    -------
    number of chain(s) (int), number of beads (int)
    Raises
    ------
    """
    kBT=get_kBT(Temp)
    heterochromatin=int(11000000/bp)
    euchromatin=int(21400000/bp)
    N=C*(heterochromatin+euchromatin)
    # wild-type
    with open(path_to+"/bead_state_wt_Dernburg_Cell_1998_"+str(C)+"chains_N"+str(N)+"_"+str(bp)+"bp.in","w") as out_file:
        out_file.write("chain monomer state\n")
        for c in range(C):
            for i in range(heterochromatin):
                out_file.write(str(c)+" "+str(i)+" "+"heterochromatin"+"\n")
            for i in range(euchromatin):
                out_file.write(str(c)+" "+str(heterochromatin+i)+" "+"euchromatin"+"\n")
    # interactions
    with open(path_to+"/HP1_HP1_"+str(g)+"kBT.in","w") as out_file:
        out_file.write("heterochromatin"+" "+"heterochromatin"+" "+str(g)+"\n")
    return C,N

def make_mutant_Dernburg_Cell_1998_epigenetic_model(C: int,bp: int,g: float=0.0,Temp: unit.Quantity=300.0*unit.kelvin,path_to: str="/scratch") -> tuple:
    """create simple mutant epigenetic model from Dernburg & al, Cell 1998.
    Parameters
    ----------
    C       : number of chains (int)
    bp      : the number of base-pair per bead (int)
    g       : the strength of interaction (in units of kBT) between state 'r' and state 's' (float)
    Temp    : temperature of the system (unit.Quantity)
    path_to : path to write the epigenetic model (str)
    Returns
    -------
    number of chain(s) (int), number of beads (int)
    Raises
    ------
    """
    kBT=get_kBT(Temp)
    insertion=int(2000000/bp)
    position=int(19415328/bp)# from dm6 to dm3
    heterochromatin=int(11000000/bp)
    euchromatin=int(21400000/bp)
    N=heterochromatin+position+insertion+euchromatin-position
    # mutant (insertion)
    with open(path_to+"/bead_state_mutant_Dernburg_Cell_1998_"+str(C)+"chains_N"+str(N)+"_"+str(bp)+"bp.in","w") as out_file:
        out_file.write("chain monomer state\n")
        for c in range(C):
            for i in range(heterochromatin):
                out_file.write(str(c)+" "+str(i)+" "+"heterochromatin"+"\n")
            for i in range(position):
                out_file.write(str(c)+" "+str(heterochromatin+i)+" "+"euchromatin"+"\n")
            for i in range(insertion):
                out_file.write(str(c)+" "+str(heterochromatin+position+i)+" "+"heterochromatin"+"\n")
            for i in range(euchromatin-position):
                out_file.write(str(c)+" "+str(heterochromatin+position+insertion+i)+" "+"euchromatin"+"\n")
    # interactions
    with open(path_to+"/HP1_HP1_"+str(g)+"kBT.in","w") as out_file:
        out_file.write("heterochromatin"+" "+"heterochromatin"+" "+str(g)+"\n")
    return C,C*N

def make_yeast_genome(bp: int=150,g: float=0.0,Temp: unit.Quantity=300.0*unit.kelvin,path_to: str="/scratch") -> tuple:
    """create the yeast genome.
    Parameters
    ----------
    bp      : the number of base-pair per bead (integer)
    g       : the strength of interaction (in units of kBT) between state 'r' and state 's' (float)
    Temp    : temperature of the system (unit.Quantity)
    path_to : path to write the epigenetic model (str)
    Returns
    -------
    number of chain(s) (int), number of beads (int)
    Raises
    ------
    """
    # 5000 * N + 150 * 1000 = G(chr 12) 1078177 bp
    # N*r^3=n*a^3 donc a=100*(150/n)^1/3 nm
    # 1/3=N^3/2*b^3/(6^3/2*1000^3)
    kBT=get_kBT(Temp)
    n_rDNA=150
    chains=[46*5000//bp,163*5000//bp,63*5000//bp,306*5000//bp,115*5000//bp,54*5000//bp,218*5000//bp,113*5000//bp,88*5000//bp,149*5000//bp,133*5000//bp,(186*5000+n_rDNA*1000)//bp,185*5000//bp,157*5000//bp,218*5000//bp,190*5000//bp]
    with open(path_to+"/bead_state_yeast_"+str(C)+"chains_N"+str(N)+"_"+str(bp)+"bp.in","w") as out_file:
        out_file.write("chain monomer state\n")
        for c in range(16):
            for n in range(chains[c]):
                out_file.write(str(c)+" "+str(n)+" "+"nothing"+"\n")
    with open(path_to+"/generic_interaction_"+str(g)+"kBT.in","w") as out_file:
        out_file.write("nothing"+" "+"nothing"+" "+str(g)+"\n")
    return len(chains),sum(chains)

def get_yeast_centromeres(bp: int=150):
    """return the start of each chromosome, each chromosomic arm as well as the centromeres.
    Parameters
    ----------
    bp    : the number of base-pair per bead (int)
    Returns
    -------
    Raises
    ------
    """
    C=16
    n_rDNA=150
    chains=[46*5000//bp,163*5000//bp,63*5000//bp,306*5000//bp,115*5000//bp,54*5000//bp,218*5000//bp,113*5000//bp,88*5000//bp,149*5000//bp,133*5000//bp,(186*5000+n_rDNA*1000)//bp,185*5000//bp,157*5000//bp,218*5000//bp,190*5000//bp]
    # centromeres
    centromeres=[29,47,21,89,29,29,99,20,70,87,87,29,52,125,64,110]
    centromeres=[c*5000//bp for c in centromeres]
    # start of the chromosome
    chr0=[0]*C
    arm0=[0]*(2*C)
    arm0[1]=29*5000//bp
    for c in range(1,C):
        chr0[c]=chr0[c-1]+chains[c-1]
        arm0[2*c]=chr0[c]
        if c==1:
            offset=47*5000//bp
        elif c==2:
            offset=21*5000//bp
        elif c==3:
            offset=89*5000//bp
        elif c==4:
            offset=29*5000//bp
        elif c==5:
            offset=29*5000//bp
        elif c==6:
            offset=99*5000//bp
        elif c==7:
            offset=20*5000//bp
        elif c==8:
            offset=70*5000//bp
        elif c==9:
            offset=87*5000//bp
        elif c==10:
            offset=87*5000//bp
        elif c==11:
            offset=29*5000//bp
        elif c==12:
            offset=52*5000//bp
        elif c==13:
            offset=125*5000//bp
        elif c==14:
            offset=64*5000//bp
        elif c==15:
            offset=110*5000//bp
        else:
            pass
        arm0[2*c+1]=arm0[2*c]+offset
    return chr0,arm0,centromeres

def make_yeast_single_chromosome_genome(bp: int=150,g: float=0.0,Temp: unit.Quantity=300.0*unit.kelvin,path_to: str="/scratch") -> tuple:
    """create the yeast single chromosome genome.
    Parameters
    ----------
    bp      : the number of base-pair per bead (integer)
    g       : the strength of interaction (in units of kBT) between state 'r' and state 's' (float)
    Temp    : temperature of the system (unit.Quantity)
    path_to : path to write the epigenetic model (str)
    Returns
    -------
    number of chain(s) (int), number of beads (int)
    Raises
    ------
    """
    # 5000 * N + 150 * 1000 = G(chr 12) 1078177 bp
    # N*r^3=n*a^3 donc a=100*(150/n)^1/3 nm
    # 1/3=N^3/2*b^3/(6^3/2*1000^3)
    kBT=get_kBT(Temp)
    n_rDNA=150
    model="yeast_single_chromosome_"+str(bp)+"bp"
    order=[10,9,8,7,4,3,2,1,11,15,12,13,14,6,5,16]
    chains=[46*5000//bp,163*5000//bp,63*5000//bp,306*5000//bp,115*5000//bp,54*5000//bp,218*5000//bp,113*5000//bp,88*5000//bp,149*5000//bp,133*5000//bp,(186*5000+n_rDNA*1000)//bp,185*5000//bp,157*5000//bp,218*5000//bp,190*5000//bp]
    chains=[chains[c-1] for c in order]
    with open(path_to+"/bead_state_"+model+"_"+str(C)+"chains_N"+str(N)+"_"+str(bp)+"bp.in","w") as out_file:
        out_file.write("chain monomer state\n")
        for n in range(sum(chains)):
            out_file.write(str(0)+" "+str(n)+" "+"nothing"+"\n")
    with open(path_to+"/generic_interaction_"+str(g)+"kBT_"+model+".in","w") as out_file:
        out_file.write("nothing"+" "+"nothing"+" "+str(g)+"\n")
    return 1,sum(chains)

def make_Yad_Ghavi_Helm_system(C: int,start: int=21000000,end: int=24000000,bp: int=150,g: float=0.0,Temp: unit.Quantity=300.0*unit.kelvin,path_to: str="/scratch") -> tuple:
    """create the Yad-Ghavi Helm model one state for the 'twist' promoter and the enhancer.
    Parameters
    ----------
    C       : number of chains (integer)
    start   : start of the part you want to study (integer)
    end     : end of the part you want to study (integer)
    bp      : the number of base-pair per bead (integer)
    g       : the strength of interaction (in units of kBT) between state 'r' and state 's' (float)
    Temp    : temperature of the system (unit.Quantity)
    path_to : path to write the epigenetic model (str)
    Returns
    -------
    number of chain(s) (int), number of beads (int)
    Raises
    ------
    """
    kBT=get_kBT(Temp)
    promoter=23046321//bp
    enhancers=[23049440,23053901,23120077,22865023,23097536,22971775,23084945,21381841,22996502]
    types=["wt","m1","m2","m3","m4","m5","m6","m7","m8"]
    E=len(enhancers)
    # enhancer genomic size (in bp)
    de=23050530-23049440
    # number of beads
    N=C*(end-start)//bp
    title="s"+str(start)+"_e"+str(end)+"_"+str(C)+"chains_N"+str(N)+"_"+str(bp)+"bp"
    # loop over the differents positions of the enhancer
    for e in range(E):
        model="promoter_twist_enhancer_"+types[e]+"_"+str(enhancers[e])
        print("Yad-Ghavi Helm system : "+model+", creation of the states and interactions files.")
        # states
        with open(path_to+"/bead_state_"+model+"_"+title+".in","w") as out_file:
            out_file.write("chain monomer state\n")
            for c in range(C):
                for s in range(start,end,bp):
                    if (s//bp)==promoter:
                        out_file.write(str(c)+" "+str(((s-start)//bp))+" "+"promoter"+"\n")
                    elif (enhancers[e]//bp)<=(s//bp) and (s//bp)<=((enhancers[e]+de)//bp):
                        out_file.write(str(c)+" "+str(((s-start)//bp))+" "+"enhancer"+"\n")
                    else:
                        out_file.write(str(c)+" "+str(((s-start)//bp))+" "+"nothing"+"\n")
        # interactions
        with open(path_to+"/enhancer_promoter_"+str(g)+"kBT.in","w") as out_file:
            out_file.write("nothing"+" "+"nothing"+" "+str(0.25*g)+"\n")
            out_file.write("enhancer"+" "+"enhancer"+" "+str(0.5*g)+"\n")
            out_file.write("promoter"+" "+"promoter"+" "+str(0.5*g)+"\n")
            out_file.write("enhancer"+" "+"promoter"+" "+str(g)+"\n")
    return C,N

def make_simple_epigenetic_models(C: int=1,N: int=100,bp: int=150,pattern: str="A.B.C",g: float=0.0,Temp: unit.Quantity=300.0*unit.kelvin,path_to: str="/scratch") -> tuple:
    """create simple epigenetic models based on the 'pattern' argument (with interactions between same state : 's' with 's').
    it also creates a file with binders to the states extracted from the pattern.
    it also creates a file with a Potts model according to the pattern.
    Each state file has the header 'chain monomer state'.
    Each binder file has the header 'binder number mass_amu sigma_nm'.
    Parameters
    ----------
    C       : number of chains (int)
    N       : the number of beads (int)
    bp      : the number of base-pair per bead (int)
    pattern : simple block-copolymer pattern (str)
              each state of the pattern has to be followed by a dot
    g       : the strength of interaction (in units of kBT) between state 'r' and state 's' (float)
    Temp    : temperature of the system (unit.Quantity)
    path_to : path to write the epigenetic model (str)
    Returns
    -------
    number of chain(s) (int), number of beads (int)
    Raises
    ------
    """
    kBT=get_kBT(Temp)
    spattern=pattern.split(".")
    n_states=len(spattern)
    per_states=int(N/n_states)+int((N%n_states)>0)
    # bead state
    if path_to=="":
        name="bead_state_"+pattern+"_"+str(C)+"chains_N"+str(N)+"_"+str(bp)+"bp.in"
    else:
        name=path_to+"/bead_state_"+pattern+"_"+str(C)+"chains_N"+str(N)+"_"+str(bp)+"bp.in"
    with open(name,"w") as out_file:
        out_file.write("chain monomer state\n")
        for c in range(C):
            for i in range(N):
                out_file.write(str(c)+" "+str(i)+" "+spattern[int(i/per_states)]+"\n")
    # interactions
    if path_to=="":
        name="Potts_"+str(g)+"kBT.in"
    else:
        name=path_to+"/Potts_"+str(g)+"kBT.in"
    with open(name,"w") as out_file:
        for i in range(n_states):
            for j in range(i,n_states):
                if spattern[i]==spattern[j]:
                    out_file.write(spattern[i]+" "+spattern[j]+" "+str(g)+"\n")
    # binders
    if path_to=="":
        name="binders_"+pattern+".in"
    else:
        name=path_to+"/binders_"+pattern+".in"
    with open(name,"w") as out_file:
        out_file.write("binder number mass_amu sigma_nm\n")
        for s in spattern:
            out_file.write("binder_"+s+" "+str(N//(4*n_states))+" 1000.0 10.0\n")
    # interactions with binders
    if path_to=="":
        name="Potts_binders_"+str(g)+"kBT.in"
    else:
        name=path_to+"/Potts_binders_"+str(g)+"kBT.in"
    with open(name,"w") as out_file:
        for s in spattern:
            out_file.write(s+" binder_"+s+" "+str(g)+"\n")
            out_file.write("binder_"+s+" binder_"+s+" "+str(g)+"\n")
    return C,N

def get_bonds(context: mm.Context) -> int:
    """return the number of bonds in the context.getSystem().
    Parameters
    ----------
    context : OpenMM context (OpenMM.Context)
    Returns
    -------
    the number of bonds in the context.getSystem() (integer).
    Raises
    ------
    """
    F=context.getSystem().getNumForces()
    n_bonds=0
    for f in range(F):
        try:
            n_bonds+=context.getSystem().getForce(f).getNumBonds()
        except:
            pass
    return n_bonds

def get_number_of_chains(context: mm.Context) -> int:
    """return the number of chains in the context.getSystem().
    it assumes the number of beads per chain to be the same.
    Parameters
    ----------
    context : OpenMM context (OpenMM.Context)
    Returns
    -------
    the number of bonds in the context.getSystem() (integer).
    Raises
    ------
    """
    # C chains, N particles per chain, C*(N-1) bonds
    return context.getSystem().getNumParticles()-get_bonds(context)

def remove_all_forces(context: mm.Context):
    """remove all the forces from the context.
    Parameters
    ----------
    context : OpenMM context
    Returns
    -------
    Raises
    ------
    """
    F=context.getSystem().getNumForces()
    print("before F=",F)
    for f in range(F):
        try:
            context.getSystem().removeForce(F-1-f)
        except:
            pass
    print("after F=",context.getSystem().getNumForces())

def get_interaction_from_title(model: str="") -> float:
    """return the strength of interaction from the file name 'model'.
    Parameters
    ----------
    model : name of the interactions model (str)
    Returns
    -------
    the interaction strengh (float)
    Raises
    ------
    """
    g=0.0
    si=model.split('_')
    # print(si)
    for s in si:
        if 'kBT' in s:
            # print(s)
            # print(s.replace('kBT',''))
            g=float(s.replace('kBT',''))
    return g

@numba.jit
def get_particles_per_bead():
    return 1
