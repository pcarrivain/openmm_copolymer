#!/usr/bin/python
# -*- coding: utf-8 -*-
"""@package docstring
This module 'models.py' is an example on how to create and run your model from
the functions described in the module 'openmm_copolymer_functions.py'.
The following code (has been developped by Pascal Carrivain) is distributed under MIT licence.
"""
import getopt
import numpy as np
import openmm_copolymer_functions as ocf
import sys
import time


def main(argv):
    # default parameters
    C = 1
    N = 100
    resolution = 150
    g = 0.2
    tmpdir = "/scratch"
    try:
        opts, args = getopt.getopt(
            argv,
            "hC:N:r:g:",
            ["chains=", "chain_length=", "resolution=", "g=", "tmpdir="],
        )
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for opt, arg in opts:
        if opt == "-h":
            print("usage:")
            print("python3.7 models.py -C <chains>")
            print("                    -N <chain length>")
            print("                    -r <resolution in bp>")
            print("                    -g <interactions strength in kBT>")
            print("                    --tmpdir=<where to save the data>")
            print(
                "python3.7 models.py -C 2 -N 1000 -r 150 -g 0.0 --tmpdir=/scratch/pcarriva/simulations/openmm_copolymer/epigenetics"
            )
            sys.exit()
        elif opt == "-C" or opt == "--chains":
            C = int(arg)
        elif opt == "-N" or opt == "--chain_length":
            N = int(arg)
        elif opt == "-r" or opt == "--resolution":
            resolution = int(arg)
        elif opt == "g":
            g = float(arg)
        elif opt == "--tmpdir":
            tmpdir = arg
        else:
            pass

    # temperature in Kelvin
    Temp = 300.0

    print("make Dernburg & al, Cell 1998 system")
    print("two copies of the chromosomes")
    ocf.make_wt_Dernburg_Cell_1998_epigenetic_model(
        2, resolution, g, Temp, tmpdir + "/"
    )
    ocf.make_mutant_Dernburg_Cell_1998_epigenetic_model(
        2, resolution, g, Temp, tmpdir + "/"
    )

    print("make Yeast single chromosome genome")
    ocf.make_yeast_single_chromosome_genome(resolution, g, Temp, tmpdir + "/")

    print("make Yeast genome")
    ocf.make_yeast_genome(resolution, g, Temp, tmpdir + "/")

    print("make simple epigenetic models with Potts interaction model (no binders)")
    print("ABCD pattern")
    ocf.make_simple_epigenetic_models(
        C, N, resolution, "A.B.C.D", [0] * 4, [g] * 4, Temp, tmpdir + "/"
    )

    print("make simple epigenetic models with Potts interaction model (with binders)")
    print("ABCD pattern")
    print("ABCD binders")
    ocf.make_simple_epigenetic_models(
        C, N, resolution, "A.B.C.D", [20] * 4, [g] * 4, Temp, tmpdir + "/"
    )


if __name__ == "__main__":
    main(sys.argv[1:])
